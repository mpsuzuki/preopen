#ifndef TRIFIDES_H
#define TRIFIDES_H

typedef enum {
  TRIFIDES_NONE = 0, /* for easier initialization by = { 0 } */
  TRIFIDES_PIPE,
  TRIFIDES_SOCK,
  TRIFIDES_AUTO /* only for trfd_open(), to be selected from available one */
} trfd_type;

typedef struct {
  int r;
  int w;
  int e;
} trfd_s;

typedef enum {
  TRIFIDES_CLOSE_ALWAYS = 0,
  TRIFIDES_CLOSE_ONLY /* close if it is unique */
} trfd_close_op;

int  trfd_open      (trfd_type, trfd_s*, trfd_s*);
int  trfd_close_all (trfd_s*);

int  trfd_close_r (trfd_s*, trfd_close_op);
int  trfd_close_w (trfd_s*, trfd_close_op);
int  trfd_close_e (trfd_s*, trfd_close_op);

int* trfd_get_fdp_r (trfd_s*);
int* trfd_get_fdp_w (trfd_s*);
int* trfd_get_fdp_e (trfd_s*);

int  trfd_xfer_012(trfd_s*);

#endif /* TRIFIDES_H */
