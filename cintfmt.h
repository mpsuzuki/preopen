#ifndef CINTFMT_H
#define CINTFMT_H

#if (defined(__ISO_C_VISIBLE)  && (__ISO_C_VISIBLE  >= 1999   )) || \
    (defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)) || \
    (defined(__POSIX_VISIBLE)  && (__POSIX_VISIBLE  >= 200112 ))
# define CINTFMT_SIZE_T	  "%zu"
# define CINTFMT_SSIZE_T  "%zd"
#else
# define CINTFMT_SIZE_T	  "%lu"
# define CINTFMT_SSIZE_T  "%ld"
#endif

#endif
