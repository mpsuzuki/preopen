#include <stdio.h>

#include "env.h"
#include "fmt.h"
#include "pathexec.h"
#include "str.h"
#include "stralloc.h"

int main(int argc, char** argv)
{
  int found_delim = 0, i;

  for (i = 1; i < argc; i++)
    if (str_equal("--", argv[i]))
      found_delim = 1;

  if (!found_delim)
    i = 1;
  else {
    for (i = 1; str_diff("--", argv[i]); i += 1) {
      FILE* f;
      int j = 0, k = 0, l = 0;
      char uint_buff[FMT_ULONG + 1];
      stralloc sa_env = { NULL, 0, 0 };

      for (j = 0; argv[i][j] && argv[i][j] != '='; j++)
        ;
      
      if (argv[i][j] != '=')
        continue;

      f = fopen(argv[i] + j + 1, "r");
      if (!f)
        continue;

      k = fileno(f);
      if (k < 3 ||
          !stralloc_ready(&sa_env, j + 1) ||
          !stralloc_copyb(&sa_env, argv[i], j) )
        goto close_continue;
      sa_env.s[j] = '\0';

      l = fmt_uint(uint_buff, k);
      uint_buff[l] = '\0';

      pathexec_env(sa_env.s, uint_buff);
      continue;

  close_continue:
      fclose(f);
    }

    /* skip "--" separator */
    i += 1;
  }

  pathexec(argv + i);
}
