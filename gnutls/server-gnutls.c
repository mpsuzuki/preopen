#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <gnutls/gnutls.h>
#include <assert.h>

#include "preopen-cli.h"

FILE* fh_log = NULL;
int   debug_level = 0;

#ifndef CAFILE
# define CAFILE "/etc/ssl/certs/ca-certificates.crt"
#endif
#define CRLFILE "crl.pem"

#define MAX_SIZE_PEM 0x100000 /* 1MiB by default */
#define DEFAULT_PORT 10443

#define CHECK(x) assert((x) >= 0)
#define LOOP_CHECK(rval, cmd) \
  do {                        \
    rval = cmd;               \
  } while (rval == GNUTLS_E_AGAIN || rval == GNUTLS_E_INTERRUPTED)

/* The OCSP status file contains up to date information about revocation
 * of the server's certificate. That can be periodically be updated
 * using:
 * $ ocsptool --ask --load-cert your_cert.pem --load-issuer your_issuer.pem
 *            --load-signer your_issuer.pem --outfile ocsp-status.der
 */
#define OCSP_STATUS_FILE "ocsp-status.der"

/* This is a sample TLS 1.0 echo server, using X.509 authentication and
 * OCSP stapling support.
 */

#define MAX_BUF 1024

int set_x509_server_cert_pkey_fd2(gnutls_certificate_credentials_t x509_cred,
                                  int fd_srv_cert,
                                  int fd_srv_pkey,
                                  size_t max_size_pem)
{
  int ret = -1;

  gnutls_datum_t datum_srv_cert, datum_srv_pkey;
  size_t buff_srv_cert_len = max_size_pem, buff_srv_pkey_len = max_size_pem;
  unsigned char* buff_srv_cert = propn_read_fd(fd_srv_cert, &buff_srv_cert_len);
  unsigned char* buff_srv_pkey = propn_read_fd(fd_srv_pkey, &buff_srv_pkey_len);

  if (!buff_srv_cert || !buff_srv_pkey)
    goto bail_out;

  datum_srv_cert.data = buff_srv_cert;
  datum_srv_cert.size = buff_srv_cert_len;

  datum_srv_pkey.data = buff_srv_pkey;
  datum_srv_pkey.size = buff_srv_pkey_len;

  ret = gnutls_certificate_set_x509_key_mem(x509_cred,
                                            &datum_srv_cert, &datum_srv_pkey,
                                            GNUTLS_X509_FMT_PEM);
  if (fh_log) {
    fprintf(fh_log, "gnutls_certificate_set_x509_key_mem() returns %d\n", ret);
    fprintf(fh_log, "  CERTFILE=%p len=%d\n", datum_srv_cert.data, datum_srv_cert.size);
    fprintf(fh_log, "  KEYFILE= %p len=%d\n", datum_srv_pkey.data, datum_srv_pkey.size);
    fprintf(fh_log, "  %s \"%s\"\n", 0 > ret ? "ERROR" : "SUCCESS", gnutls_strerror(ret));
    fflush(fh_log);
  }

bail_out:
  if (buff_srv_cert) {
    memset(buff_srv_cert, 0, buff_srv_cert_len);
    free(buff_srv_cert);
  }

  if (buff_srv_pkey) {
    memset(buff_srv_pkey, 0, buff_srv_pkey_len);
    free(buff_srv_pkey);
  }

  return ret;
}

void log_gnutls(int lvl, const char* str)
{
  if (!fh_log)
    return;
  fprintf(fh_log, "%d\t%s", lvl, str);
  fflush(fh_log);
}

ssize_t
stdout_write(gnutls_transport_ptr_t ptr, const void *data, size_t data_size)
{
  if ((long)ptr != 1)
    return 0;
  return write(1, data, data_size);
}

ssize_t
stdout_writev(gnutls_transport_ptr_t ptr, const giovec_t* iov, int iovcnt)
{
  int i;
  ssize_t sum = 0;

  if ((long)ptr != 1)
    return 0;

  for (i = 0; i < iovcnt; i++) {
    size_t len = write(1, iov[i].iov_base, iov[i].iov_len);
    if (len < iov[i].iov_len)
      return (sum + len);
    else
      sum += len;
  }
  return sum;
}

ssize_t
stdin_read(gnutls_transport_ptr_t ptr, void *data, size_t data_size)
{
  if ((long)ptr != 0)
    return 0;
  return read(0, data, data_size);
}

int
stdin_timeout(gnutls_transport_ptr_t ptr, unsigned int ms)
{
  return gnutls_system_recv_timeout(ptr, ms);
}

int main(const int argc, char* const* argv)
{
  int sd, ret;
  gnutls_certificate_credentials_t x509_cred;
  gnutls_priority_t priority_cache;
  char topbuf[512];
  gnutls_session_t session;
  char buffer[MAX_BUF + 1];

  propn_prog_opt_st prog_opt = PROPN_PROG_OPT_ST_NULL;
  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port = DEFAULT_PORT;

  int fd_srv_cert = -1, fd_srv_pkey = -1;
  int listen_sd = -1;

  propn_parse_args(argc, argv, &prog_opt);

  if (prog_opt.pathname_log)
    fh_log = fopen(prog_opt.pathname_log, "w");
  if (!fh_log && prog_opt.port > 0)
    fh_log = stderr;
  propn_chroot(&prog_opt);

  fd_srv_cert = propn_get_fd_by_var_name(prog_opt.envkey_srv_cert);
  if (fh_log) {
    fprintf(fh_log, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, prog_opt.envkey_srv_cert);
    fflush(fh_log);
  }

  fd_srv_pkey = propn_get_fd_by_var_name(prog_opt.envkey_srv_pkey);
  if (fh_log) {
    fprintf(fh_log, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, prog_opt.envkey_srv_pkey);
    fflush(fh_log);
  }

  /* for backwards compatibility with gnutls < 3.3.0 */
  CHECK(gnutls_global_init());
  gnutls_global_set_log_level(prog_opt.debug_level);
  gnutls_global_set_log_function(log_gnutls);

  CHECK(gnutls_certificate_allocate_credentials(&x509_cred));
  if (!prog_opt.pathname_chroot) {
    if (!prog_opt.envkey_ca_cert) {
      if (fh_log) {
        fprintf(fh_log, "CAFILE: use default bundle file %s\n", CAFILE);
        fflush(fh_log);
      }
      CHECK(gnutls_certificate_set_x509_trust_file(x509_cred, CAFILE, GNUTLS_X509_FMT_PEM));
    } else {
      int fd_ca_cert = propn_get_fd_by_var_name(prog_opt.envkey_ca_cert);
      if (fd_ca_cert < 0) {
        if (fh_log) {
          fprintf(fh_log, "CAFILE: use explicitly specified bundle file %s\n", prog_opt.envkey_ca_cert);
          fflush(fh_log);
        }
        CHECK(gnutls_certificate_set_x509_trust_file(x509_cred, prog_opt.envkey_ca_cert, GNUTLS_X509_FMT_PEM));
      } else {
        size_t max_size_pem = prog_opt.max_size_pem;
        gnutls_datum_t ca;
        ca.data = propn_read_fd(fd_ca_cert, &max_size_pem);
        ca.size = max_size_pem;
        if (fh_log) {
          fprintf(fh_log, "CAFILE: fd#%d <- %s (%p, len=%d)\n",
                                      fd_ca_cert, prog_opt.envkey_ca_cert,
                                      ca.data, (int)max_size_pem);
          fflush(fh_log);
        }
        CHECK(gnutls_certificate_set_x509_trust_mem(x509_cred, &ca, GNUTLS_X509_FMT_PEM));
        memset(ca.data, 0, ca.size);
        free(ca.data);
      }
    }
  }
  CHECK(set_x509_server_cert_pkey_fd2(x509_cred, fd_srv_cert, fd_srv_pkey, prog_opt.max_size_pem));

  CHECK(gnutls_priority_init(&priority_cache, NULL, NULL));

  CHECK(gnutls_init(&session, GNUTLS_SERVER));
  CHECK(gnutls_priority_set(session, priority_cache));
  CHECK(gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, x509_cred));

  /* We don't request any certificate from the client.
   * If we did we would need to verify it. One way of
   * doing that is shown in the "Verifying a certificate"
   * example.
   */
  gnutls_certificate_server_set_request(session, GNUTLS_CERT_IGNORE);
  gnutls_handshake_set_timeout(session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

  if (prog_opt.port > 0) {
    struct sockaddr_in sa_serv;
    struct sockaddr_in sa_cli;
    socklen_t client_len = sizeof(sa_cli);
    int optval = 1;
    listen_sd = socket(AF_INET, SOCK_STREAM, 0);

    memset(&sa_serv, '\0', sizeof(sa_serv));
    sa_serv.sin_family      = AF_INET;
    sa_serv.sin_addr.s_addr = INADDR_ANY;
    sa_serv.sin_port        = htons(prog_opt.port); /* Server Port number */

    setsockopt(listen_sd, SOL_SOCKET, SO_REUSEADDR, (void *)&optval, sizeof(int));

    bind(listen_sd, (struct sockaddr *)&sa_serv, sizeof(sa_serv));
    listen(listen_sd, 1024);
    if (fh_log) {
      fprintf(fh_log, "Server ready. Listening to port '%d'.\n\n", prog_opt.port);
      fflush(fh_log);
    }

    sd = accept(listen_sd, (struct sockaddr *)&sa_cli, &client_len);
    gnutls_transport_set_int(session, sd);

    if (fh_log) {
      fprintf(fh_log, "- connection from %s, port %d\n",
              inet_ntop(AF_INET, &sa_cli.sin_addr, topbuf, sizeof(topbuf)),
              ntohs(sa_cli.sin_port));
      fflush(fh_log);
    }
  } else {
    gnutls_transport_set_pull_function(session, stdin_read);
    gnutls_transport_set_push_function(session, stdout_write);
    gnutls_transport_set_vec_push_function(session, stdout_writev);
    gnutls_transport_set_pull_timeout_function(session, stdin_timeout);
    gnutls_transport_set_int2(session, 0, 1);
  }

  {
    ret = gnutls_handshake(session);
    if (ret < 0) {
      close(sd);
      gnutls_deinit(session);
      if (fh_log)
        fprintf(fh_log, "*** Handshake has failed (%s)\n\n", gnutls_strerror(ret));
    } else {
      const char reply[] = "test\n";

      if (fh_log)
        fprintf(fh_log, "- Handshake was completed\n");

      LOOP_CHECK(ret, gnutls_record_recv(session, buffer, MAX_BUF));

      if (ret == 0) {
        if (fh_log)
          fprintf(fh_log, "\n- Peer has closed the GnuTLS connection\n");
      } else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
        if (fh_log)
          fprintf(fh_log, "*** Warning: %s\n", gnutls_strerror(ret));
      } else if (ret < 0) {
        if (fh_log)
          fprintf(fh_log, "\n*** Received corrupted data(%d). Closing the connection.\n\n", ret);
      } else if (ret > 0) {
        /* echo data back to the client
         */
        CHECK(gnutls_record_send(session, reply, strlen(reply)));
      }
    }
    if (fh_log)
      fprintf(fh_log, "\n");
    /* do not wait for the peer to close the connection.
     */
    LOOP_CHECK(ret, gnutls_bye(session, GNUTLS_SHUT_WR));

    close(sd);
    gnutls_deinit(session);
  }
  if (0 < listen_sd)
    close(listen_sd);

  gnutls_certificate_free_credentials(x509_cred);
  gnutls_priority_deinit(priority_cache);

  gnutls_global_deinit();

  if (fh_log && stderr != fh_log)
    fclose(fh_log);

  return 0;
}
