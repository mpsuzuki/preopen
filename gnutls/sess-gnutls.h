#ifndef SESS_GTLS_H
#define SESS_GTLS_H

#include <gnutls/gnutls.h>
#include <assert.h>
#include "sslexec-help-sess.h"

ssl_sess_t* ssl_sess_fill_gtls(ssl_sess_t*, gnutls_session_t*);
ssl_sess_t* ssl_sess_init_err_sym_gtls(ssl_sess_t*);
gnutls_session_t get_session_from_sess(ssl_sess_t*);

#endif
