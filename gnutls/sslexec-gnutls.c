#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <gnutls/gnutls.h>
#include <assert.h>

#include "env.h"
#include "str.h"
#include "stralloc.h"
#include "pathexec.h"

#include "preopen-cli.h"

#include "config.h"
#include "preopen-cli.h"
#include "fdpb.h"
#include "trifides.h"

#include "sslexec-help.h"
#include "sslexec-help-sess.h"

#include "sess-gnutls.h"

FILE* fh_log = NULL;
int debug_level = 0;
size_t pipe_buff_size = 16;
int child_finished = 0;

#define MAX_REALLOC_SIZE (2 << 16)

#ifndef CAFILE
# define CAFILE "/etc/ssl/certs/ca-certificates.crt"
#endif
#define CRLFILE "crl.pem"

#define MAX_SIZE_PEM 0x100000 /* 1MiB by default */
#define DEFAULT_PORT 10443

#define CHECK(x) assert((x) >= 0)
#define LOOP_CHECK(rval, cmd) \
  do {                        \
    rval = cmd;               \
  } while (rval == GNUTLS_E_AGAIN || rval == GNUTLS_E_INTERRUPTED)

/* The OCSP status file contains up to date information about revocation
 * of the server's certificate. That can be periodically be updated
 * using:
 * $ ocsptool --ask --load-cert your_cert.pem --load-issuer your_issuer.pem
 *            --load-signer your_issuer.pem --outfile ocsp-status.der
 */
#define OCSP_STATUS_FILE "ocsp-status.der"

/* This is a sample TLS 1.0 echo server, using X.509 authentication and
 * OCSP stapling support.
 */

#define MAX_BUF 1024

gnutls_priority_t  priority_cache;
gnutls_certificate_credentials_t  x509_cred;
char topbuf[512];

int set_x509_server_cert_pkey_fd2(gnutls_certificate_credentials_t x509_cred,
                                  int fd_srv_cert,
                                  int fd_srv_pkey,
                                  size_t max_size_pem)
{
  int ret = -1;

  gnutls_datum_t datum_srv_cert, datum_srv_pkey;
  size_t buff_srv_cert_len = max_size_pem, buff_srv_pkey_len = max_size_pem;
  unsigned char* buff_srv_cert = propn_read_fd(fd_srv_cert, &buff_srv_cert_len);
  unsigned char* buff_srv_pkey = propn_read_fd(fd_srv_pkey, &buff_srv_pkey_len);

  if (!buff_srv_cert || !buff_srv_pkey)
    goto bail_out;

  datum_srv_cert.data = buff_srv_cert;
  datum_srv_cert.size = buff_srv_cert_len;

  datum_srv_pkey.data = buff_srv_pkey;
  datum_srv_pkey.size = buff_srv_pkey_len;

  ret = gnutls_certificate_set_x509_key_mem(x509_cred,
                                            &datum_srv_cert, &datum_srv_pkey,
                                            GNUTLS_X509_FMT_PEM);
  if (fh_log) {
    fprintf_log(0, "gnutls_certificate_set_x509_key_mem() returns %d\n", ret);
    fprintf_log(0, "  CERTFILE=%p len=%d\n", datum_srv_cert.data, datum_srv_cert.size);
    fprintf_log(0, "  KEYFILE= %p len=%d\n", datum_srv_pkey.data, datum_srv_pkey.size);
    fprintf_log(0, "  %s \"%s\"\n", 0 > ret ? "ERROR" : "SUCCESS", gnutls_strerror(ret));
    fflush(fh_log);
  }

bail_out:
  if (buff_srv_cert) {
    memset(buff_srv_cert, 0, buff_srv_cert_len);
    free(buff_srv_cert);
  }

  if (buff_srv_pkey) {
    memset(buff_srv_pkey, 0, buff_srv_pkey_len);
    free(buff_srv_pkey);
  }

  return ret;
}

void log_gnutls(int lvl, const char* str)
{
  fprintf_log(lvl, "%d\t%s", lvl, str);
}

ssize_t
stdout_write(gnutls_transport_ptr_t ptr, const void *data, size_t data_size)
{
  if ((long)ptr != 1)
    return 0;
  return write(1, data, data_size);
}

ssize_t
stdout_writev(gnutls_transport_ptr_t ptr, const giovec_t* iov, int iovcnt)
{
  int i;
  ssize_t sum = 0;

  if ((long)ptr != 1)
    return 0;

  for (i = 0; i < iovcnt; i++) {
    size_t len = write(1, iov[i].iov_base, iov[i].iov_len);
    if (len < iov[i].iov_len)
      return (sum + len);
    else
      sum += len;
  }
  return sum;
}

ssize_t
stdin_read(gnutls_transport_ptr_t ptr, void *data, size_t data_size)
{
  if ((long)ptr != 0)
    return 0;
  return read(0, data, data_size);
}

int
stdin_timeout(gnutls_transport_ptr_t ptr, unsigned int ms)
{
  return gnutls_system_recv_timeout(ptr, ms);
}

/*
 * retrun 1 if retryable
 */
int ssl_err_is_retryable(int ssl_err)
{
  switch (ssl_err) {
  case GNUTLS_E_SUCCESS:
  case GNUTLS_E_AGAIN:
  case GNUTLS_E_INTERRUPTED:
    return 1;
  default:
    return 0;
  }
}

int fill_priority(void)
{
  int err = 0;
  const char* gnutls_priority = env_get("GNUTLS_PRIORITY");
  const char* gnutls_no_sslv3 = env_get("NO_SSLV3");
  const char* gnutls_no_tlsv1 = env_get("NO_TLSV1");
  const char* gnutls_no_tlsv1_1 = env_get("NO_TLSV1_1");
  const char* gnutls_no_tlsv1_2 = env_get("NO_TLSV1_2");
  const char* err_pos_priority = NULL;
  static stralloc sa = {0};

  CHECK(gnutls_priority_init(&priority_cache, NULL, NULL));

  if (gnutls_priority) {
    stralloc_cats(&sa, gnutls_priority);
    if (gnutls_no_sslv3)
      stralloc_cats(&sa, ":-VERS-SSL3.0");
    if (gnutls_no_tlsv1)
      stralloc_cats(&sa, ":-VERS-TLS1.0");
    if (gnutls_no_tlsv1_1)
      stralloc_cats(&sa, ":-VERS-TLS1.1");
    if (gnutls_no_tlsv1_2)
      stralloc_cats(&sa, ":-VERS-TLS1.2");
  } else if (gnutls_no_sslv3 || gnutls_no_tlsv1 || gnutls_no_tlsv1_1 || gnutls_no_tlsv1_2 ) {
    if (GNUTLS_TLS1_2 < GNUTLS_TLS_VERSION_MAX)
      stralloc_cats(&sa, "NORMAL:-VERS-ALL:+VERS-TLS1.3");
    else
      stralloc_cats(&sa, "NORMAL:-VERS-ALL:+VERS-TLS1.2");

    if (!gnutls_no_sslv3)
      stralloc_cats(&sa, ":+VERS-SSL3.0");
    if (!gnutls_no_tlsv1)
      stralloc_cats(&sa, ":+VERS-TLS1.0");
    if (!gnutls_no_tlsv1_1)
      stralloc_cats(&sa, ":+VERS-TLS1.1");

    if (!gnutls_no_tlsv1_2 && GNUTLS_TLS1_2 < GNUTLS_TLS_VERSION_MAX)
      stralloc_cats(&sa, ":+VERS-TLS1.2");
    else if (gnutls_no_tlsv1_2)
      stralloc_cats(&sa, ":-VERS-TLS1.2");
  }

  if (sa.len == 0)
    err = gnutls_priority_init(&priority_cache, NULL, NULL);
  else {
    stralloc_0(&sa);
    err = gnutls_priority_init(&priority_cache, sa.s, &err_pos_priority);

    fprintf_log(0, "gnutls_priority_init returns %d\n", err);
    if (err && err_pos_priority)
      fprintf_log(0, "GNUTLS_PRIORITY is failed to reflect after: %s\n", err_pos_priority);
  }
  return err;
}


gnutls_session_t* setup_ssl(propn_prog_opt_st* prog_opt, int* client, int* sock, ssl_sess_t* sess)
{
  int fd_srv_cert = -1, fd_srv_pkey = -1;
  gnutls_session_t* ssl_ptr = malloc(sizeof(gnutls_session_t));

  if (!ssl_ptr)
    return NULL;

  fd_srv_cert = propn_get_fd_by_var_name(prog_opt->envkey_srv_cert);
  fprintf_log(0, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, prog_opt->envkey_srv_cert);

  fd_srv_pkey = propn_get_fd_by_var_name(prog_opt->envkey_srv_pkey);
  fprintf_log(0, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, prog_opt->envkey_srv_pkey);

  /* for backwards compatibility with gnutls < 3.3.0 */
  CHECK(gnutls_global_init());
  gnutls_global_set_log_level(prog_opt->debug_level);
  gnutls_global_set_log_function(log_gnutls);

  CHECK(gnutls_certificate_allocate_credentials(&x509_cred));
  if (!prog_opt->pathname_chroot) {
    if (!prog_opt->envkey_ca_cert) {
      CHECK(gnutls_certificate_set_x509_trust_file(x509_cred, CAFILE, GNUTLS_X509_FMT_PEM));
      fprintf_log(0, "CAFILE: use default bundle file \"%s\"\n", CAFILE);
    } else {
      int fd_ca_cert = propn_get_fd_by_var_name(prog_opt->envkey_ca_cert);
      if (fd_ca_cert < 0) {
        fprintf_log(0, "CAFILE: use explicitly specified bundle file %s\n", prog_opt->envkey_ca_cert);
        CHECK(gnutls_certificate_set_x509_trust_file(x509_cred, prog_opt->envkey_ca_cert, GNUTLS_X509_FMT_PEM));
      } else {
        size_t max_size_pem = prog_opt->max_size_pem;
        gnutls_datum_t ca;
        ca.data = propn_read_fd(fd_ca_cert, &max_size_pem);
        ca.size = max_size_pem;
        fprintf_log(0, "CAFILE: fd#%d <- %s (%p, len=%d)\n",
                                fd_ca_cert, prog_opt->envkey_ca_cert,
                                            ca.data, (int)max_size_pem);
        CHECK(gnutls_certificate_set_x509_trust_mem(x509_cred, &ca, GNUTLS_X509_FMT_PEM));
        memset(ca.data, 0, ca.size);
        free(ca.data);
      }
    }
  }
  CHECK(set_x509_server_cert_pkey_fd2(x509_cred, fd_srv_cert, fd_srv_pkey, prog_opt->max_size_pem));

  CHECK(fill_priority());
  CHECK(gnutls_init(ssl_ptr, GNUTLS_SERVER));
  CHECK(gnutls_priority_set(*ssl_ptr, priority_cache));
  CHECK(gnutls_credentials_set(*ssl_ptr, GNUTLS_CRD_CERTIFICATE, x509_cred));

  /* We don't request any certificate from the client.
   * If we did we would need to verify it. One way of
   * doing that is shown in the "Verifying a certificate"
   * example.
   */
  gnutls_certificate_server_set_request(*ssl_ptr, GNUTLS_CERT_IGNORE);
  gnutls_handshake_set_timeout(*ssl_ptr, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

  if (prog_opt->port > 0) {
    struct sockaddr_in sa_serv;
    struct sockaddr_in sa_cli;
    socklen_t client_len = sizeof(sa_cli);
    int optval = 1;
    *sock = socket(AF_INET, SOCK_STREAM, 0);

    memset(&sa_serv, '\0', sizeof(sa_serv));
    sa_serv.sin_family      = AF_INET;
    sa_serv.sin_addr.s_addr = INADDR_ANY;
    sa_serv.sin_port        = htons(prog_opt->port); /* Server Port number */

#if defined(SOL_SOCKET) && defined(SO_REUSEADDR)
    if (0 > setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, (void *)&optval, sizeof(int)))
      fprintf_log(0, "Failure in setsockopt( SO_REUSEADDR ), errno=%s\n", strerror(errno));
#endif

    bind(*sock, (struct sockaddr *)&sa_serv, sizeof(sa_serv));
    listen(*sock, 1024);
    fprintf_log(0, "Server ready. Listening to port '%d'.\n\n", prog_opt->port);

#if defined(SOL_SOCKET) && defined(SO_RCVTIMEO)
    if (prog_opt->timeout_accept > 0) {
      struct timeval tv = {prog_opt->timeout_accept, 0};
      if (0 > setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)))
        fprintf_log(0, "Failure in setsockopt( SO_RCVTIMEO ), errno=%s\n", strerror(errno));
    }
#endif

    *client = accept(*sock, (struct sockaddr *)&sa_cli, &client_len);
    gnutls_transport_set_int(*ssl_ptr, *client);

    fprintf_log(0, "- connection from %s, port %d\n",
              inet_ntop(AF_INET, &sa_cli.sin_addr, topbuf, sizeof(topbuf)),
              ntohs(sa_cli.sin_port));
  } else {
    gnutls_transport_set_pull_function(*ssl_ptr, stdin_read);
    gnutls_transport_set_push_function(*ssl_ptr, stdout_write);
    gnutls_transport_set_vec_push_function(*ssl_ptr, stdout_writev);
    gnutls_transport_set_pull_timeout_function(*ssl_ptr, stdin_timeout);
    gnutls_transport_set_int2(*ssl_ptr, 0, 1);
  }

  ssl_sess_fill_gtls(sess, ssl_ptr);
  return ssl_ptr;
}

void proc_parent(trfd_s* trfd, ssl_sess_t* sess)
{
  struct timeval  tv;

  tuple_err_t err = TUPLE_ERR_ZERO;
  fdpb_t  c2p, p2c, e2p;
  int     p_c2p = trfd->r, p_p2c = trfd->w, p_e2p = trfd->e;
  fd_set  fds_rd;
  int     nfds;
  int     ssl_rd_got_eof = 0;

  FD_ZERO(&fds_rd);
  FD_SET(p_c2p, &fds_rd);
  FD_SET(p_e2p, &fds_rd);
  FD_SET(sess->rfd, &fds_rd);

  nfds = MY_MAX( MY_MAX(p_c2p, p_e2p), sess->rfd ) + 1;

  fdpb_init(&p2c, p_p2c, pipe_buff_size);
  fdpb_init(&c2p, p_c2p, pipe_buff_size);
  fdpb_init(&e2p, p_e2p, pipe_buff_size);

  set_timeval(&tv, sess->timeout->select_rd, 0);
  set_timeval(&(p2c.tv), 1, 1);
  set_timeval(&(c2p.tv), 1, 1);
  set_timeval(&(e2p.tv), 1, 1);

  // reading loop
  while (0 != select(nfds, &fds_rd, NULL, NULL, &tv)) {
    dump_selected_fds(p_p2c, p_c2p, p_e2p, sess->rfd, sess->wfd, &fds_rd, NULL);

    fdpb_set_ready(&c2p, &fds_rd);
    fdpb_set_ready(&e2p, &fds_rd);

    handle_e2p(&e2p);
    handle_p2c(&p2c);

    if (FD_ISSET(sess->rfd, &fds_rd)) {
      if (0 <= ssl_to_p2c(sess, &p2c, &err)) {
        handle_e2p(&e2p);
        handle_p2c(&p2c);
        goto prep_next;
      }

      ssl_rd_got_eof = 1;
      break;
    } else
    if (fdpb_has_data(&c2p)) {
      if (0 > c2p_to_ssl(sess, &c2p, &err)) {
        ssl_rd_got_eof = 1;
        break;
      }

      if (0 == sess->get_pending(sess->ssl)) {
        switch (err.ssl) {
        case EINTR:
        case GNUTLS_E_INTERRUPTED:
        case GNUTLS_E_AGAIN:
          fprintf_log(0, "nothing to read after writing\n");
          goto prep_next;
        default:
          break;
        }
      }

      if (fdpb_is_valid(&p2c)) {
        if (0 > ssl_to_p2c(sess, &p2c, &err))
          ssl_rd_got_eof = 1;
      } else {
        if (0 > discard_ssl_pended_data(sess, &err))
          ssl_rd_got_eof = 1;
      }

      fprintf_log(0, "ssl_rd_got_eof = %d\n", ssl_rd_got_eof);
      if (ssl_rd_got_eof)
        break;

      goto prep_next;

    } else
    if (fdpb_is_valid(&c2p) && c2p.ready) {
      // SSL is not confirmed to be ready to write, but read from c2p to its internal buffer
      fdpb_read_internal(&c2p);

      fprintf_log(0, "fdpb_read_internal(c2p) got %zu bytes, total %zu bytes\n", c2p.len_chunk, c2p.len);
      if (c2p.got_eof) {
        fprintf_log(0, " c2p got EOF\n");
      } else
      if (0 > c2p_to_ssl(sess, &c2p, &err)) {
        ssl_rd_got_eof = 1;
        break;
      }
    }

    close_dead_pipes(&p2c, &c2p, &e2p);

    if (child_finished && !fdpb_is_valid(&p2c) && !fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "child process is finished, p2c, c2p, e2p are closed, break select() loop\n");
      break;
    }

prep_next:
    if (!fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "c2p & e2p are closed, exit loop\n");
      break;
    }

    fprintf_log(20, "prep for next select():");
    FD_ZERO(&fds_rd);
    if (fdpb_is_valid(&c2p)) {
      fprintf_log(21, " c2p");
      FD_SET(c2p.fd, &fds_rd);
    }
    if (fdpb_is_valid(&e2p)) {
      fprintf_log(21, " e2p");
      FD_SET(e2p.fd, &fds_rd);
    }
    if (ssl_err_is_retryable(err.ssl) && !ssl_rd_got_eof) {
      fprintf_log(21, " ssl_rd");
      FD_SET(sess->rfd, &fds_rd);
    }
    fprintf_log(21, "\n");

    set_timeval(&tv, sess->timeout->select_rd, 0);
  }
}

int main(const int argc, char* const* argv)
{
  int r = 0;
  propn_prog_opt_st prog_opt = PROPN_PROG_OPT_ST_NULL;

  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port = DEFAULT_PORT;
  prog_opt.timeout_accept = 60;
  prog_opt.timeout_select = 60;
  propn_parse_args(argc, argv, &prog_opt);
  if (prog_opt.buffsize > 0)
    pipe_buff_size = prog_opt.buffsize;

  debug_level = prog_opt.debug_level;

  /* Ignore broken pipe signals */
  // signal(SIGPIPE, SIG_IGN);
  signal(SIGPIPE, update_child_finished);
  signal(SIGCHLD, update_child_finished);

  if (prog_opt.pathname_log) {
    unlink(prog_opt.pathname_log);
    fh_log = fopen(prog_opt.pathname_log, "w");
  } else
    fh_log = stderr;

  if (fh_log)
    fflush(fh_log);

  /* Handle connections */
  {
    pid_t pid;

    trfd_s trfd_prnt = { 0, 0, 0 }, trfd_chld = { 0, 0, 0 };

    if (trfd_open( TRIFIDES_AUTO, &trfd_prnt, &trfd_chld )) {
      fprintf_log(0, "failure in 3-file descriptors between parent and child STDIO/STDERR\n");
      r = -1;
      goto log_close;
    }

    pid = fork();
    switch (pid) {
      case -1:
        r = -2;
        break;

      case 0:
        /* child process */
        trfd_close_all(&trfd_prnt);
        propn_uidgid(&prog_opt, 1);
        trfd_xfer_012(&trfd_chld);

        pathexec(argv + (prog_opt.index_last_parsed_argv + 1));
        break;

      default:
        /* parent process, should keep SSL pipes until child process exit */
        trfd_close_all(&trfd_chld);
 
        {
          int client = -1, sock = -1;
          ssl_timeout_t sess_to = {prog_opt.timeout_accept, prog_opt.timeout_select, prog_opt.timeout_select};
          ssl_sess_t sess;
          gnutls_session_t ssl_d = 0;

          if (propn_chroot(&prog_opt)) {
            fprintf_log(0, "chroot failed\n");
          }

          propn_uidgid(&prog_opt, 0);

          ssl_sess_clear(&sess);
          ssl_sess_init_err_sym_gtls(&sess);
          sess.timeout = &sess_to;
          setup_ssl(&prog_opt, &client, &sock, &sess);
          if (!sess.ssl) {
            fprintf_log(0, "Failed to setup SSL session: %s\n", strerror(errno));
            goto bail_out;
          }
          ssl_d = *(gnutls_session_t*)(sess.ssl);

          if (GNUTLS_E_SUCCESS != (r = gnutls_handshake(ssl_d))) {
            const char* sym = sess.get_err_sym(r);
            fprintf_log(0, "Error in gnutls_handshake(): %s\n", sym ? sym : "Unknown");
            goto bail_out;
          } else {
            /* stderr from client should be written somewhere, even if no debug mode */
            if (!fh_log)
              fh_log = stderr;

            proc_parent(&trfd_prnt, &sess);
            fprintf_log(0, "parental loop is finished\n");
            goto bail_out;
          }

bail_out:
          if (!child_finished) {
            fprintf_log(0, "terminate child process pid=%d\n", pid);
            kill(pid, SIGTERM);
          }
          if (ssl_d) {
            LOOP_CHECK(r, gnutls_bye(ssl_d, GNUTLS_SHUT_WR));
            gnutls_deinit(ssl_d);
          }
          if (0 <= client)
            close(client);
          if (0 <= sock)
            close(sock);
          gnutls_certificate_free_credentials(x509_cred);
          gnutls_priority_deinit(priority_cache);
          gnutls_global_deinit();
        } /* parent  process */
    } /* switch */
  }

log_close:
  if (fh_log)
    fclose(fh_log);

  exit(r);
}
