#include <gnutls/gnutls.h>
#include <assert.h>

#include "fdpb.h"

#include "sslexec-help-sess.h"

/*
 * Most GnuTLS functions take the "gnutls_session_t" data, not "gnutls_session_t*" pointer.
 * Significant contrast with OpenSSL-style API taking "SSL*" pointer.
 * Although the gnutls_session_t is a pointer type (see gnutls.h) in current implementation,
 * and we can cast it to "void*" pointer, we do not hold a raw gnutls_session_t data in
 * ssl_sess_t.ssl, we hold "gnutls_session_t*" pointer in ssl_sess_t.ssl.
 *
 * Therefore, gnutls_record_read(), etc are called with refered value *(ssl_sess_t.ssl).
 * It might be looking like unsafe code.
 *
 */
gnutls_session_t get_session_from_sess(ssl_sess_t* sess)
{
  return *(gnutls_session_t*)(sess->ssl);
}

ssize_t gtls_read(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  MY_UNUSED(d);
  return gnutls_record_recv(*(gnutls_session_t*)ssl, buff, (size_t)len);
}

ssize_t gtls_write(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  MY_UNUSED(d);
  return gnutls_record_send(*(gnutls_session_t*)ssl, buff, (size_t)len);
}

ssize_t gtls_get_pending(void* ssl)
{
  return gnutls_record_check_pending(*(gnutls_session_t*)ssl);
}

/* in GnuTLS convention, the native GnuTLS error codes are negative,
 * and the native GnuTLS functions return them directly, no need to
 * translate from opaque function return value to error code.
 */
int gtls_get_ssl_err(void* ssl, int ssl_ret)
{
  MY_UNUSED(ssl);
  return ssl_ret;
}

const char* gtls_get_err_sym(int err)
{
  /* gnutls_strerror() returns the description of the error,
   * like "The Diffie-Hellman prime sent by the server is not acceptable (not long enough).".
   * gnutls_strerror_name() returns the symbolic name of the error,
   * like "GNUTLS_E_DH_PRIME_UNACCEPTABLE"
   */
  return gnutls_strerror_name(err);
}

ssl_sess_t* ssl_sess_fill_gtls(ssl_sess_t* sess, gnutls_session_t* ssl)
{
  if (!sess || !ssl)
    return NULL;

  sess->ssl = ssl;
  gnutls_transport_get_int2(*ssl, &(sess->rfd), &(sess->wfd));
  sess->read  = gtls_read;
  sess->write = gtls_write;
  sess->get_pending = gtls_get_pending;
  sess->get_ssl_err = gtls_get_ssl_err;
  sess->get_err_sym = gtls_get_err_sym;

  return sess;
}

ssl_sess_t* ssl_sess_init_err_sym_gtls(ssl_sess_t* sess)
{
  if (!sess)
    return NULL;

  sess->get_ssl_err = gtls_get_ssl_err;
  sess->get_err_sym = gtls_get_err_sym;

  return sess;
}
