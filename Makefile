all: preopen.exe libpreopen-cli.a check-fds.exe pipe-exec.exe echoback.exe configure

CFLAGS = -g3 -ggdb -O0 -Wall -Wextra
# QLIBS_SRC_DIR=../fehQlibs
# QLIBS_CFLAGS = -I$(QLIBS_SRC_DIR)/include
# QLIBS_LIBS = -L$(QLIBS_SRC_DIR)/ -lqlibs
# PREOPEN_DEPS =
QLIBS_MINI_DIR = qlibs-mini
QLIBS_CFLAGS = -I$(QLIBS_MINI_DIR)
QLIBS_LIBS = -L$(QLIBS_MINI_DIR) -lqlibs-mini
LIBQLIBS_MINI_A = libqlibs-mini.a
PREOPEN_DEPS = $(LIBQLIBS_MINI_A)
$(LIBQLIBS_MINI_A):
	make -C qlibs-mini

VPATH = .:$(QLIBS_MINI_DIR)/

.SUFFIXES: .c .o .exe
.PHONY: clean $(LIBQLIBS_MINI_A) configure

# Autotool
ACLOCAL := $(if $(ACLOCAL),$(ACLOCAL),aclocal)
AUTOHEADER := $(if $(AUTOHEADER),$(AUTOHEADER),autoheader)
AUTOMAKE := $(if $(AUTOMAKE),$(AUTOMAKE),automake)
AUTOCONF := $(if $(AUTOCONF),$(AUTOCONF),autoconf)
configure:
	$(ACLOCAL)
	$(AUTOHEADER)
	$(AUTOMAKE) -a
	$(AUTOCONF)

# MINIMUM SAMPLES

preopen.exe: preopen.c $(PREOPEN_DEPS)
	$(CC) $(CFLAGS) $(QLIBS_CFLAGS) -o $@ $< $(QLIBS_LIBS)

echoback.exe: echoback.c
	$(CC) $(CFLAGS) $(QLIBS_CFLAGS) -o $@ $< $(QLIBS_LIBS)

pipe-exec.exe: pipe-exec.c
	$(CC) $(CFLAGS) $(QLIBS_CFLAGS) -o $@ $< $(QLIBS_LIBS) $(PREOPEN_LDFLAGS)

check-fds.exe: check-fds.c
	$(CC) $(CFLAGS) -o $@ $<

#
# CLIENT LIB
#

LIBPREOPEN_CLI_A = libpreopen-cli.a
PREOPEN_LDFLAGS = -L. -lpreopen-cli
RANLIB := $(if $(RANLIB),$(RANLIB), ranlib)

preopen-cli.o: preopen-cli.c preopen-cli.h
fdpb.o: fdpb.c fdpb.h

$(LIBPREOPEN_CLI_A): preopen-cli.o fdpb.o
	$(AR) $(ARFLAGS) $@ $^
	$(RANLIB) $@

clean:
	rm -vf *.o *.a *.exe
