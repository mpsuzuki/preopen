#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <errno.h>

/* for dup2() */
#include <unistd.h>

#ifdef HAVE_SOCKETPAIR
# include <sys/socket.h>
#endif


#include "trifides.h"

int trfd_open (trfd_type tp, trfd_s* trfd_prnt, trfd_s* trfd_chld)
{
  int r = 0;
  int tmp[2];

  if (!trfd_prnt || !trfd_chld)
    return -1;

  if (tp == TRIFIDES_AUTO)
#ifdef HAVE_SOCKETPAIR
    tp = TRIFIDES_SOCK;
#else
    tp = TRIFIDES_PIPE;
#endif

  switch(tp) {
  case TRIFIDES_PIPE:
    r = pipe(tmp);
    if (r < 0)
      goto bail_out0;

    trfd_prnt->r = tmp[0];
    trfd_chld->w = tmp[1];

    r = pipe(tmp);
    if (r < 0)
      goto bail_out1;

    trfd_chld->r = tmp[0];
    trfd_prnt->w = tmp[1];

    r = pipe(tmp);
    if (r < 0)
      goto bail_out2;

    trfd_prnt->e = tmp[0];
    trfd_chld->e = tmp[1];
    break;

  bail_out2:
    close(trfd_chld->r);
    close(trfd_prnt->w);

  bail_out1:
    close(trfd_prnt->r);
    close(trfd_chld->w);

  bail_out0:
    break;

#ifdef HAVE_SOCKETPAIR
  case TRIFIDES_SOCK:
    r = socketpair(AF_UNIX, SOCK_STREAM, 0, tmp);
    if (r < 0)
      break;

    trfd_prnt->r = tmp[0];
    trfd_prnt->w = tmp[0];
    trfd_chld->r = tmp[1];
    trfd_chld->w = tmp[1];

    r = pipe(tmp);
    if (r < 0) {
      close(trfd_prnt->r);
      close(trfd_chld->r);
      break;
    }

    trfd_prnt->e = tmp[0];
    trfd_chld->e = tmp[1];

    return 0;
#endif

  default: break;
  }

  if (r)
    return errno;

  return 0;
}

int trfd_close_all(trfd_s* trfd)
{
  int r = 0;

  r = trfd_close_r(trfd, TRIFIDES_CLOSE_ALWAYS);
  if (r)
    return r;

  r = trfd_close_w(trfd, TRIFIDES_CLOSE_ALWAYS);
  if (r)
    return r;

  r = trfd_close_e(trfd, TRIFIDES_CLOSE_ALWAYS);

  return r;
}

int trfd_close_r(trfd_s* trfd, trfd_close_op op)
{
  int r = 0;
  int shared_w = (trfd->r == trfd->w);
  int shared_e = (trfd->r == trfd->e);

  if (!trfd)
    return -1;

  if (trfd->r < 0)
    return 0;

  switch (op) {
  case TRIFIDES_CLOSE_ONLY:
    if (shared_w || shared_e)
      return 0;
    break;

  case TRIFIDES_CLOSE_ALWAYS:
    break;

  default:
    return -2;
  }

  r = close(trfd->r);
  if (r)
    return r;

  trfd->r = -1;

  if (shared_w)
    trfd->w = -1;

  if (shared_e)
    trfd->e = -1;
  return 0;
}

int trfd_close_w(trfd_s* trfd, trfd_close_op op)
{
  int r = 0;
  int shared_r = (trfd->w == trfd->r);
  int shared_e = (trfd->w == trfd->e);

  if (!trfd)
    return -1;

  if (trfd->w < 0)
    return 0;

  switch (op) {
  case TRIFIDES_CLOSE_ONLY:
    if (shared_r || shared_e)
      return 0;
    break;

  case TRIFIDES_CLOSE_ALWAYS:
    break;

  default:
    return -2;
  }

  r = close(trfd->w);
  if (r)
    return r;

  trfd->w = -1;

  if (shared_r)
    trfd->w = -1;

  if (shared_e)
    trfd->e = -1;
  return 0;
}

int trfd_close_e(trfd_s* trfd, trfd_close_op op)
{
  int r = 0;
  int shared_r = (trfd->e == trfd->r);
  int shared_w = (trfd->e == trfd->w);

  if (!trfd)
    return -1;

  if (trfd->e < 0)
    return 0;

  switch (op) {
  case TRIFIDES_CLOSE_ONLY:
    if (shared_r || shared_w)
      return 0;
    break;

  case TRIFIDES_CLOSE_ALWAYS:
    break;

  default:
    return -2;
  }

  r = close(trfd->e);
  if (r)
    return r;

  trfd->e = -1;

  if (shared_r)
    trfd->r = -1;

  if (shared_w)
    trfd->w = -1;
  return 0;
}

int* trfd_get_fdp_r (trfd_s* trfd)
{
  if (!trfd)
    return NULL;
  return &(trfd->r);
}

int* trfd_get_fdp_w (trfd_s* trfd)
{
  if (!trfd)
    return NULL;
  return &(trfd->w);
}

int* trfd_get_fdp_e (trfd_s* trfd)
{
  if (!trfd)
    return NULL;
  return &(trfd->e);
}

int trfd_xfer_012   (trfd_s* trfd)
{
  int r = 0;
  r = dup2(*trfd_get_fdp_r(trfd), 0);
  if (r < 0)
    return r;

  r = dup2(*trfd_get_fdp_w(trfd), 1);
  if (r < 0)
    return r;
 
  r = dup2(*trfd_get_fdp_e(trfd), 2);
  if (r < 0)
    return r;

  trfd_close_all(trfd);
  return 0;
}
