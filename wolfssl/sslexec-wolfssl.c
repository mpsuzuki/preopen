#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <signal.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <sys/select.h>

/* sockaddr_in */
#include <netinet/in.h>

#define DEBUG_WOLFSSL_VERBOSE
#include <wolfssl/ssl.h>
#include <wolfssl/error-ssl.h>

#include "env.h"
#include "pathexec.h"

#include "config.h"
#include "preopen-cli.h"
#include "fdpb.h"
#include "trifides.h"

#include "sslexec-help.h"
#include "sslexec-help-sess.h"

#include "sess-wolfssl.h"

/* chunk size to load PEM file */
#define SIZE_CHUNK_PEM 1024

/* buffer size to receive SSL error message */
/* according to man 3 ERR_error_string, its size should be > 256 */
#define SIZE_SSL_ERR_STR 1024

#define DEFAULT_PORT 10443

FILE* fh_log = NULL;
int debug_level = 0;
size_t pipe_buff_size = 16;
int child_finished = 0;

#define MAX_REALLOC_SIZE (2 << 16)

int create_socket(int port)
{
  int s, optval = 1;
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    fprintf_log(0, "Unable to create socket");
    return -1;
  }

#if defined(SOL_SOCKET) && defined(SO_REUSEADDR)
  if (0 > setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (void *)&optval, sizeof(int)))
    fprintf_log(0, "Failure in setsockopt( SO_REUSEADDR ), errno=%s\n", strerror(errno));
#endif

  if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    fprintf_log(0, "Unable to bind");
    close(s);
    return -1;
  }

  if (listen(s, 1) < 0) {
    fprintf_log(0, "Unable to listen");
    close(s);
    return -1;
  }

  fprintf_log(0, "Listen at port %d\n", port);

  return s;
}

WOLFSSL_CTX *create_context()
{
  WOLFSSL_CTX *ctx;

  ctx = wolfSSL_CTX_new(wolfTLS_server_method());
  if (!ctx) {
    fprintf_log(0, "wolfSSL_CTX_new() returns NULL, failed to create SSL context\n");
    return NULL;
  }

  {
    long ssl_ctx_option = 0;
    void *NO_SSLv3   = env_get("NO_SSLV3");
    void *NO_TLSv1   = env_get("NO_TLSV1");
    void *NO_TLSv1_1 = env_get("NO_TLSV1_1");
    void *NO_TLSv1_2 = env_get("NO_TLSV1_2");
    char* my_ciphers = env_get("WOLFSSL_CIPHERS");

    if (NO_SSLv3)    ssl_ctx_option |= WOLFSSL_OP_NO_SSLv3;
    if (NO_TLSv1)    ssl_ctx_option |= WOLFSSL_OP_NO_TLSv1;
    if (NO_TLSv1_1)  ssl_ctx_option |= WOLFSSL_OP_NO_TLSv1_1;
    if (NO_TLSv1_2)  ssl_ctx_option |= WOLFSSL_OP_NO_TLSv1_2;
    if (ssl_ctx_option)
      wolfSSL_CTX_set_options(ctx, ssl_ctx_option);

    if (my_ciphers && !wolfSSL_CTX_set_cipher_list(ctx, my_ciphers)) {
      fprintf_log(0, "Unable to set cipher list %s\n", my_ciphers);
      wolfSSL_CTX_free(ctx);
      return NULL;
    }
  }

  return ctx;
}

int ctx_use_cert_pkey_fd2(WOLFSSL_CTX* ctx,
                          int fd_srv_cert,
                          int fd_srv_pkey,
                          size_t max_size_pem)
{
  int ret = -1;

  size_t buff_srv_cert_len = max_size_pem, buff_srv_pkey_len = max_size_pem;
  unsigned char* buff_srv_cert = propn_read_fd(fd_srv_cert, &buff_srv_cert_len);
  unsigned char* buff_srv_pkey = propn_read_fd(fd_srv_pkey, &buff_srv_pkey_len);

  if (!buff_srv_cert || !buff_srv_pkey)
    goto bail_out;

  ret = wolfSSL_CTX_use_certificate_chain_buffer(ctx, buff_srv_cert, buff_srv_cert_len);
  fprintf_log(0, "wolfSSL_CTX_use_certificate_chain_buffer() returns %d (%s)\n", ret, (ret == 1) ? "Success" : "Failed" );
  if (ret != 1)
    goto bail_out;

  ret = wolfSSL_CTX_use_PrivateKey_buffer(ctx, buff_srv_pkey, buff_srv_pkey_len, WOLFSSL_FILETYPE_PEM);
  fprintf_log(0, "wolfSSL_CTX_use_PrivateKey_buffer() returns %d (%s)\n", ret, (ret == 1) ? "Success" : "Failed" );

bail_out:
  if (buff_srv_cert) {
    memset(buff_srv_cert, 0, buff_srv_cert_len);
    free(buff_srv_cert);
  }

  if (buff_srv_pkey) {
    memset(buff_srv_pkey, 0, buff_srv_pkey_len);
    free(buff_srv_pkey);
  }

  return ret;
}

int
stdout_write(WOLFSSL* ssl, char* data, int data_size, void* ctx)
{
  MY_UNUSED(ctx);
  if (!ssl)
    return 0;
  return write(1, data, data_size);
}

int
stdin_read(WOLFSSL* ssl,  char* data, int data_size, void* ctx)
{
  MY_UNUSED(ctx);
  if (!ssl)
    return 0;
  return read(0, data, data_size);
}

/*
 * retrun 1 if retryable
 *
 * NOTE: OpenSSL's SSL error is positive, wolfSSL has compatibility errors,
 *       but wolfSSL internal error codes are nagative. refer wolfssl/error-ssl.h
 *
 */
int ssl_err_is_retryable(int ssl_err)
{
  switch (ssl_err) {
  case WOLFSSL_ERROR_NONE:         return 1;
  case WOLFSSL_ERROR_WANT_READ:    return 1;
  case WOLFSSL_ERROR_WANT_WRITE:   return 1;
  case WANT_READ:    return 1;
  case WANT_WRITE:   return 1;

  /* SSL session is closed normally */
  case WOLFSSL_ERROR_ZERO_RETURN:  return 0;

  /* SSL session is no longer valid */
  case WOLFSSL_ERROR_WANT_CONNECT:     return 0;
  case WOLFSSL_ERROR_WANT_ACCEPT:      return 0;
  case WOLFSSL_ERROR_WANT_X509_LOOKUP: return 0;

#if 0 /* following errors are defined in OpenSSL, but not in wolfSSL */
  case SSL_ERROR_WANT_ASYNC:       return 0;
  case SSL_ERROR_WANT_ASYNC_JOB:   return 0;
  case SSL_ERROR_WANT_CLIENT_HELLO_CB: return 0;
#endif

  /* out-of-OpenSSL errors */
  case WOLFSSL_ERROR_SYSCALL:      return 0;
  case WOLFSSL_ERROR_SSL:          return 0;

  /* wolfSSL-specific errors (defined in wolfssl/error-ssl.h) are taken as non-retryable */
  default: return (0 <= ssl_err ? 1 : 0);
  }
}

WOLFSSL* setup_ssl(propn_prog_opt_st* prog_opt, int* client, int* sock, WOLFSSL_CTX **ctx, ssl_sess_t* sess)
{
  int ret;
  int fd_srv_cert, fd_srv_pkey;
  WOLFSSL *ssl = NULL;

  if (!prog_opt || !client || !sock || !ctx || !sess)
    return NULL; /* invalid argument */

  *client = -1;
  *sock   = -1;

  *ctx = create_context();
  if (!(*ctx)) {
    fprintf_log(0, "create_contex() returns NULL\n");
    goto bail_out;
  }

  fd_srv_cert = propn_get_fd_by_var_name(prog_opt->envkey_srv_cert);
  fprintf_log(0, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, prog_opt->envkey_srv_cert);

  fd_srv_pkey = propn_get_fd_by_var_name(prog_opt->envkey_srv_pkey);
  fprintf_log(0, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, prog_opt->envkey_srv_pkey);

  ret = ctx_use_cert_pkey_fd2(*ctx, fd_srv_cert, fd_srv_pkey, prog_opt->max_size_pem);
  fprintf_log(0, "ctx_use_cert_pkey_fd2() returns %d (%s)\n",
              ret, (ret == 1) ? "Success" : sess->get_err_sym(ret));

  if (prog_opt->port > 0) {
    *sock = create_socket(prog_opt->port);
    if (*sock < 0)
      goto bail_out;
  }

  ssl = wolfSSL_new(*ctx);
  if (!ssl) {
    fprintf_log(0, "wolfSSL_new() returned NULL\n");
    goto bail_out;
  }

  if (prog_opt->port == 0) {
    fprintf_log(0, "wolfSSL_set_read_fd(SSL=%p, 0), wolfSSL_set_write_fd(SSL=%p, 1)\n", ssl, ssl);

    ret = wolfSSL_set_read_fd(ssl, 0);
    if (ret != WOLFSSL_SUCCESS) {
      fprintf_log(0, "wolfSSL_set_read_fd() returns %d\n", ret);
      goto bail_out;
    }
    sess->rfd = 0;

    ret = wolfSSL_set_write_fd(ssl, 1);
    if (ret != WOLFSSL_SUCCESS) {
      fprintf_log(0, "wolfSSL_set_write_fd() returns %d\n", ret);
      goto bail_out;
    }
    sess->wfd = 1;

  } else
  if (0 < *sock) {
    struct sockaddr_in addr;
    unsigned int len = sizeof(addr);

#if defined(SOL_SOCKET) && defined(SO_RCVTIMEO)
    if (prog_opt->timeout_accept > 0) {
      struct timeval tv = {prog_opt->timeout_accept, 0};
      if (0 > setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)))
        fprintf_log(0, "Failure in setsockopt( SO_RCVTIMEO ), errno=%s\n", strerror(errno));
    }
#endif
    *client = accept(*sock, (struct sockaddr*)&addr, &len);
    if (*client < 0) {
      fprintf_log(0, "Failure in accept(), errno=%s\n", strerror(errno));
      goto bail_out;
    }

    fprintf_log(0, "wolfSSL_set_fd(SSL=%p, socket=%d)\n", ssl, *client);
    ret = wolfSSL_set_fd(ssl, *client);
    fprintf_log(0, "wolfSSL_set_fd() returns %d (%s)\n", ret, ret == 1 ? "Success" : "Fail");
    if (ret == 0) {
      /* XXX error should be logged */
      goto bail_out;
    }
    sess->rfd = *client;
    sess->wfd = *client;
  }

  /* because wolfSSL_get_fd(ssl) does not distinguish rfd/wfd,
   * we should not overwrite sess.rfd and sess.wfd which were set in above.
   */
  ssl_sess_fill_wssl(sess, ssl, 0);
  return ssl;

bail_out:
  /* the caller may want to know the success/fail by the return value (= ssl),
   * so if we got any failure, we should shutdown ssl before return
   */
  if (ssl) {
    wolfSSL_shutdown(ssl);
    wolfSSL_free(ssl);
    ssl = NULL;
  }

  if (*ctx) {
    wolfSSL_CTX_free(*ctx);
    *ctx = NULL;
  }

  if (0 < prog_opt->port) {
    if (0 < *client)
      close(*client);

    if (0 < *sock)
      close(*sock);
  }

  *client = -1;
  *sock   = -1;

  return NULL;
}

void proc_parent(trfd_s* trfd, ssl_sess_t* sess)
{
  struct timeval  tv;

  tuple_err_t err = TUPLE_ERR_ZERO;
  fdpb_t  c2p, p2c, e2p;
  int     p_c2p = trfd->r, p_p2c = trfd->w, p_e2p = trfd->e;
  fd_set  fds_rd;
  /* fd_set  fds_wr; */
  int     nfds;
  int     ssl_rd_got_eof = 0;

  FD_ZERO(&fds_rd);
  FD_SET(p_c2p, &fds_rd);
  FD_SET(p_e2p, &fds_rd);
  FD_SET(sess->rfd, &fds_rd);

  nfds = MY_MAX( MY_MAX(p_c2p, p_e2p), sess->rfd ) + 1;

  fdpb_init(&p2c, p_p2c, pipe_buff_size);
  fdpb_init(&c2p, p_c2p, pipe_buff_size);
  fdpb_init(&e2p, p_e2p, pipe_buff_size);

  set_timeval(&tv, sess->timeout->select_rd, 0);
  set_timeval(&(p2c.tv), 1, 1);
  set_timeval(&(c2p.tv), 1, 1);
  set_timeval(&(e2p.tv), 1, 1);

  // reading loop
  while (0 != select(nfds, &fds_rd, NULL, NULL, &tv)) {
    dump_selected_fds(p_p2c, p_c2p, p_e2p, sess->rfd, sess->wfd, &fds_rd, NULL);

    fdpb_set_ready(&c2p, &fds_rd);
    fdpb_set_ready(&e2p, &fds_rd);

    handle_e2p(&e2p);
    handle_p2c(&p2c);

    if (FD_ISSET(sess->rfd, &fds_rd)) {
      fprintf_log(0, "something to be read\n");
      if (0 <= ssl_to_p2c(sess, &p2c, &err)) {
        fprintf_log(0, "  0 <= ssl_to_p2c()\n");
        handle_e2p(&e2p);
        handle_p2c(&p2c);
        fprintf_log(0, "    e2p, p2c handled, goto prep_next\n");
        goto prep_next;
      }

      fprintf_log(0, " ssl_to_p2c() < 0, ssl_rd_got_eof\n");
      ssl_rd_got_eof = 1;
      break;
    } else
    if (fdpb_has_data(&c2p)) {
      fprintf_log(0, "c2p has some data\n");
      if (0 > c2p_to_ssl(sess, &c2p, &err)) {
        fprintf_log(0, "  failure in c2p_to_ssl(), ssl_rd_got_eof\n");
        ssl_rd_got_eof = 1;
        break;
      }

      if (err.ssl != SSL_ERROR_WANT_READ && 0 == sess->get_pending(sess->ssl)) {
        fprintf_log(0, "nothing to read after writing\n");
        goto prep_next;
      }

      if (fdpb_is_valid(&p2c)) {
        fprintf_log(0, "p2c is valid\n");
        if (0 > ssl_to_p2c(sess, &p2c, &err))
          ssl_rd_got_eof = 1;
      } else {
        fprintf_log(0, "p2c is invalid\n");
        if (0 > discard_ssl_pended_data(sess, &err))
          ssl_rd_got_eof = 1;
      }

      fprintf_log(0, "ssl_rd_got_eof = %d\n", ssl_rd_got_eof);
      if (ssl_rd_got_eof)
        break;

      goto prep_next;

    } else
    if (fdpb_is_valid(&c2p) && c2p.ready) {
      fprintf_log(0, "c2p is ready\n");
      // SSL is not confirmed to be ready to write, but read from c2p to its internal buffer
      fdpb_read_internal(&c2p);

      fprintf_log(0, "fdpb_read_internal(c2p) got %zu bytes, total %zu bytes\n", c2p.len_chunk, c2p.len);
      if (c2p.got_eof) {
        fprintf_log(0, " c2p got EOF\n");
      } else
      if (0 > c2p_to_ssl(sess, &c2p, &err)) {
        ssl_rd_got_eof = 1;
        break;
      }
    }

    close_dead_pipes(&p2c, &c2p, &e2p);

    if (child_finished && !fdpb_is_valid(&p2c) && !fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "child process is finished, p2c, c2p, e2p are closed, break select() loop\n");
      break;
    }

prep_next:
    if (!fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "c2p & e2p are closed, exit loop\n");
      break;
    }

    fprintf_log(20, "prep for next select():");
    FD_ZERO(&fds_rd);
    if (fdpb_is_valid(&c2p)) {
      fprintf_log(21, " c2p");
      FD_SET(c2p.fd, &fds_rd);
    }
    if (fdpb_is_valid(&e2p)) {
      fprintf_log(21, " e2p");
      FD_SET(e2p.fd, &fds_rd);
    }
    if (ssl_err_is_retryable(err.ssl) && !ssl_rd_got_eof) {
      fprintf_log(21, " ssl_rd");
      FD_SET(sess->rfd, &fds_rd);
    }
    fprintf_log(21, "\n");

    set_timeval(&tv, sess->timeout->select_rd, 0);
  }
}

int main(const int argc, char* const* argv)
{
  int r = 0;
  propn_prog_opt_st prog_opt = PROPN_PROG_OPT_ST_NULL;

  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port = DEFAULT_PORT;
  prog_opt.timeout_accept = 60;
  prog_opt.timeout_select = 60;
  propn_parse_args(argc, argv, &prog_opt);
  if (prog_opt.buffsize > 0)
    pipe_buff_size = prog_opt.buffsize;

  debug_level = prog_opt.debug_level;

  /* Ignore broken pipe signals */
  // signal(SIGPIPE, SIG_IGN);
  signal(SIGPIPE, update_child_finished);
  signal(SIGCHLD, update_child_finished);

  if (prog_opt.pathname_log) {
    unlink(prog_opt.pathname_log);
    fh_log = fopen(prog_opt.pathname_log, "w");
  } else
    fh_log = stderr;

  if (fh_log)
    fflush(fh_log);

  /* Handle connections */
  {
    pid_t pid;

    trfd_s trfd_prnt = { 0, 0, 0 }, trfd_chld = { 0, 0, 0 };

    if (trfd_open( TRIFIDES_AUTO, &trfd_prnt, &trfd_chld )) {
      fprintf_log(0, "failure in 3-file descriptors between parent and child STDIO/STDERR\n");
      r = -1;
      goto log_close;
    }

    pid = fork();
    switch (pid) {
      case -1:
        r = -2;
        break;

      case 0:
        /* child process */
        trfd_close_all(&trfd_prnt);

        propn_uidgid(&prog_opt, 1);

        trfd_xfer_012(&trfd_chld);

        pathexec(argv + (prog_opt.index_last_parsed_argv + 1));
        break;

      default:
        /* parent process, should keep SSL pipes until child process exit */
        trfd_close_all(&trfd_chld);

        {
          int  ret = 0, sock = -1, client = -1;
          WOLFSSL_CTX *ctx = NULL;
          WOLFSSL     *ssl = NULL;
          ssl_sess_t   sess;
          ssl_timeout_t sess_to = {prog_opt.timeout_accept, prog_opt.timeout_select, prog_opt.timeout_select};

          if (propn_chroot(&prog_opt)) {
            fprintf_log(0, "chroot failed\n");
          }

          propn_uidgid(&prog_opt, 0);

          ssl_sess_clear(&sess);
          ssl_sess_init_err_sym_wssl(&sess);
          sess.timeout = &sess_to;
          ssl = setup_ssl(&prog_opt, &client, &sock, &ctx, &sess);
          if (!ssl) {
            fprintf_log(0, "setup_ssl() returns NULL, failed to create SSL session\n");
            goto bail_out;
          }

          ret = wolfSSL_accept(sess.ssl);
          if (SSL_SUCCESS != ret) {
            fprintf_log(0, "wolfSSL_accept() returns %s\n", sess.get_err_sym(ret));
            goto bail_out;
          } else {
            /* stderr from client should be written somewhere, even if no debug mode */
            if (!fh_log)
              fh_log = stderr;

            proc_parent(&trfd_prnt, &sess);
            fprintf_log(0, "parental loop is finished\n");
            goto bail_out;
          }

bail_out:
          if (!child_finished) {
            fprintf_log(0, "terminate child process pid=%d\n", pid);
            kill(pid, SIGTERM);
          }
          if (ssl) {
            fprintf_log(0, "shutdown SSL session %p\n", ssl);
            wolfSSL_shutdown(sess.ssl);
            wolfSSL_free(sess.ssl);
          }
          if (0 <= client) {
            fprintf_log(0, "close() client %d\n", client);
            close(client);
          }
          if (ctx) {
            fprintf_log(0, "free() SSL context %p\n", ctx);
            wolfSSL_CTX_free(ctx);
          }
          if (0 <= sock) {
            fprintf_log(0, "close() socket %d\n", sock);
            close(sock);
          }
        }
    } /* switch */
  }

log_close:
  if (fh_log)
    fclose(fh_log);

  exit(r);
}
