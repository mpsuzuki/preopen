/*
 * https://wiki.openssl.org/index.php/Simple_TLS_Server
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define DEBUG_WOLFSSL_VERBOSE
#include <wolfssl/openssl/ssl.h>
#include <wolfssl/openssl/err.h>

#include "preopen-cli.h"

#ifndef MY_UNUSED
# define MY_UNUSED(a) ((void)(a))
#endif

#define BIO_LOAD_STACKSIZE 1024
FILE* fh_log = NULL;
int   debug_level = 0;
#define DEFAULT_PORT 10443

int create_socket(int port)
{
  int s;
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    if (fh_log)
      fprintf(fh_log, "Unable to create socket");
    perror("Unable to create socket");
    exit(EXIT_FAILURE);
  }

  if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    if (fh_log)
      fprintf(fh_log, "Unable to bind");
    exit(EXIT_FAILURE);
  }

  if (listen(s, 1) < 0) {
    if (fh_log)
      fprintf(fh_log, "Unable to listen");
    exit(EXIT_FAILURE);
  }

  if (fh_log)
    fprintf(fh_log, "Listen at port %d\n", port);

  return s;
}

WOLFSSL_CTX *create_context()
{
  WOLFSSL_CTX *ctx = wolfSSL_CTX_new(wolfTLS_server_method());
  if (!ctx) {
    fprintf_log(0, "Unable to create SSL context\n");
    exit(EXIT_FAILURE);
  }

  return ctx;
}

int ctx_use_cert_pkey_fd2(WOLFSSL_CTX* ctx,
                          int fd_srv_cert,
                          int fd_srv_pkey,
                          size_t max_size_pem)
{
  int ret = -1;

  size_t buff_srv_cert_len = max_size_pem, buff_srv_pkey_len = max_size_pem;
  unsigned char* buff_srv_cert = propn_read_fd(fd_srv_cert, &buff_srv_cert_len);
  unsigned char* buff_srv_pkey = propn_read_fd(fd_srv_pkey, &buff_srv_pkey_len);

  if (!buff_srv_cert || !buff_srv_pkey)
    goto bail_out;

  ret = wolfSSL_CTX_use_certificate_chain_buffer(ctx, buff_srv_cert, buff_srv_cert_len);
  if (fh_log) {
    fprintf(fh_log, "wolfSSL_CTX_use_certificate_chain_buffer() returns %d (%s)\n",
            ret, (ret == 1) ? "Success" : "Failed" );
  }
  if (ret != 1)
    goto bail_out;

  ret = wolfSSL_CTX_use_PrivateKey_buffer(ctx, buff_srv_pkey, buff_srv_pkey_len, WOLFSSL_FILETYPE_PEM);
  if (fh_log) {
    fprintf(fh_log, "wolfSSL_CTX_use_PrivateKey_buffer() returns %d (%s)\n",
            ret, (ret == 1) ? "Success" : "Failed" );
  }

bail_out:
  if (buff_srv_cert) {
    memset(buff_srv_cert, 0, buff_srv_cert_len);
    free(buff_srv_cert);
  }

  if (buff_srv_pkey) {
    memset(buff_srv_pkey, 0, buff_srv_pkey_len);
    free(buff_srv_pkey);
  }

  return ret;
}

int
stdout_write(WOLFSSL* ssl, char* data, int data_size, void* ctx)
{
  MY_UNUSED(ctx);
  if (!ssl)
    return 0;
  return write(1, data, data_size);
}

int
stdin_read(WOLFSSL* ssl,  char* data, int data_size, void* ctx)
{
  MY_UNUSED(ctx);
  if (!ssl)
    return 0;
  return read(0, data, data_size);
}

int main(const int argc, char* const* argv)
{
  int ret = -1;
  int sock = -1;
  int fd_srv_cert = -1;
  int fd_srv_pkey = -1;
  WOLFSSL_CTX *ctx;

  propn_prog_opt_st prog_opt = PROPN_PROG_OPT_ST_NULL;
  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port = DEFAULT_PORT;
  propn_parse_args(argc, argv, &prog_opt);

  /* Ignore broken pipe signals */
  signal(SIGPIPE, SIG_IGN);

  if (prog_opt.pathname_log)
    fh_log = fopen(prog_opt.pathname_log, "w");
  if (!fh_log && prog_opt.port > 0)
    fh_log = stderr;
  propn_chroot(&prog_opt);

  ctx = create_context();

  fd_srv_cert = propn_get_fd_by_var_name(prog_opt.envkey_srv_cert);
  fprintf_log(0, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, prog_opt.envkey_srv_cert);

  fd_srv_pkey = propn_get_fd_by_var_name(prog_opt.envkey_srv_pkey);
  fprintf_log(0, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, prog_opt.envkey_srv_pkey);

  ret = ctx_use_cert_pkey_fd2(ctx, fd_srv_cert, fd_srv_pkey, prog_opt.max_size_pem);
  fprintf_log(0, "ctx_use_cert_pkey_fd2() returns %d (%s)\n",
                  ret, (ret == 1) ? "Success" : "Failed" );

  if (prog_opt.port > 0)
    sock = create_socket(prog_opt.port);

  /* Handle connections */
  while(1) {
    WOLFSSL *ssl;
    const char reply[] = "test\n";
    int client;

    ssl = wolfSSL_new(ctx);

    if (prog_opt.port > 0) {
      struct sockaddr_in addr;
      unsigned int len = sizeof(addr);

      client = accept(sock, (struct sockaddr*)&addr, &len);
      if (client < 0) {
        fprintf_log(0, "accept() returns %d\n", client);
        exit(EXIT_FAILURE);
      }
      fprintf_log(0, "woldSSL_set_fd(SSL=%p, socket=%d)\n", ssl, client);
      wolfSSL_set_fd(ssl, client);
    } else {
      wolfSSL_SSLSetIORecv(ssl, stdin_read);
      ret = wolfSSL_set_read_fd(ssl, 0);
      fprintf_log(0, "SSL_set_read_fd(SSL=%p, 0) returns %d\n", ssl, ret);

      wolfSSL_SSLSetIOSend(ssl, stdout_write);
      ret = wolfSSL_set_write_fd(ssl, 1);
      fprintf_log(0, "SSL_set_write_fd(SSL=%p, 1) returns %d\n", ssl, ret);
    }

    ret = wolfSSL_accept(ssl);
    if (0 < ret) {
      wolfSSL_write(ssl, reply, strlen(reply));
    } else {
      char buff_err_str[256];
      int err_code = wolfSSL_get_error(ssl, ret);
      wolfSSL_ERR_error_string_n(err_code, buff_err_str, 256);
      fprintf_log(0, "wolfSSL() failed: code=%d: %s\n", ret, buff_err_str);
    }

    wolfSSL_shutdown(ssl);
    wolfSSL_free(ssl);
    if (prog_opt.port > 0)
      close(client);
    break;
  }

  if (0 < sock)
    close(sock);
  wolfSSL_CTX_free(ctx);
  if (fh_log)
    fclose(fh_log);
}
