#ifndef SESS_WSSL_H
#define SESS_WSSL_H

#include <wolfssl/ssl.h>
#include "sslexec-help-sess.h"

ssl_sess_t* ssl_sess_fill_wssl(ssl_sess_t*, WOLFSSL*, int);
ssl_sess_t* ssl_sess_init_err_sym_wssl(ssl_sess_t*);

#endif
