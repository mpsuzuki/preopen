#include <wolfssl/ssl.h>

/* for wolfSSL_ErrorCodes */
#include <wolfssl/error-ssl.h>

#include "fdpb.h"

#include "sslexec-help-sess.h"

/* str_len() */
#include "str.h"

ssize_t wssl_read(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  MY_UNUSED(d);
  return wolfSSL_read((WOLFSSL*)ssl, buff, (int)len);
}

ssize_t wssl_write(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  MY_UNUSED(d);
  return wolfSSL_write((WOLFSSL*)ssl, buff, (int)len);
}

ssize_t wssl_get_pending(void* ssl)
{
  return wolfSSL_pending((WOLFSSL*)ssl);
}

int wssl_get_ssl_err(void* ssl, int ssl_ret)
{
  return wolfSSL_get_error((WOLFSSL*)ssl, ssl_ret);
}

typedef struct {
  int          code;
  const char*  sym;
} tuple_err_code_sym;

static tuple_err_code_sym wssl_err_array[] = {
  { WOLFSSL_ERROR_NONE,         "WOLFSSL_ERROR_NONE" },
  { WOLFSSL_ERROR_WANT_READ,    "WOLFSSL_ERROR_WANT_READ" },
  { WOLFSSL_ERROR_WANT_WRITE,   "WOLFSSL_ERROR_WANT_WRITE" },
  { WOLFSSL_ERROR_WANT_CONNECT, "WOLFSSL_ERROR_WANT_CONNECT" },
  { WOLFSSL_ERROR_WANT_ACCEPT,  "WOLFSSL_ERROR_WANT_ACCEPT" },
  { WOLFSSL_ERROR_SYSCALL,      "WOLFSSL_ERROR_SYSCALL" },
  { WOLFSSL_ERROR_WANT_X509_LOOKUP, "WOLFSSL_ERROR_WANT_X509_LOOKUP" }, 
  { WOLFSSL_ERROR_ZERO_RETURN,  "WOLFSSL_ERROR_ZERO_RETURN" },
  { WOLFSSL_ERROR_SSL,          "WOLFSSL_ERROR_SSL" },


  /* wolfSSL_ErrorCodes defined in wolfssl/errorssl.h */

  { INPUT_CASE_ERROR,		"INPUT_CASE_ERROR" },
  { PREFIX_ERROR,		"PREFIX_ERROR" },
  { MEMORY_ERROR,		"MEMORY_ERROR" },
  { VERIFY_FINISHED_ERROR,	"VERIFY_FINISHED_ERROR" },
  { VERIFY_MAC_ERROR,		"VERIFY_MAC_ERROR" },
  { PARSE_ERROR,		"PARSE_ERROR" },
  { UNKNOWN_HANDSHAKE_TYPE,	"UNKNOWN_HANDSHAKE_TYPE" },
  { SOCKET_ERROR_E,		"SOCKET_ERROR_E" },
  { SOCKET_NODATA,		"SOCKET_NODATA" },
  { INCOMPLETE_DATA,		"INCOMPLETE_DATA" },
  { UNKNOWN_RECORD_TYPE,	"UNKNOWN_RECORD_TYPE" },
  { DECRYPT_ERROR,		"DECRYPT_ERROR" },
  { FATAL_ERROR,		"FATAL_ERROR" },
  { ENCRYPT_ERROR,		"ENCRYPT_ERROR" },
  { FREAD_ERROR,		"FREAD_ERROR" },
  { NO_PEER_KEY,		"NO_PEER_KEY" },
  { NO_PRIVATE_KEY,		"NO_PRIVATE_KEY" },
  { RSA_PRIVATE_ERROR,		"RSA_PRIVATE_ERROR" },
  { NO_DH_PARAMS,		"NO_DH_PARAMS" },
  { BUILD_MSG_ERROR,		"BUILD_MSG_ERROR" },
  { BAD_HELLO,			"BAD_HELLO" },
  { DOMAIN_NAME_MISMATCH,	"DOMAIN_NAME_MISMATCH" },
  { WANT_READ,			"WANT_READ" },
  { NOT_READY_ERROR,		"NOT_READY_ERROR" },
  { IPADDR_MISMATCH,		"IPADDR_MISMATCH" },
  { VERSION_ERROR,		"VERSION_ERROR" },
  { WANT_WRITE,			"WANT_WRITE" },
  { BUFFER_ERROR,		"BUFFER_ERROR" },
  { VERIFY_CERT_ERROR,		"VERIFY_CERT_ERROR" },
  { VERIFY_SIGN_ERROR,		"VERIFY_SIGN_ERROR" },
  { CLIENT_ID_ERROR,		"CLIENT_ID_ERROR" },
  { SERVER_HINT_ERROR,		"SERVER_HINT_ERROR" },
  { PSK_KEY_ERROR,		"PSK_KEY_ERROR" },

  { GETTIME_ERROR,		"GETTIME_ERROR" },
  { GETITIMER_ERROR,		"GETITIMER_ERROR" },
  { SIGACT_ERROR,		"SIGACT_ERROR" },
  { SETITIMER_ERROR,		"SETITIMER_ERROR" },
  { LENGTH_ERROR,		"LENGTH_ERROR" },
  { PEER_KEY_ERROR,		"PEER_KEY_ERROR" },
  { ZERO_RETURN,		"ZERO_RETURN" },
  { SIDE_ERROR,			"SIDE_ERROR" },
  { NO_PEER_CERT,		"NO_PEER_CERT" },
  { ECC_CURVETYPE_ERROR,	"ECC_CURVETYPE_ERROR" },
  { ECC_CURVE_ERROR,		"ECC_CURVE_ERROR" },
  { ECC_PEERKEY_ERROR,		"ECC_PEERKEY_ERROR" },
  { ECC_MAKEKEY_ERROR,		"ECC_MAKEKEY_ERROR" },
  { ECC_EXPORT_ERROR,		"ECC_EXPORT_ERROR" },
  { ECC_SHARED_ERROR,		"ECC_SHARED_ERROR" },
  { NOT_CA_ERROR,		"NOT_CA_ERROR" },

  { BAD_CERT_MANAGER_ERROR,	"BAD_CERT_MANAGER_ERROR" },
  { OCSP_CERT_REVOKED,		"OCSP_CERT_REVOKED" },
  { CRL_CERT_REVOKED,		"CRL_CERT_REVOKED" },
  { CRL_MISSING,		"CRL_MISSING" },
  { MONITOR_SETUP_E,		"MONITOR_SETUP_E" },
  { THREAD_CREATE_E,		"THREAD_CREATE_E" },
  { OCSP_NEED_URL,		"OCSP_NEED_URL" },
  { OCSP_CERT_UNKNOWN,		"OCSP_CERT_UNKNOWN" },
  { OCSP_LOOKUP_FAIL,		"OCSP_LOOKUP_FAIL" },
  { MAX_CHAIN_ERROR,		"MAX_CHAIN_ERROR" },
  { COOKIE_ERROR,		"COOKIE_ERROR" },
  { SEQUENCE_ERROR,		"SEQUENCE_ERROR" },
  { SUITES_ERROR,		"SUITES_ERROR" },

  { OUT_OF_ORDER_E,		"OUT_OF_ORDER_E" },
  { BAD_KEA_TYPE_E,		"BAD_KEA_TYPE_E" },
  { SANITY_CIPHER_E,		"SANITY_CIPHER_E" },
  { RECV_OVERFLOW_E,		"RECV_OVERFLOW_E" },
  { GEN_COOKIE_E,		"GEN_COOKIE_E" },
  { NO_PEER_VERIFY,		"NO_PEER_VERIFY" },
  { FWRITE_ERROR,		"FWRITE_ERROR" },
  { CACHE_MATCH_ERROR,		"CACHE_MATCH_ERROR" },
  { UNKNOWN_SNI_HOST_NAME_E,	"UNKNOWN_SNI_HOST_NAME_E" },
  { UNKNOWN_MAX_FRAG_LEN_E,	"UNKNOWN_MAX_FRAG_LEN_E" },
  { KEYUSE_SIGNATURE_E,		"KEYUSE_SIGNATURE_E" },

  { KEYUSE_ENCIPHER_E,		"KEYUSE_ENCIPHER_E" },
  { EXTKEYUSE_AUTH_E,		"EXTKEYUSE_AUTH_E" },
  { SEND_OOB_READ_E,		"SEND_OOB_READ_E" },
  { SECURE_RENEGOTIATION_E,	"SECURE_RENEGOTIATION_E" },
  { SESSION_TICKET_LEN_E,	"SESSION_TICKET_LEN_E" },
  { SESSION_TICKET_EXPECT_E,	"SESSION_TICKET_EXPECT_E" },
  { SCR_DIFFERENT_CERT_E,	"SCR_DIFFERENT_CERT_E" },
  { SESSION_SECRET_CB_E,	"SESSION_SECRET_CB_E" },
  { NO_CHANGE_CIPHER_E,		"NO_CHANGE_CIPHER_E" },
  { SANITY_MSG_E,		"SANITY_MSG_E" },
  { DUPLICATE_MSG_E,		"DUPLICATE_MSG_E" },
  { SNI_UNSUPPORTED,		"SNI_UNSUPPORTED" },
  { SOCKET_PEER_CLOSED_E,	"SOCKET_PEER_CLOSED_E" },
  { BAD_TICKET_KEY_CB_SZ,	"BAD_TICKET_KEY_CB_SZ" },
  { BAD_TICKET_MSG_SZ,		"BAD_TICKET_MSG_SZ" },
  { BAD_TICKET_ENCRYPT,		"BAD_TICKET_ENCRYPT" },
  { DH_KEY_SIZE_E,		"DH_KEY_SIZE_E" },
  { SNI_ABSENT_ERROR,		"SNI_ABSENT_ERROR" },
  { RSA_SIGN_FAULT,		"RSA_SIGN_FAULT" },
  { HANDSHAKE_SIZE_ERROR,	"HANDSHAKE_SIZE_ERROR" },
  { UNKNOWN_ALPN_PROTOCOL_NAME_E,	"UNKNOWN_ALPN_PROTOCOL_NAME_E" },
  { BAD_CERTIFICATE_STATUS_ERROR,	"BAD_CERTIFICATE_STATUS_ERROR" },
  { OCSP_INVALID_STATUS,	"OCSP_INVALID_STATUS" },
  { OCSP_WANT_READ,		"OCSP_WANT_READ" },
  { RSA_KEY_SIZE_E,		"RSA_KEY_SIZE_E" },
  { ECC_KEY_SIZE_E,		"ECC_KEY_SIZE_E" },
  { DTLS_EXPORT_VER_E,		"DTLS_EXPORT_VER_E" },
  { INPUT_SIZE_E,		"INPUT_SIZE_E" },
  { CTX_INIT_MUTEX_E,		"CTX_INIT_MUTEX_E" },
  { EXT_MASTER_SECRET_NEEDED_E,	"EXT_MASTER_SECRET_NEEDED_E" },
  { DTLS_POOL_SZ_E,		"DTLS_POOL_SZ_E" },
  { DECODE_E,			"DECODE_E" },
  { HTTP_TIMEOUT,		"HTTP_TIMEOUT" },
  { WRITE_DUP_READ_E,		"WRITE_DUP_READ_E" },
  { WRITE_DUP_WRITE_E,		"WRITE_DUP_WRITE_E" },
  { INVALID_CERT_CTX_E,		"INVALID_CERT_CTX_E" },
  { BAD_KEY_SHARE_DATA,		"BAD_KEY_SHARE_DATA" },
  { MISSING_HANDSHAKE_DATA,	"MISSING_HANDSHAKE_DATA" },
  { BAD_BINDER,			"BAD_BINDER" },
  { EXT_NOT_ALLOWED,		"EXT_NOT_ALLOWED" },
  { INVALID_PARAMETER,		"INVALID_PARAMETER" },
  { MCAST_HIGHWATER_CB_E,	"MCAST_HIGHWATER_CB_E" },
  { ALERT_COUNT_E,		"ALERT_COUNT_E" },
  { EXT_MISSING,		"EXT_MISSING" },
  { UNSUPPORTED_EXTENSION,	"UNSUPPORTED_EXTENSION" },
  { PRF_MISSING,		"PRF_MISSING" },
  { DTLS_RETX_OVER_TX,		"DTLS_RETX_OVER_TX" },
  { DH_PARAMS_NOT_FFDHE_E,	"DH_PARAMS_NOT_FFDHE_E" },
  { TCA_INVALID_ID_TYPE,	"TCA_INVALID_ID_TYPE" },
  { TCA_ABSENT_ERROR,		"TCA_ABSENT_ERROR" },
  { TSIP_MAC_DIGSZ_E,		"TSIP_MAC_DIGSZ_E" },
  { CLIENT_CERT_CB_ERROR,	"CLIENT_CERT_CB_ERROR" },
  { SSL_SHUTDOWN_ALREADY_DONE_E,	"SSL_SHUTDOWN_ALREADY_DONE_E" },
  { TLS13_SECRET_CB_E,		"TLS13_SECRET_CB_E" },
  { DTLS_SIZE_ERROR,		"DTLS_SIZE_ERROR" },
  { NO_CERT_ERROR,		"NO_CERT_ERROR" },
  { APP_DATA_READY,		"APP_DATA_READY" },
  { TOO_MUCH_EARLY_DATA,	"TOO_MUCH_EARLY_DATA" },
  { SOCKET_FILTERED_E,		"SOCKET_FILTERED_E" },
  { HTTP_RECV_ERR,		"HTTP_RECV_ERR" },
  { HTTP_HEADER_ERR,		"HTTP_HEADER_ERR" },
  { HTTP_PROTO_ERR,		"HTTP_PROTO_ERR" },
  { HTTP_STATUS_ERR,		"HTTP_STATUS_ERR" },
  { HTTP_VERSION_ERR,		"HTTP_VERSION_ERR" },
  { HTTP_APPSTR_ERR,		"HTTP_APPSTR_ERR" },
  { UNSUPPORTED_PROTO_VERSION,	"UNSUPPORTED_PROTO_VERSION" },
  { FALCON_KEY_SIZE_E,		"FALCON_KEY_SIZE_E" },

  { UNSUPPORTED_SUITE,		"UNSUPPORTED_SUITE" },
  { MATCH_SUITE_ERROR,		"MATCH_SUITE_ERROR" },
  { COMPRESSION_ERROR,		"COMPRESSION_ERROR" },
  { KEY_SHARE_ERROR,		"KEY_SHARE_ERROR" },
  { POST_HAND_AUTH_ERROR,	"POST_HAND_AUTH_ERROR" },
  { HRR_COOKIE_ERROR,		"HRR_COOKIE_ERROR" },

  { 0, NULL }
};

/* we assume macro name lenth is shorter than WOLFSSL_MAX_ERROR_SZ */
static char wssl_err_string[(WOLFSSL_MAX_ERROR_SZ) * 2];

const char* wssl_get_err_sym(int err)
{
  char* p;
  int i;

  memset(wssl_err_string, 0, (WOLFSSL_MAX_ERROR_SZ) * 2);

  for (i = 0; wssl_err_array[i].sym != NULL; i++) {
    if (wssl_err_array[i].code == err) {
      // return wssl_err_array[i].sym;
      snprintf(wssl_err_string, (WOLFSSL_MAX_ERROR_SZ) * 2, "%s: ", wssl_err_array[i].sym);
      break;
    }
  }
  p = wssl_err_string + str_len(wssl_err_string);
  wolfSSL_ERR_error_string_n(err, p, (WOLFSSL_MAX_ERROR_SZ) * 2 - (wssl_err_string - p));

  if (str_len(wssl_err_string) == 0)
    return NULL;

  return wssl_err_string;
}

ssl_sess_t* ssl_sess_fill_wssl(ssl_sess_t* sess, WOLFSSL* ssl, int auto_fds)
{
  if (!sess || !ssl)
    return NULL;

  sess->ssl = ssl;
  if (auto_fds) {
    sess->rfd = wolfSSL_get_fd(ssl); // no wolfSSL_get_rfd()
    sess->wfd = wolfSSL_get_fd(ssl); // no wolfSSL_get_wfd()
  }
  sess->read  = wssl_read;
  sess->write = wssl_write;
  sess->get_pending = wssl_get_pending;
  sess->get_ssl_err = wssl_get_ssl_err;
  sess->get_err_sym = wssl_get_err_sym;

  return sess;
}

ssl_sess_t* ssl_sess_init_err_sym_wssl(ssl_sess_t* sess)
{
  if (!sess)
    return NULL;

  sess->get_ssl_err = wssl_get_ssl_err;
  sess->get_err_sym = wssl_get_err_sym;

  return sess;
}
