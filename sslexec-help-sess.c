#if HAVE_CONFIG_H
# include "config.h"
#endif

/* strerror() */
#include <string.h>

/* FreeBSD 4.x (or older), select.h needs sys/types.h */
#if HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif

/* FreeBSD 4.x (or older), select.h needs sys/time.h */
#if HAVE_SYS_TIME_H
# include <sys/time.h>
#endif

#include <sys/select.h>

#include "preopen-cli.h"
#include "fdpb.h"

#include "sslexec-help-sess.h"

ssl_sess_t* ssl_sess_clear(ssl_sess_t* sess)
{
  if (!sess)
    return NULL;
  sess->ssl = NULL;
  sess->rfd = -1;
  sess->wfd = -1;
  sess->timeout = NULL;
  sess->read  = NULL;
  sess->write = NULL;
  sess->get_pending = NULL;
  sess->get_ssl_err = NULL;
  sess->get_err_sym = NULL;

  return sess;
}

void log_ssl_error(ssl_sess_t* sess, int ssl_ret, int newline)
{
  int ssl_err = sess->get_ssl_err(sess->ssl, ssl_ret);
  const char* sym = sess->get_err_sym(ssl_err);
  fprintf_log(0, "%s", sym ? sym : "Unknown");
  if (newline)
    fprintf_log(0, "\n");
}

void tuple_err_update(ssl_sess_t* sess, sess_data_t* data, tuple_err_t* err)
{
  void* ssl;

  if (!sess || !err)
    return;

  ssl = (void*)(sess->ssl);
  err->ssl = sess->get_ssl_err(ssl, data->i);
  if (err->ssl) {
    const char* sym = sess->get_err_sym(err->ssl);
    fprintf_log(0, "SSL got an error %d: %s\n", err->ssl, sym ? sym : "Unknown");
  }

  if (err->ssl == 0)
    err->sys = errno;
  else
    err->sys = 0;

  return;
}

size_t write_fdpb_to_ssl(ssl_sess_t* sess, fdpb_t* f, tuple_err_t* err)
{
  int  ssl_ret = 0;
  void* ssl;
  sess_data_t sess_data = { 0 };

  if (!err)
    return 0;
  tuple_err_clear(err);

  if (!sess || !f) {
    err->sys = EINVAL;
    return 0;
  }

  ssl = (void*)(sess->ssl);

  while (0 < f->len) {
    int r = fdpb_wait_select_fd_external(f, sess->wfd, 'w');
    if (r <= 0) {
      fprintf_log(0, "      fdpb_wait_select_fd_external() returned %d, cannot write-out\n", r);
      break;
    }
    ssl_ret = sess->write(ssl, f->buff, f->len, NULL);
    if (0 < ssl_ret) {
      fprintf_log(0, "      parent writes %d bytes to SSL: ", ssl_ret);
      log_escaped_str(0, f->buff, ssl_ret);

      fdpb_shift(f, ssl_ret);

    } else {
      fprintf_log(0, "      parent got an error in writing to SSL: ");
      log_ssl_error(sess, ssl_ret, 1);

      sess_data.i = ssl_ret;
      tuple_err_update(sess, &sess_data, err);
      if (!ssl_err_is_retryable(err->ssl))
        return 0;
    }
  }

  return ssl_ret;
}

size_t read_ssl_to_fdpb(ssl_sess_t* sess, fdpb_t *f, int* has_pended_data, size_t limit_extend, tuple_err_t* err)
{
  void* ssl;
  size_t len0;

  if (!err)
    return 0;

  tuple_err_clear(err);
  if (!sess || !f) {
    err->sys = EINVAL;
    return 0;
  }

  ssl = (void*)(sess->ssl);
  len0 = f->len;

  if (!limit_extend)
    limit_extend = f->limit;

  while (f->len < f->limit) {
    int  ssl_ret, len_pend;
    ssize_t sdelta;

    ssl_ret = sess->read(ssl, f->buff + f->len, f->limit - f->len, NULL);

    if (ssl_ret <= 0) {
      sess_data_t d;
      d.i = ssl_ret;
      tuple_err_update(sess, &d, err);

      // XXX: if SSL_ERROR_WANT_READ, we had better continue than break?
      break;
    }

    f->len += ssl_ret;
    f->len_chunk = ssl_ret;

    len_pend = sess->get_pending(ssl);
    if (has_pended_data)
      *has_pended_data = (0 < len_pend);

    if (len_pend == 0)
      break;

    sdelta = len_pend - fdpb_len_room(f);

    /* no need extend, call SSL_read() again */
    if (sdelta < 0)
      continue;

    /* cannot extend to load all pended data */
    if (limit_extend < f->limit + sdelta)
      break;

    /* try to extend */
    sdelta = fdpb_extend(f, (size_t)sdelta);

    /* if delta is negative, it indicates a negated errno    */
    /* from realloc(), which must be ENOMEM (see C89, POSIX) */
    if (sdelta < 0) {
      err->sys = ENOMEM;
      break;
    }
  }

  return f->len - len0;
}

/* read from SSL and write to p2c, but not emit buffered data yet.
 * return 0 if successfully loaded, return -1 if EOF got.
 */
int ssl_to_p2c(ssl_sess_t* sess, fdpb_t* p2c, tuple_err_t* err)
{
  int len_diff;
  int has_pended_data = 0;

  if (!err)
    return 0;
  tuple_err_clear(err);

  if (!sess || !fdpb_is_valid(p2c)) {
    err->sys = EINVAL;
    return 0;
  }

  /* XXX check ssl, p2c are valid */

  switch (fdpb_wait_select_fd_internal(p2c, 'w')) {
  case -1: // p2c broken?
    fprintf_log(0, "ssl_to_p2c(): error in waiting p2c ready to write\n");
    return -1;
  case 0: // timeout. if you want to wait endlessly tv should be NULL
    fprintf_log(0, "ssl_to_p2c(): timeout in waiting for p2c ready to write\n");
    return 0;
  default:
    break;
  }

  len_diff = read_ssl_to_fdpb(sess, p2c, &has_pended_data, MAX_REALLOC_SIZE, err);
  if (len_diff == 0 && err->ssl == 0) {
    fprintf_log(0, "ssl_rd got EOF (%s:%d)\n", __FUNCTION__, __LINE__);
    return -1;
  }
  fprintf_log(0, "read_ssl_to_fdpb() loaded %d bytes\n", len_diff);
  if (0 == len_diff) {
    if (tuple_err_ok(err) || (ssl_err_is_retryable(err->ssl) && !err->sys)) {
      // got EOF from SSL and no data in p2c, close it to finish the child process.
      if (!fdpb_has_data(p2c))
        fdpb_done(p2c);
    } else {
      if (err->ssl) {
        const char* sym = sess->get_err_sym(err->ssl);
        fprintf_log(0, "read_ssl_to_fdpb() got an SSL error: %s\n", sym ? sym : "Unknown");
      } else {
        fprintf_log(0, "read_ssl_to_fdpb() got non-SSL error: %s\n", strerror(err->sys));
      }
      return -1;
    }
  }
  return 0;
}

int discard_ssl_pended_data(ssl_sess_t* sess, tuple_err_t* err)
{
  void* ssl;
  char dustbin[SIZE_CHUNK_DUST];
  int  size_pended_data = SIZE_CHUNK_DUST;

  if (!err)
    return 0;
  tuple_err_clear(err);

  if (!sess) {
    err->sys = EINVAL;
    return EINVAL;
  }

  ssl = (void*)(sess->ssl);

  /*
   * considering the case that no application data is ready
   * but SSL socket/pipe descriptor is "readable",
   * we should not estimate the initial size of the pended data as 0.
   */

  while (0 < (size_pended_data = sess->get_pending(ssl))) {
    int ssl_ret = sess->read(ssl, dustbin, MY_MIN(size_pended_data, SIZE_CHUNK_DUST), NULL);
    err->ssl    = sess->get_ssl_err(ssl, ssl_ret);
    if (ssl_ret == 0 && err->ssl == 0) {
      fprintf_log(0, "ssl_rd got EOF (%s:%d)\n", __FUNCTION__, __LINE__);
      return -1;
    }
    if (ssl_ret < size_pended_data && size_pended_data < SIZE_CHUNK_DUST)
      fprintf_log(0, "SSL_read() loaded pended data partially %d/%d\n", ssl_ret, size_pended_data);
  }
  return 0;
}

int c2p_to_ssl(ssl_sess_t* sess, fdpb_t* c2p, tuple_err_t* err)
{
  // if SSL is writable, write c2p buffer to it
  int ssl_ret;

  if (!err)
    return 0;
  tuple_err_clear(err);

  if (!sess) {
    err->sys = EINVAL;
    return EINVAL;
  }

  // XXX check return value of write_fdpb_to_ssl()
  ssl_ret = write_fdpb_to_ssl(sess, c2p, err);
  fprintf_log(0, "write_fdpb_to_ssl() write %d bytes\n", ssl_ret);
  if (ssl_ret == 0 && !ssl_err_is_retryable(err->ssl))
    return -1;

  return ssl_ret;
}
