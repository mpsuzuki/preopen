#if HAVE_CONFIG_H
# include "config.h"
#endif

/* FreeBSD 4.x (or older), select.h needs sys/types.h */
#if HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif

/* FreeBSD 4.x (or older), select.h needs sys/time.h */
#if HAVE_SYS_TIME_H
# include <sys/time.h>
#endif

/* fd_set, FD_ISSET(), etc */
#include <sys/select.h>

/* strerror() */
#include <string.h>

#include "preopen-cli.h"
#include "fdpb.h"
#include "sslexec-help.h"

/*
 * child_finished is declared as extern int in preopen-cli.h
 */
void update_child_finished(int signum)
{
  fprintf_log(0, "received signal \"%s\" from child process\n", strsignal(signum));
  child_finished = 1;
}

/*
 * what we want to do is "tv = {12, 34};", but it is prohibited except of its initialization.
 */
struct timeval* set_timeval(struct timeval* tv, unsigned int sec, unsigned int usec)
{
  tv->tv_sec  = sec;
  tv->tv_usec = usec;
  return tv;
}

void log_escaped_str(int log_level, char* buff, size_t limit)
{
  char* c = buff;
  while (*c) {
    if (limit && buff + limit <= c)
      break;
    switch (*c) {
    case 0x07: fprintf_log(log_level, "\\a"); break;
    case 0x08: fprintf_log(log_level, "\\b"); break;
    case 0x09: fprintf_log(log_level, "\\t"); break;
    case 0x0A: fprintf_log(log_level, "\\n"); break;
    case 0x0B: fprintf_log(log_level, "\\v"); break;
    case 0x0C: fprintf_log(log_level, "\\f"); break;
    case 0x0D: fprintf_log(log_level, "\\r"); break;
    default:
      if (*c <= 0x1F || 0x7F <= *c)
        fprintf_log(log_level, "\\x%02X ", (char)*c);
      else
        fprintf_log(log_level, "%c", *c);
    }
    c ++;
  }
  fprintf_log(log_level, "\n");
}

void tuple_err_clear(tuple_err_t* err)
{
  if (!err)
    return;

  err->sys = 0;
  err->ssl = 0;
}

int tuple_err_ok(tuple_err_t* err)
{
  if (err && err->sys == 0 && err->ssl == 0)
    return 1;

  return 0;
}

void dump_selected_fds(int p_p2c, int p_c2p, int p_e2p,
                       int ssl_rd, int ssl_wr,
                       fd_set* fds_rd, fd_set* fds_wr)
{
   fprintf_log(10, "returned from select():");
   if (fds_rd && FD_ISSET(p_c2p, fds_rd))
     fprintf_log(10, " c2p");
   if (fds_rd && FD_ISSET(p_e2p, fds_rd))
     fprintf_log(10, " e2p");
   if (fds_wr && FD_ISSET(p_p2c, fds_wr))
     fprintf_log(10, " p2c");
   if (fds_rd && FD_ISSET(ssl_rd, fds_rd))
     fprintf_log(10, " ssl_rd");
   if (fds_wr && FD_ISSET(ssl_wr, fds_wr))
     fprintf_log(10, " ssl_wr");
   fprintf_log(10, "\n");
}

void handle_e2p(fdpb_t* e2p)
{
  // if something is buffered in e2p, dump it to stderr before appending it.
  if (0 < e2p->len) {
    if (fh_log && 0 <= debug_level)
      fwrite(e2p->buff, 1, e2p->len, fh_log);
    fdpb_shift(e2p, e2p->len);
  }

  // if e2p is readable, load it and dump it immediately.
  if (e2p->ready) {
    fprintf_log(0, "e2p is ready: ");
    fdpb_read_internal(e2p);
    fprintf_log(0, "got %zu bytes, got_eof=%d\n", e2p->len, e2p->got_eof);
    if (fh_log && 0 <= debug_level)
      fwrite(e2p->buff, 1, e2p->len, fh_log);
    fdpb_shift(e2p, e2p->len);
  }
}

void close_fdpb_if_eof(fdpb_t* f, char* nm)
{
  if (fdpb_has_data(f)) {
    fprintf_log(1, "%s has %d bytes in buffer (got_eof=%d)\n", nm, f->len, f->got_eof);
    return;
  }

  if (!f->got_eof) {
    fprintf_log(1, "%s has no data, but has not received EOF yet (got_eof=%d)\n", nm, f->got_eof);
    return;
  }

  fprintf_log(0, "%s has already received EOF, and no data is buffered, close %s\n", nm, nm);

  fdpb_done(f);
}

void close_dead_pipes(fdpb_t* p2c, fdpb_t* c2p, fdpb_t* e2p)
{
  // if child is finished, close p2c, regardless with buffered data
  if (child_finished && fdpb_is_valid(p2c) && p2c->fd != c2p->fd) {
    fprintf_log(0, "child process is finished, close p2c\n");
    fdpb_done(p2c);
  }

  close_fdpb_if_eof(c2p, "c2p");
  close_fdpb_if_eof(e2p, "e2p");
}

void handle_p2c(fdpb_t* p2c)
{
  // if p2c is writable, write the buffered data.
  if (!fdpb_is_valid(p2c))
    return;

  if (!fdpb_has_data(p2c))
    return;

  fprintf_log(0, "p2c = {fd=%d, buff=%p, len=%zu, limit=%zu, len_chunk=%zd}\n",
                  p2c->fd, p2c->buff, p2c->len, p2c->limit, p2c->len_chunk);

  while (p2c->len > 0) {
    fdpb_write_internal(p2c);

    if (0 < p2c->len_chunk) {
      fprintf_log(0, "fdpb_write_internal(p2c) write %zd bytes\n", p2c->len_chunk);
      continue;
    } else
    if (p2c->len_chunk < 0) {
      fprintf_log(0, "fdpb_write_internal(p2c) got an error: %s\n", strerror(errno));
    }

    fdpb_done(p2c);
  }
  return;
}
