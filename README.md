PREOPEN
=======

preopen is a small tool designed for the softwares those require
a read-only access to some files out of its chroot environment
or protected by other owners permission. For example, SSL-enabled
softwares requiring an access with the private keys, under DJB-like
environment. Thus, preopen depends qlibs library to avoid ANSI C
string manipulations.

preopen is a small tool designed for software that requires
read-only access to some files out of its chroot environment
or protected by other owners' permission. For example, SSL-enabled
software requires access to private keys under a DJB-like
environment. Thus, preopen depends on qlibs library to avoid ANSI C
string manipulations.

# How to build

After checkout git repository, you should make configre by
GNU autotools.  This package does not use GNU Libtool, so
no need to execute libtoolize.

```
aclocal
autoheader
automake --add-missing
autoconf
```

or, if no GNUmakefile is found in your working directory,
you can do by:

```
make configure
```

If you have OpenSSL command, you can create temporal
self-signed certification PEM files by:
```
make server-crt.pem
```
It generates server-crt.pem and server-privatekey.pem.

# How to use

If preopen is designed to be invoked as:

```
preopen ENV_A=path_a ENV_B=path_b -- command_c command_c_args
```

, command_c would be executed as if it were invoked as:
```
env ENV_A=fdnum_a ENV_B=fdnum_b command_c command_c_args
```

preopen opens the files specified by path_a & path_b (with read-only mode)
and invokes command_c (with arguments specified by command_c_args) with
the revised environmental variables:
ENV_A is revised to be a digit number of the file descriptor pointing the file path_a,
ENV_B is revised to be a digit number of the file descriptor pointing the file path_b.

preopen itself does not have the features to chroot or change UID.
To invoke command_c with different UID, using "setuidgid" in DJB
daemontool is recommended.
preopen does not have the features to chroot or change UID.
To invoke command_c with a different UID, using "setuidgid" in DJB
daemontool is recommended.

# sslexec-xxx

server-xxx (in the subdirectories for OpenSSL, wolfSSL, GnuTLS,
and BearSSL) are examples of initializing an SSL session by SSL
cert files loaded from the file descriptors, and invoke child
process, whose STDIN and STDOUT are connected to the SSL pipes.

If you execute:
```
tcpserver -v 127.0.0.1 10443 \
  ./preopen CERTFILE=server-crt.pem KEYFILE=server-privatekey.pem \
    -- ./openssl/sslexec-openssl \
       -T 600 -t 600 -s CERTFILE -k KEYFILE --debug 100 --stdio \
       -- ./echoback -T 600 -t 600
```

You can connect port 10443 by openssl:
```
openssl s_client -connect 127.0.0.1:10443
```
After the success of the connection, you can type something and
would receive the echoback.

You can invoke DJB publicfile httpd from sslexec-xxx examples.

# Note for each SSL implementation

- BearSSL does not allocate a memory buffer internally,
  its client should allocate a buffer with sufficient size,
  by "-B" option. The default value is 10000.

# chroot

sslexec-xxx (and sslserver-xxx) have "--chroot" option.
To prevent the escape from chroot environment, "--envuidgid" or
"--envuidgid-child" options are useful to reflect UID and GID
environmental variables and drop the privilege.

wolfSSL and BearSSL clients try to open "/dev/random" and
"/dev/urandom", so these files should be available in chroot
destination directory.

GnuTLS samples try to open a bundled CA file, whose pathname
can be manually specified by "-a" option.

suzuki toshiya
