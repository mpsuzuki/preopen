AC_PREREQ([2.62])
AC_INIT([preopen], [0.0.20241031], [https://gitlab.com/mpsuzuki/preopen/-/issues])
AM_INIT_AUTOMAKE([foreign])
AC_CONFIG_SRCDIR([check-fds.c])

# Checks for programs.
AC_PROG_CC
AC_PROG_MAKE_SET
AC_PROG_RANLIB
PKG_PROG_PKG_CONFIG()

# Not official release, build debug binaries by default
AC_ARG_ENABLE([debug],[
AS_HELP_STRING([--disable-debug],
               [build without debug symbols (default is debug build)])
  ],[enable_debug=${enableval}],[enable_debug=yes])
if test "x${enable_debug}" = "xyes"
then
  CFLAGS="-g3 -ggdb -O0 -fkeep-inline-functions -Wall -Wextra"
fi

# Checks for header files.
AC_CHECK_HEADERS([arpa/inet.h fcntl.h netinet/in.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T
AC_TYPE_SSIZE_T

# Checks for library functions.
AC_CHECK_FUNCS([memset strdup strtol strtoul])
AC_CHECK_FUNC([dup2],[],[
  AC_MSG_ERROR([dup2() is required.])
])
AC_CHECK_HEADERS([sys/time.h sys/socket.h],[
  AC_CHECK_FUNC([socketpair],[
    AC_DEFINE([HAVE_SOCKETPAIR],[1],[Define to 1 if you have `socketpair' function.])
  ],[])
  AC_CHECK_DECLS([AF_UNIX, AF_LOCAL],[],[],[#include <sys/socket.h>])
  if test "x${ac_cv_have_decl_AF_UNIX}" != xyes -a "x${ac_cv_have_decl_AF_LOCAL}" = xyes
  then
    CFLAGS_AF_UNIX="-DAF_UNIX=AF_LOCAL"
  fi
],[])
AC_SUBST([CFLAGS_AF_UNIX])


build_libposix2008=no
# getline() is since POSIX 2008, FreeBSD undeclares it
# by default, for the compilation of old softwares
AX_CHECK_POSIX2008_FUNC([getline],[GETLINE_CFLAGS],[have_getline=yes],[have_getline=no])
if test x"${have_getline}" != xyes
then
  AC_MSG_WARN([No getline() in system, use my own getline.c])
  GETLINE_CFLAGS='-I$(srcdir)'
  build_libposix2008=yes
fi
AC_SUBST([GETLINE_CFLAGS])
AM_CONDITIONAL([USE_MY_GETLINE],[test x"${have_getline}" != xyes])

# for strnlen()
AX_CHECK_POSIX2008_FUNC([strnlen],[STRNLEN_CFLAGS],[have_strnlen=yes],[have_strnlen=no])
if test x"${have_strnlen}" != xyes
then
  AC_MSG_WARN([No strnlen() in system, use my own strnlen.c])
  GETLINE_CFLAGS='-I$(srcdir)'
  build_libposix2008=yes
fi
AC_SUBST([STRNLEN_CFLAGS])
AM_CONDITIONAL([USE_MY_STRNLEN],[test x"${have_strnlen}" != xyes])

AM_CONDITIONAL([USE_LIBPOSIX2008],[test x"${build_libposix2008}" = xyes])

# Checks for GNU extension, getopt.h and getopt_long()
AC_CHECK_HEADER([getopt.h],[
  AC_DEFINE([HAVE_GETOPT_H],[1],[Define to 1 if you have <getopt.h>])
  AC_CHECK_FUNC([getopt_long],[
    AC_DEFINE([HAVE_GETOPT_LONG],[1],[Define to 1 if you have getopt_long()])
  ])
])


# for chroot() testing, build static binaries by default
AC_ARG_ENABLE([static],[
AS_HELP_STRING([--disable-static],
               [build dynamic linked binaries (default is static build)])
  ],[enable_static=${enableval}],[enable_static=yes])

# chroot() is since 4.2 BSD, FreeBSD undeclares it by default.
AC_CHECK_FUNCS([chroot])
AC_CHECK_DECL([chroot(const char*)],[
   CHROOT_CFLAGS=""
 ],[
   orig_CFLAGS="${CFLAGS}"
   for ac_chroot_cflags in -D__XSI_VISIBLE=1 -D__BSD_VISIBLE
   do
     CFLAGS="${orig_CFLAGS} ${ac_chroot_cflags}"
     AC_MSG_CHECKING([whether ${ac_chroot_cflags} makes unistd.h declare chroot()])
     AC_COMPILE_IFELSE([
       AC_LANG_PROGRAM([[#include <unistd.h>]],
                       [[
#ifndef chroot
#ifdef __cplusplus
  (void) chroot((const char*)0);
#else
  (void) chroot;
#endif
#endif
                       ]])],[
       AC_MSG_RESULT(yes)
       CHROOT_CFLAGS=${ac_chroot_cflags}
       break
                       ],[
       AC_MSG_RESULT(no)
                       ]
     )
   done
 ],[#include <unistd.h>])
AC_SUBST([CHROOT_CFLAGS])

# fehQlibs has no pkg-config data
CFLAGS_orig="${CFLAGS}"
LIBS_orig="${LIBS}"
AC_ARG_WITH([qlibs],[
AS_HELP_STRING([--with-qlibs=prefix],
               [specify the directory of fehQlibs (headers in prefix/include, libqlibs in prefix/lib)]
    )
  ],
  [QLIBS_CFLAGS="-I${with_qlibs}/include"
   QLIBS_LIBS="-L${with_qlibs}/lib -lqlibs"],
  []
)
AC_ARG_WITH([uninstalled-qlibs],[
AS_HELP_STRING([--with-uninstalled-qlibs=prefix],
               [specify the directory of built but not installed fehQlibs]
  )],
  [QLIBS_CFLAGS="-I${with_uninstalled_qlibs}/include"
   QLIBS_LIBS="-L${with_uninstalled_qlibs}/ -lqlibs"],
  []
)

if test x"${QLIBS_CFLAGS}" != "x" -o x"${QLIBS_LIBS}" != "x"
then
  CFLAGS="${CFLAGS} ${QLIBS_CFLAGS}"
  LIBS="${CFLAGS} ${QLIBS_LIBS}"
fi
AC_CHECK_HEADERS([env.h fmt.h pathexec.h str.h stralloc.h])
AC_CHECK_LIB([qlibs], [stralloc_ready])

# If no fehQlibs is found, use qlibs-mini
AM_CONDITIONAL(USE_QLIBS_MINI, [test "x${ac_cv_lib_qlibs_stralloc_ready}" != xyes])
if test x"${ac_cv_lib_qlibs_stralloc_ready}" != "xyes"
then
  AC_MSG_WARN([No fehQlibs is found, use local copy in qlibs-mini subdirectory])
  # prevent sh-script expantion of srcdir/builddir, use single quote instead of double quite
  QLIBS_CFLAGS='-I$(top_srcdir)/qlibs-mini'
  QLIBS_LIBS='-L$(top_builddir)/qlibs-mini -lqlibs-mini'
fi

CFLAGS="${CFLAGS_orig}"
LIBS="${LIBS_orig}"
AC_SUBST([QLIBS_CFLAGS])
AC_SUBST([QLIBS_LIBS])

# Checks "openssl" command to generate testing PEM files
AC_CHECK_PROGS(OPENSSL_CMD, [openssl])
AM_CONDITIONAL(HAVE_OPENSSL_CMD, [test "x${OPENSSL_CMD}" != x])
AC_SUBST([OPENSSL_CMD])

# Checks for libraries.
# Libraries with PKG-CONFIG files
have_openssl=no
AC_ARG_ENABLE([openssl],[
AS_HELP_STRING([--enable-openssl],
               [let pkg-config search OpenSSL])
  ],[],[enable_openssl=auto])
AC_ARG_ENABLE([openssl-secmem],[
AS_HELP_STRING([--enable-openssl-secmem],
               [use OpenSSL-specific BIO_s_secmem()])
  ],[enable_openssl_secmem="${enableval}"],[enable_openssl_secmem=auto])
#  XXX: check for OpenSSL-compatible libraries: LibreSSL, BoringSSL
if test "x${enable_openssl}" = "xno"
then
  AC_MSG_WARN([OpenSSL support is disabled, do not search])
else
  PKG_CHECK_MODULES([OPENSSL], [openssl], [
    have_openssl=yes
    CFLAGS_orig="${CFLAGS}"
    LIBS_orig="${LIBS}"
    if test "x${enable_static}" = xyes
    then
      AC_MSG_CHECKING([whether static link flag for openssl works])
      CFLAGS="${CFLAGS} "`${PKG_CONFIG} --cflags openssl`
      LIBS="-static ${LIBS} "`${PKG_CONFIG} --libs --static openssl`
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <openssl/ssl.h>]],
                                      [[SSL_CTX_new(TLS_server_method());]])],[
                       AC_MSG_RESULT(yes)
                       OPENSSL_LIBS_STATIC="-static "`${PKG_CONFIG} --libs --static openssl`
                     ],[
                       AC_MSG_RESULT(no)
                     ])
      if test "x${enable_openssl_secmem}" = xno
      then
        AC_MSG_WARN([BIO_s_secmem() is manually disabled])
      elif test "x${have_openssl}" = xyes
      then
        AC_MSG_CHECKING([whether OpenSSL has BIO_s_secmem()])
        AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <openssl/ssl.h>]],
                                        [[BIO_s_secmem();]])],[
                         AC_MSG_RESULT(yes)
                         AC_DEFINE([HAVE_BIO_S_SECMEM],[1],[have OpenSSL-specific BIO_s_secmem()])
                       ],[
                         AC_MSG_RESULT(no)
                       ])
      fi
    fi
    CFLAGS="${CFLAGS_orig}"
    LIBS="${LIBS_orig}"
  ], [have_openssl=no])
fi
AM_CONDITIONAL(HAVE_OPENSSL, [test "x${have_openssl}" = xyes])
AM_CONDITIONAL(HAVE_OPENSSL_STATIC, [test "x${OPENSSL_LIBS_STATIC}" != "x"])
AC_SUBST([OPENSSL_LIBS_STATIC])

# for CAFILE which is used by GnuTLS
AC_ARG_WITH([ca-bundle],[
AS_HELP_STRING([--with-ca-bundle=path_to_ca-bundle_file],
 [default path to bundled CA files used by GnuTLS.
  for detail, see https://curl.se/docs/caextract.html .
  default is "/etc/ssl/certs/ca-certificates.crt"])
],[
  AC_MSG_WARN([Default path to the bundled CA file is set to "${withval}"])
  AC_DEFINE_UNQUOTED([CAFILE],"${withval}",[default path to bundled CA file])
],[])

dnl --------------------------------------------------------------
dnl GnuTLS has so long dependency chain, so pkg-config .pc file is
dnl insufficient to confirm its --cflags & --libs are working well
dnl --------------------------------------------------------------

have_gnutls=no
AC_ARG_ENABLE([gnutls],[
AS_HELP_STRING([--enable-gnutls],
               [let pkg-config search GnuTLS])
  ],[],[enable_gnutls=auto])
if test "x${enable_gnutls}" = "xno"
then
  AC_MSG_WARN([GnuTLS support is disabled, do not search])
else
  PKG_CHECK_MODULES([GNUTLS], [gnutls], [have_gnutls=yes], [have_gnutls=no])
fi

if test "x${have_gnutls}" = "xyes"
then
  CFLAGS_orig="${CFLAGS}"
  LIBS_orig="${LIBS}"

  AC_MSG_CHECKING([whether default link flag for gnutls works])
  CFLAGS="${CFLAGS} "`${PKG_CONFIG} --cflags gnutls`
  LIBS="${LIBS} "`${PKG_CONFIG} --libs gnutls`
  AC_LINK_IFELSE([ AC_LANG_PROGRAM([[#include <gnutls/gnutls.h>]],
                                   [[gnutls_global_init();]])
                 ],[
                   AC_MSG_RESULT(yes)
                 ],[
                   AC_MSG_RESULT(no)
                   have_gnutls=no
                 ])
  CFLAGS="${CFLAGS_orig}"
  LIBS="${LIBS_orig}"
fi

if test "x${have_gnutls}" = "xyes"
then
  AC_MSG_CHECKING([whether static link flag for gnutls works])
  CFLAGS="${CFLAGS} "`${PKG_CONFIG} --cflags gnutls`
  LIBS="-static ${LIBS} "`${PKG_CONFIG} --libs --static gnutls`
  AC_LINK_IFELSE([ AC_LANG_PROGRAM([[#include <gnutls/gnutls.h>]],
                                   [[gnutls_global_init();]])],[
                   AC_MSG_RESULT(yes)
                   GNUTLS_LIBS_STATIC="-static "`${PKG_CONFIG} --libs --static gnutls`
                 ],[
                   AC_MSG_RESULT(no)
                   AC_MSG_CHECKING([whether static link flag for gnutls lacks -lm])
                   # XXX "-lm" is not sufficiently portable, search math library
                   LIBS="${LIBS} -lm"
                   AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <gnutls/gnutls.h>]],
                                                   [[gnutls_global_init();]])],[
                         AC_MSG_RESULT(yes)
                         GNUTLS_LIBS_STATIC="-static "`${PKG_CONFIG} --libs --static gnutls`" -lm"
                   ],[
                     AC_MSG_RESULT(no)
                   ])
                 ])
  CFLAGS="${CFLAGS_orig}"
  LIBS="${LIBS_orig}"
fi
AM_CONDITIONAL(HAVE_GNUTLS, [test "x${have_gnutls}" = xyes])
AM_CONDITIONAL(HAVE_GNUTLS_STATIC, [test "x${GNUTLS_LIBS_STATIC}" != "x"])
AC_SUBST([GNUTLS_LIBS_STATIC])

have_wolfssl=no
AC_ARG_ENABLE([wolfssl],[
AS_HELP_STRING([--enable-wolfssl],
               [let pkg-config search wolfSSL])
  ],[],[enable_wolfssl=auto])
if test "x${enable_wolfssl}" = "xno"
then
  AC_MSG_WARN([wolfSSL support is disabled, do not search])
else
  PKG_CHECK_MODULES([WOLFSSL], [wolfssl], [
    have_wolfssl=yes
    CFLAGS_orig="${CFLAGS}"
    LIBS_orig="${LIBS}"
    if test "x${enable_static}" = xyes
    then
      CFLAGS="${CFLAGS} "`${PKG_CONFIG} --cflags wolfssl`
      LIBS="-static ${LIBS} "`${PKG_CONFIG} --libs --static wolfssl`
      AC_MSG_CHECKING([whether static link flag for wolfssl works])
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <wolfssl/ssl.h>]],
                                      [[wolfSSL_Init();]])],[
                       WOLFSSL_LIBS_STATIC="-static "`${PKG_CONFIG} --libs --static wolfssl`
                       AC_MSG_RESULT(yes)
                     ],[
                       AC_MSG_RESULT(no)

        AC_MSG_CHECKING([whether static link flag for wolfssl lacks -lm])
        # XXX "-lm" is not sufficiently portable, search math library
        LIBS="${LIBS} -lm"
        AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <wolfssl/ssl.h>]],
                                        [[wolfSSL_Init();]])],[
                         AC_MSG_RESULT(yes)
                         WOLFSSL_LIBS_STATIC="-static "`${PKG_CONFIG} --libs --static wolfssl`" -lm"
                       ],[
                         AC_MSG_RESULT(no)

          AC_MSG_CHECKING([for wolfssl-config])
          wolfssl_exec_prefix=`${PKG_CONFIG} --variable=exec_prefix wolfssl`/bin
          WOLFSSL_CONFIG=${wolfssl_exec_prefix}/wolfssl-config
          if test -x ${WOLFSSL_CONFIG}
          then
            AC_MSG_RESULT(yes)
            WOLFSSL_CONFIG_LIBS=`${WOLFSSL_CONFIG} --libs`
            LIBS="${LIBS} ${WOLFSSL_CONFIG_LIBS}"

            AC_MSG_CHECKING([wolfssl-config --libs resolves undefined symbols for static linking])
            AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <wolfssl/ssl.h>]],
                                            [[wolfSSL_Init();]])],[
                              AC_MSG_RESULT(yes)
                              WOLFSSL_LIBS_STATIC="-static "`${PKG_CONFIG} --libs --static wolfssl`" ${WOLFSSL_CONFIG_LIBS} -lm"
                           ],[
                              AC_MSG_RESULT(no)
                           ])
          else
            AC_MSG_RESULT(no)
          fi
        ])
      ])
    fi
    CFLAGS="${CFLAGS_orig}"
    LIBS="${LIBS_orig}"
  ], [have_wolfssl=no])
fi
AM_CONDITIONAL(HAVE_WOLFSSL, [test "x${have_wolfssl}" = xyes])
AM_CONDITIONAL(HAVE_WOLFSSL_STATIC, [test "x${WOLFSSL_LIBS_STATIC}" != "x"])
AC_SUBST([WOLFSSL_LIBS_STATIC])

# BearSSL has no pkg-config data (at least, until 0.6 on 2024-02)
CFLAGS_orig="${CFLAGS}"
LIBS_orig="${LIBS}"
have_bearssl=no
AC_ARG_WITH([bearssl],[
AS_HELP_STRING([--with-bearssl=prefix],
               [specify the directory of BearSSL (headers in prefix/include, libbearssl in prefix/lib)]
    )
  ],[
    case ${with_bearssl} in
    yes|no)
      BEARSSL_CFLAGS=""
      BEARSSL_LIBS=""
      ;;
    *)
      BEARSSL_CFLAGS=""
      AC_MSG_CHECKING([CFLAGS to include "bearssl.h"])
      CFLAGS_orig="${CFLAGS}"
      for bearssl_cpp_flag in \
        "-I${with_bearssl}/include" \
        "-I${with_bearssl}/include/bearssl"
      do
        include_bearssl_h_ok="no"
        CFLAGS="${CFLAGS_orig} ${bearssl_cpp_flag}"
        AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[#include "bearssl.h"]],[[]])],[
          AC_MSG_RESULT(${bearssl_cpp_flag})
          include_bearssl_h_ok="yes"
        ],[])
        if test "x${include_bearssl_h_ok}" = "xyes"
        then
          break
        fi
      done
      if test "x${include_bearssl_h_ok}" = "xyes"
      then
        BEARSSL_CFLAGS="${bearssl_cpp_flag}"
        BEARSSL_LIBS="-L${with_bearssl}/lib"
      else
        with_bearssl="no"
      fi
      CFLAGS="${CFLAGS_orig}"
      ;;
    esac
  ]
)
if test "x${with_bearssl}" = "xno"
then
  have_bearssl=no
  BEARSSL_CFLAGS=""
  BEARSSL_LIBS=""
else
  BEARSSL_LIBS="${BEARSSL_LIBS} -lbearssl"
  CFLAGS="${CFLAGS} ${BEARSSL_CFLAGS}"
  LIBS="${LIBS} ${BEARSSL_LIBS}"
  AC_CHECK_LIB([bearssl], [br_pem_decoder_init], [
    have_bearssl=yes
    if test "x${enable_static}" = xyes
    then
      AC_MSG_CHECKING([whether static link for bearssl works])
      LIBS="-static ${LIBS} ${BEARSSL_LIBS}"
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include "bearssl.h"]],
                                      [[br_sslio_close(NULL);]])],[
                       BEARSSL_LIBS_STATIC="-static ${BEARSSL_LIBS}"
                       AC_MSG_RESULT(yes)
                     ],[
                       AC_MSG_RESULT(no)
                     ])
    fi
  ], [])
fi
CFLAGS="${CFLAGS_orig}"
LIBS="${LIBS_orig}"
AC_SUBST([BEARSSL_CFLAGS])
AC_SUBST([BEARSSL_LIBS])
AC_SUBST([BEARSSL_LIBS_STATIC])
AM_CONDITIONAL(HAVE_BEARSSL, [test "x${have_bearssl}" = xyes])
AM_CONDITIONAL(HAVE_BEARSSL_STATIC, [test "x${BEARSSL_LIBS_STATIC}" != "x"])

AC_CONFIG_HEADERS([config.h:config.h.in])
AC_CONFIG_FILES([GNUmakefile
                 qlibs-mini/GNUmakefile
                 openssl/GNUmakefile
                 wolfssl/GNUmakefile
                 gnutls/GNUmakefile
                 bearssl/GNUmakefile bearssl/brhlp/GNUmakefile
])
AC_OUTPUT
