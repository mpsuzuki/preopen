#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <errno.h>
#include <sys/types.h>
#include <signal.h>

#include <sys/select.h>
#include <fcntl.h>

#include "cintfmt.h"

#include "pathexec.h"

#include "fdpb.h"

#define MY_BUFSIZE 1024
#define MY_MAX(a, b) ((a) > (b) ? (a) : (b))

#define ZU CINTFMT_SIZE_T
#define ZD CINTFMT_SSIZE_T


struct timeval* set_timeval(struct timeval* tv)
{
  tv->tv_sec  = 60; // DJBware sets their timeout to 60 sec
  tv->tv_usec = 0;
  return tv;
}

FILE* flog = NULL;
int child_finished = 0;

void sighandle_parent_ignore(int signum)
 {
  fprintf(stderr, "received signal=%s from child process\n", strsignal(signum));
}

void sighandle_parent_child_finish(int signum)
{
  fprintf(stderr, "received signal=%s from child process\n", strsignal(signum));
  child_finished = 1;
}

void proc_parent(int p_p2c, int p_c2p, int p_e2p)
{
  struct timeval  tv;
  fdpb_t  p2c, c2p, e2p;
  fd_set  fds_rd, fds_rd0;
  fd_set  fds_wr, fds_wr0;
  int     nfds;
 
  FD_ZERO(&fds_rd);
  FD_SET(0, &fds_rd);
  FD_SET(p_c2p, &fds_rd);
  FD_SET(p_e2p, &fds_rd);
  fds_rd0 = fds_rd;

  FD_ZERO(&fds_wr);
  FD_SET(p_p2c, &fds_wr);
  fds_wr0 = fds_wr;

  nfds = 1 + MY_MAX(MY_MAX(p_p2c, p_c2p), p_e2p);

  fdpb_init(&p2c, p_p2c, MY_BUFSIZE);
  fdpb_init(&c2p, p_c2p, MY_BUFSIZE);
  fdpb_init(&e2p, p_e2p, MY_BUFSIZE);

  set_timeval(&tv);

  while (0 != select(nfds, &fds_rd, &fds_wr, NULL, &tv)) {
    int is_stdin_readable = (FD_ISSET(0, &fds_rd) != 0) && !(0 > fcntl(0, F_GETFD));

    fdpb_set_ready(&c2p, &fds_rd);
    fdpb_set_ready(&e2p, &fds_rd);
    fdpb_set_ready(&p2c, &fds_wr);

    if (p2c.ready && 0 < p2c.len && !child_finished) {
      // if some data is stored, write it 
      if (fdpb_write_internal(&p2c) < 0)
        fprintf(stderr, "parent failed to write to p2c\n");
      else
        fprintf(stderr, "parent writes "ZD"/"ZU" bytes to p2c\n", p2c.len_chunk, p2c.len + p2c.len_chunk);

      fflush(stderr);
    }

    if (is_stdin_readable && 0 < fdpb_len_room(&p2c)) {
      // if stdin has some data, read it 
      if (fdpb_read_external(&p2c, 0) < 0) {
        fprintf(stderr, "parent failed to read stdin for p2c\n");
        fclose(stdin);
        fflush(stderr);
        fdpb_done(&p2c);
      } else
      if (0 < p2c.len_chunk) {
        fprintf(stderr, "parent got "ZD" byte from stdin for p2c\n", p2c.len_chunk);
      } else {
        fprintf(stderr, "parent got EOF for stdin, "ZU" byte data is buffered in p2c\n", p2c.len);
        fclose(stdin);
        if (p2c.len == 0) {
          fprintf(stderr, "no buffered data in p2c, close p2c pipe\n");
          fdpb_done(&p2c);
        } 
        fflush(stdout);
        fflush(stderr);
      }
    }

    if (e2p.ready && 0 < fdpb_len_room(&e2p)) {
      // if client error wants to say something, read it
      if (fdpb_read_internal(&e2p) < 0) {
        fprintf(stderr, "parent failed to read from e2p\n");
        fdpb_done(&e2p);
      } else
      if (0 == e2p.len_chunk) {
        fprintf(stderr, "parent got 0 byte from client stderr\n");
        fflush(stderr);
      } else {
        size_t len_chunk;
        fprintf(stderr, "parent got "ZD" byte from client stderr, "ZU" byte buffered in e2p\n", e2p.len_chunk, e2p.len);
        fflush(stderr);
        len_chunk = fwrite(e2p.buff, 1, e2p.len, stderr);
        fdpb_shift(&e2p, len_chunk);
      }
    }

    if (c2p.ready && 0 < fdpb_len_room(&c2p)) {
      // if client wants to say something, read it
      if (fdpb_read_internal(&c2p) < 0) {
        fprintf(stderr, "parent failed to read from c2p\n");
        fdpb_done(&c2p);
      } else
      if (0 == c2p.len_chunk) {
        fprintf(stderr, "parent got 0 byte from client stdout\n");
        fflush(stderr);
      } else {
        size_t len_chunk;
        fprintf(stderr, "parent got "ZD" byte from client stderr, "ZU" byte buffered in c2p\n", c2p.len_chunk, c2p.len);
        fflush(stderr);
        len_chunk = fwrite(c2p.buff, 1, c2p.len, stdout);
        fflush(stdout);
        fdpb_shift(&c2p, len_chunk);
      }
    }

#if 1
    fprintf(stderr, "  child_finished=%d\n", child_finished);
    if (p2c.len) fprintf(stderr, "  len_p2c="ZU"\n", p2c.len);
    if (c2p.len) fprintf(stderr, "  len_c2p="ZU"\n", c2p.len);
    if (e2p.len) fprintf(stderr, "  len_e2p="ZU"\n", e2p.len);
    if (c2p.len_chunk) fprintf(stderr, "  r_c2p="ZD"\n", c2p.len_chunk);
    if (e2p.len_chunk) fprintf(stderr, "  r_e2p="ZD"\n", e2p.len_chunk);
#endif

    if (child_finished && p2c.len == 0 && c2p.len == 0 && e2p.len == 0 && c2p.len_chunk == 0 && e2p.len_chunk == 0) {
      fprintf(stderr, "all buffers are empty and child process is finished\n");
      break;
    }

    fds_rd = fds_rd0;
    fds_wr = fds_wr0;

    set_timeval(&tv);
  }

  fdpb_done(&p2c);
  fdpb_done(&c2p);
  fdpb_done(&e2p);
}

int main(int argc, char** argv)
{
  fd_set  fds;
  int     p_p2c[2] = {0, 0}; // stdin  to client
  int     p_c2p[2] = {0, 0}; // stdout from client
  int     p_e2p[2] = {0, 0}; // stderr from client
  
  if (argc < 2) {
    fprintf(stderr, "argument for child process is needed\n");
    exit(-1);
  }


  FD_ZERO(&fds);
  FD_SET(0, &fds);
  if (select(1, &fds, NULL, NULL, NULL)) {
    pid_t pid;
    if (0 > pipe(p_p2c))
      fprintf(stderr, "pipe() for p2c failed: %s\n", strerror(errno));
    if (0 > pipe(p_c2p))
      fprintf(stderr, "pipe() for c2p failed: %s\n", strerror(errno));
    if (0 > pipe(p_e2p))
      fprintf(stderr, "pipe() for e2p failed: %s\n", strerror(errno));

    pid = fork();
    switch(pid) {
    case -1:
      fprintf(stderr, "fork() failed %s\n", strerror(errno));
      exit(-1);

    case 0:
      /* child process */
      close(p_p2c[1]); // child should not write to  p2c
      close(p_c2p[0]); // child should not read from c2p
      close(p_e2p[0]); // child should not read from e2p

      // remap p2c pipe to stdin
      dup2(p_p2c[0], 0);
      close(p_p2c[0]);

      // remap c2p pipe to stdout
      dup2(p_c2p[1], 1);
      close(p_c2p[1]);

      // remap e2p pipe to stderr
      dup2(p_e2p[1], 2);
      close(p_e2p[1]);

      pathexec(argv + 1);
      break; /* should not reach here */

    default:
      signal(SIGCHLD, sighandle_parent_child_finish);
      signal(SIGPIPE, sighandle_parent_ignore);
      close(p_p2c[0]); // child should not read from p2c
      close(p_c2p[1]); // child should not write to  c2p
      close(p_e2p[1]); // child should not write to  e2p

      proc_parent(p_p2c[1], p_c2p[0], p_e2p[0]);
      break; /* should not reach here */
    }
  }
  exit(-2);
}
