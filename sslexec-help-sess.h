#ifndef SSLEXEC_HELP_SESS_H
#define SSLEXEC_HELP_SESS_H

#include "sslexec-help.h"

typedef union {
  void* ptr;
  long  l;
  int   i;
  short s;
  char  c;
} sess_data_t;

int ssl_err_is_retryable(int);
const char* get_ssl_err2sym(int);

#define MAX_REALLOC_SIZE (2 << 16) 
#define SIZE_CHUNK_DUST  (1024)

#define MY_MAX(a, b) ((a) > (b) ? (a) : (b))
#define MY_MIN(a, b) ((a) < (b) ? (a) : (b))
#define MY_UNUSED(a) ((void)(a))

typedef ssize_t func_ssl_read_t(void*, void*, ssize_t, sess_data_t*);
typedef ssize_t func_ssl_write_t(void*, void*, ssize_t, sess_data_t*);
typedef ssize_t func_ssl_pending_t(void*);
typedef int     func_ssl_err_code_t(void*, int);
typedef const char* func_ssl_err_sym_t(int);

typedef struct {
  int accept;
  int select_rd;
  int select_wr;
} ssl_timeout_t;

typedef struct {
  void*             ssl;
  int               rfd;
  int               wfd;
  ssl_timeout_t*    timeout;
  func_ssl_read_t*  read;
  func_ssl_write_t* write;
  func_ssl_pending_t*  get_pending;
  func_ssl_err_code_t* get_ssl_err;
  func_ssl_err_sym_t*  get_err_sym;
} ssl_sess_t;

ssl_sess_t* ssl_sess_clear(ssl_sess_t*);

void tuple_err_update(ssl_sess_t*, sess_data_t*, tuple_err_t*);
size_t write_fdpb_to_ssl(ssl_sess_t*, fdpb_t*, tuple_err_t*);
size_t read_ssl_to_fdpb(ssl_sess_t*, fdpb_t *, int*, size_t, tuple_err_t*);
int ssl_to_p2c(ssl_sess_t*, fdpb_t*, tuple_err_t*);
int discard_ssl_pended_data(ssl_sess_t*, tuple_err_t*);
int c2p_to_ssl(ssl_sess_t*, fdpb_t*, tuple_err_t*);

#endif
