#ifndef PREOPEN_CLI_H
#define PREOPEN_CLI_H

/* for FILE */
#include <stdio.h>

/*
 * client should have global variable for log and debug_level
 */
extern FILE* fh_log;
extern int debug_level;

/* if (log_min < log_arg) fprintf(fh_log, ...); */
void fprintf_log(int, char*, ...);

/*
 * Get a fd-like integer from specified environmental variable.
 * to avoid the confusion with STDIN, STDOUT, STDERR,
 * the integers 0, 1, and 2 are dealt as wrong numbers.
 */
int propn_get_int_from_env(const char* k);

/*
 * Get a fd-like integer of file descriptor from string.
 * The string is tried as an environmental variable
 * whose value is a digit number, but if it is failed,
 * fallback as a pathname to the file.
 */
int propn_get_fd_by_var_name(const char* s);

/*
 * Allocate new buffer and read the content from
 * the file descriptor.
 * The second argument sets the maximum size of
 * the buffer, and returns the loaded size. If
 * it is NULL, or a pointer to "0", malloc() tries
 * to allocate the size obtained by fstat().
 *
 */
unsigned char* propn_read_fd(int fd, size_t* len);

/* --------------------------------------------------------- */
/*                 B O I L E R   P L A T E                   */
/* --------------------------------------------------------- */

typedef struct {
  char* envkey_ca_cert;
  char* envkey_srv_cert;
  char* envkey_srv_pkey;
  char* envkey_cli_cert;

  unsigned int port;

  size_t max_size_pem;

  char* pathname_chroot;
  char* pathname_log;
  int   debug_level;

  int   index_last_parsed_argv;
  int   reflect_envuidgid;
  int   reflect_envuidgid_child;

  size_t buffsize;
  int   timeout_accept; // timeout for session starting
  int   timeout_select; // timeout for session interruption
} propn_prog_opt_st;

#define PROPN_PROG_OPT_ST_NULL ((propn_prog_opt_st) { NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0 })

void propn_print_help(const char* prog_name);
void propn_parse_args(const int argc, char* const* argv, propn_prog_opt_st* prog_opt);
int propn_chroot(const propn_prog_opt_st* prog_opt);
int propn_uidgid(const propn_prog_opt_st* prog_opt, int is_child);

#endif /* PREOPEN_CLI_H */
