/*
 * https://wiki.openssl.org/index.php/Simple_TLS_Server
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>

/* sockaddr_in */
#include <netinet/in.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

#include "config.h"

#include "preopen-cli.h"

#define BIO_LOAD_STACKSIZE 1024
FILE* fh_log = NULL;
int   debug_level = 0;
#define DEFAULT_PORT 10443

int create_socket(int port)
{
  int s;
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    if (fh_log)
      fprintf(fh_log, "Unable to create socket");
    perror("Unable to create socket");
    exit(EXIT_FAILURE);
  }

  if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    if (fh_log)
      fprintf(fh_log, "Unable to bind");
    exit(EXIT_FAILURE);
  }

  if (listen(s, 1) < 0) {
    if (fh_log)
      fprintf(fh_log, "Unable to listen");
    exit(EXIT_FAILURE);
  }

  if (fh_log)
    fprintf(fh_log, "Listen at port %d\n", port);

  return s;
}

SSL_CTX *create_context()
{
  const SSL_METHOD *method;
  SSL_CTX *ctx;

  method = TLS_server_method();

  ctx = SSL_CTX_new(method);
  if (!ctx) {
      perror("Unable to create SSL context");
      if (fh_log)
        ERR_print_errors_fp(fh_log);
      exit(EXIT_FAILURE);
  }

  return ctx;
}

/*
 * Get OpenSSL's BIO stream from a file descriptor
 */
int get_bio_from_fd(BIO** bio_p, int fd)
{
  if (bio_p == NULL || fd < 3)
    return -1;

#if HAVE_BIO_S_SECMEM
  *bio_p = BIO_new(BIO_s_secmem());
#else
  *bio_p = BIO_new(BIO_s_mem());
#endif
  if (!(*bio_p)) {
    if (fh_log)
#if HAVE_BIO_S_SECMEM
      fprintf(fh_log, "cannot make new BIO by BIO_s_secmem()\n");
#else
      fprintf(fh_log, "cannot make new BIO by BIO_s_mem()\n");
#endif
    return -2;
  }

  {
    /* read contents to buffer */
    FILE* fh = fdopen(fd, "r");
    if (!fh) {
      if (fh_log)
        fprintf(fh_log, "cannot fopen() for fd%d\n", fd);
    } else {
      fseek(fh, 0L, SEEK_SET);

      while (1) {
        char buff[BIO_LOAD_STACKSIZE];
        size_t rlen = fread(buff, 1, BIO_LOAD_STACKSIZE, fh);
        BIO_write(*bio_p, buff, rlen);
        if (rlen < BIO_LOAD_STACKSIZE)
          break;
      }
      fclose(fh);
    }
    close(fd);
  }

  if (0 > BIO_seek(*bio_p, 0))
    return -1;
  return 1;
}

/*
 * Set chained server certifications to SSL_CTX from BIO stream
 */
int ssl_ctx_use_certificate_chain_from_bio_pem(BIO* bio, SSL_CTX* ctx)
{
  /* work as use_certificate_chain_file() with NULL SSL and non-NULL ctx */
  int ret = 0;
  X509 *x509_aux = NULL;
  pem_password_cb *passwd_cb;
  void *passwd_cb_dt;

  /* check arguments */
  if (!ctx || !bio)
    return -1;

  if (0 > BIO_seek(bio, 0))
    return -1;
  ERR_clear_error();

  passwd_cb    = SSL_CTX_get_default_passwd_cb(ctx),
  passwd_cb_dt = SSL_CTX_get_default_passwd_cb_userdata(ctx);
  x509_aux = PEM_read_bio_X509_AUX(bio, NULL, passwd_cb, passwd_cb_dt);
  if (x509_aux == NULL)
    return -2;

  if (fh_log)
    fprintf(fh_log, "  X509_AUX=%p is loaded from BIO=%p\n", x509_aux, bio);
  ret = SSL_CTX_use_certificate(ctx, x509_aux);
  if (ERR_peek_error() != 0) {
    if (fh_log)
      fprintf(fh_log, "  unable to set X509_AUX=%p for SSL_CTX=%p\n", x509_aux, ctx);
    ret = 0;
  }

  if (ret) {
    X509 *x509_ca;
    unsigned long err;
    if (0 == SSL_CTX_clear_chain_certs(ctx)) {
      if (fh_log)
        fprintf(fh_log, "  unable to clear chain cert for SSL_CTX=%p\n", ctx);
      ret = 0;
      goto end;
    }

    while (NULL != (x509_ca = PEM_read_bio_X509(bio, NULL, passwd_cb, passwd_cb_dt))) {
      if (fh_log)
        fprintf(fh_log, "  X509=%p is loaded from BIO=%p\n", x509_ca, bio);
      if (SSL_CTX_add0_chain_cert(ctx, x509_ca))
        continue;

      if (fh_log)
        fprintf(fh_log, "  unable to set X509=%p to chain of SSL_CTX=%p\n", x509_ca, ctx);
      X509_free(x509_ca);
      ret = 0;
      goto end;
    }
    err = ERR_peek_last_error();
    if (ERR_GET_LIB(err) == ERR_LIB_PEM &&
        ERR_GET_REASON(err) == PEM_R_NO_START_LINE)
      ERR_clear_error();
    else
      ret = 0;
  }

end:
  X509_free(x509_aux);
  return (ret);
}

void configure_context(SSL_CTX *ctx,
                       const char* envkey_srv_cert,
                       const char* envkey_srv_pkey)
{
  int fd_srv_cert = propn_get_fd_by_var_name(envkey_srv_cert);
  int fd_srv_pkey = propn_get_fd_by_var_name(envkey_srv_pkey);
  BIO* bio_srv_cert = NULL;
  BIO* bio_srv_pkey = NULL;

  if (fh_log)
    fprintf(fh_log, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, envkey_srv_cert);

  if (fd_srv_cert < 3) {
    if (fh_log)
      fprintf(fh_log, "%s is connected to invalid FD#%d\n", envkey_srv_cert, fd_srv_cert);
  }
  else if (get_bio_from_fd(&bio_srv_cert, fd_srv_cert) < 1) {
    if (fh_log) 
      fprintf(fh_log, "cannot open a BIO stream for %d\n", fd_srv_cert);
  }
  else if (ssl_ctx_use_certificate_chain_from_bio_pem(bio_srv_cert, ctx) < 1) {
    if (fh_log)
      fprintf(fh_log, "fail to set CERT files in BIO %p\n", bio_srv_cert);
  }
  BIO_free(bio_srv_cert);

  if (fh_log)
    fprintf(fh_log, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, envkey_srv_pkey);

  if (fd_srv_pkey < 3) {
    if (fh_log)
      fprintf(fh_log, "%s is connected to invalid FD#%d\n", envkey_srv_pkey, fd_srv_pkey);
  }
  else if (get_bio_from_fd(&bio_srv_pkey, fd_srv_pkey) < 1) {
    if (fh_log)
      fprintf(fh_log, "cannot open a BIO stream for %d\n", fd_srv_pkey);
  }
  else {
    EVP_PKEY* pkey = PEM_read_bio_PrivateKey(bio_srv_pkey, NULL,
                                             SSL_CTX_get_default_passwd_cb(ctx),
                                             SSL_CTX_get_default_passwd_cb_userdata(ctx));

    if (!pkey) {
      if (fh_log)
        fprintf(fh_log, "  failed to load a PrivateKey from BIO=%p for SSL_CTX=%p\n", bio_srv_pkey, ctx);
    }
    else {
      int ret_pkey_ok;
      if (fh_log)
        fprintf(fh_log, "  PrivateKey=%p is loaded for SSL_CTX=%p\n", pkey, ctx);
      ret_pkey_ok = SSL_CTX_use_PrivateKey(ctx, pkey);
      if (fh_log)
        fprintf(fh_log, "  SSL_CTX_use_PrivateKey(SSL_CTX=%p, pkey=%p) returns %d\n", ctx, pkey, ret_pkey_ok);
      if (1 > ret_pkey_ok) {
        if (fh_log)
          fprintf(fh_log, "  private key from fd#%d does not match\n", fd_srv_pkey);
      }
      else {
        if (fh_log)
          fprintf(fh_log, "  PrivateKey is successfully set to SSL_CTX=%p\n", ctx);
      }
      EVP_PKEY_free(pkey);
    }
  }

  BIO_free(bio_srv_pkey);
}

int main(const int argc, char* const* argv)
{
  int sock = -1;
  SSL_CTX *ctx;

  propn_prog_opt_st prog_opt = PROPN_PROG_OPT_ST_NULL;
  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port = DEFAULT_PORT;
  propn_parse_args(argc, argv, &prog_opt);

  /* Ignore broken pipe signals */
  signal(SIGPIPE, SIG_IGN);

  if (prog_opt.pathname_log)
    fh_log = fopen(prog_opt.pathname_log, "w");
  if (!fh_log && prog_opt.port > 0)
    fh_log = stderr;
  propn_chroot(&prog_opt);

  ctx = create_context();

  configure_context(ctx, prog_opt.envkey_srv_cert, prog_opt.envkey_srv_pkey);

  if (prog_opt.port > 0)
    sock = create_socket(prog_opt.port);

  if (fh_log)
    fflush(fh_log);

  /* Handle connections */
  while(1) {
    SSL *ssl;
    const char reply[] = "test\n";
    int client;

    ssl = SSL_new(ctx);

    if (prog_opt.port > 0) {
      struct sockaddr_in addr;
      unsigned int len = sizeof(addr);

      client = accept(sock, (struct sockaddr*)&addr, &len);
      if (client < 0) {
        perror("Unable to accept");
        exit(EXIT_FAILURE);
      }
      if (fh_log)
        fprintf(fh_log, "SSL_set_fd(SSL=%p, socket=%d)\n", ssl, client);
      SSL_set_fd(ssl, client);
    } else {
      if (fh_log)
        fprintf(fh_log, "SSL_set_rfd(SSL=%p, 0), SSL_set_wfd(SSL=%p, 1)\n", ssl, ssl);
      SSL_set_rfd(ssl, 0);
      SSL_set_wfd(ssl, 1);
    }

    if (SSL_accept(ssl) <= 0) {
      ERR_print_errors_fp(fh_log);
    } else {
      SSL_write(ssl, reply, strlen(reply));
    }

    SSL_shutdown(ssl);
    SSL_free(ssl);
    if (prog_opt.port > 0)
      close(client);
    break;
  }

  if (0 < sock)
    close(sock);
  SSL_CTX_free(ctx);
  if (fh_log)
    fclose(fh_log);
}
