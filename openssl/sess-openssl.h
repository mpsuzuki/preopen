#ifndef SESS_OSSL_H
#define SESS_OSSL_H

#include <openssl/ssl.h>
#include "sslexec-help-sess.h"

ssl_sess_t* ssl_sess_fill_ossl(ssl_sess_t*, SSL*);
ssl_sess_t* ssl_sess_init_err_sym_ossl(ssl_sess_t*);

#endif
