#include <openssl/ssl.h>
#include <openssl/err.h>

#include "fdpb.h"

#include "sslexec-help-sess.h"

ssize_t ossl_read(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  MY_UNUSED(d);
  return SSL_read((SSL*)ssl, buff, (int)len);
}

ssize_t ossl_write(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  MY_UNUSED(d);
  return SSL_write((SSL*)ssl, buff, (int)len);
}

ssize_t ossl_get_pending(void* ssl)
{
  return SSL_pending((SSL*)ssl);
}

int ossl_get_ssl_err(void* ssl, int ssl_ret)
{
  return SSL_get_error((SSL*)ssl, ssl_ret);
}

typedef struct {
  int          code;
  const char*  sym;
} tuple_err_code_sym;

static tuple_err_code_sym ossl_err_array[] = {
  { SSL_ERROR_NONE,       "SSL_ERROR_NONE" },
  { SSL_ERROR_SSL,        "SSL_ERROR_SSL" },
  { SSL_ERROR_WANT_READ,  "SSL_ERROR_WANT_READ" },
  { SSL_ERROR_WANT_WRITE, "SSL_ERROR_WANT_WRITE" },
  { SSL_ERROR_WANT_X509_LOOKUP, "SSL_ERROR_WANT_X509_LOOKUP" },
  { SSL_ERROR_SYSCALL, "SSL_ERROR_SYSCALL" },
  { SSL_ERROR_ZERO_RETURN,  "SSL_ERROR_ZERO_RETURN" },
  { SSL_ERROR_WANT_CONNECT, "SSL_ERROR_WANT_CONNECT" },
  { SSL_ERROR_WANT_ACCEPT,  "SSL_ERROR_WANT_ACCEPT" },
  { SSL_ERROR_WANT_ASYNC,   "SSL_ERROR_WANT_ASYNC" },
  { SSL_ERROR_WANT_ASYNC_JOB,       "SSL_ERROR_WANT_ASYNC_JOB" },
#ifdef SSL_ERROR_WANT_CLIENT_HELLO_CB
  { SSL_ERROR_WANT_CLIENT_HELLO_CB, "SSL_ERROR_WANT_CLIENT_HELLO_CB" },
#endif
#ifdef SSL_ERROR_WANT_RETRY_VERIFY
  { SSL_ERROR_WANT_RETRY_VERIFY,    "SSL_ERROR_WANT_RETRY_VERIFY" },
#endif
  { 0, NULL }
};

const char* ossl_get_err_sym(int err)
{
  int i;
  for (i = 0; ossl_err_array[i].sym != NULL; i++)
    if (ossl_err_array[i].code == err)
      return ossl_err_array[i].sym;

  return NULL;
}

ssl_sess_t* ssl_sess_fill_ossl(ssl_sess_t* sess, SSL* ssl)
{
  if (!sess || !ssl)
    return NULL;

  sess->ssl = ssl;
  sess->rfd = SSL_get_rfd(ssl);
  sess->wfd = SSL_get_wfd(ssl);
  sess->read  = ossl_read;
  sess->write = ossl_write;
  sess->get_pending = ossl_get_pending;
  sess->get_ssl_err = ossl_get_ssl_err;
  sess->get_err_sym = ossl_get_err_sym;

  return sess;
}

ssl_sess_t* ssl_sess_init_err_sym_ossl(ssl_sess_t* sess)
{
  if (!sess)
    return NULL;

  sess->get_ssl_err = ossl_get_ssl_err;
  sess->get_err_sym = ossl_get_err_sym;

  return sess;
}
