#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <signal.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <sys/select.h>

/* sockaddr_in */
#include <netinet/in.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

#include "env.h"
#include "pathexec.h"

#include "config.h"
#include "preopen-cli.h"
#include "fdpb.h"
#include "trifides.h"

#include "sslexec-help.h"
#include "sslexec-help-sess.h"
#include "sess-openssl.h"

/* chunk size to load PEM file */
#define SIZE_CHUNK_PEM 1024

/* buffer size to receive SSL error message */
/* according to man 3 ERR_error_string, its size should be > 256 */
#define SIZE_SSL_ERR_STR 1024

#define DEFAULT_PORT 10443

FILE* fh_log = NULL;
int debug_level = 0;
size_t pipe_buff_size = 16;
int child_finished = 0;

#define MAX_REALLOC_SIZE (2 << 16)

void fputs_log_err(int log_level, char* prefix, char* newline)
{
  char buff_err[SIZE_SSL_ERR_STR];

  if (prefix)
    fprintf_log(log_level, "%s", prefix);

  ERR_error_string_n(ERR_get_error(), buff_err, SIZE_SSL_ERR_STR);
  fprintf_log(log_level, "%s", buff_err);

  if (newline)
    fprintf_log(log_level, "%s", newline);
}

int create_socket(int port)
{
  int s, optval = 1;
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    fprintf_log(0, "Unable to create socket");
    return -1;
  }

#if defined(SOL_SOCKET) && defined(SO_REUSEADDR)
  if (0 > setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (void *)&optval, sizeof(int)))
    fprintf_log(0, "Failure in setsockopt( SO_REUSEADDR ), errno=%s\n", strerror(errno));
#endif

  if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    fprintf_log(0, "Unable to bind");
    close(s);
    return -1;
  }

  if (listen(s, 1) < 0) {
    fprintf_log(0, "Unable to listen");
    close(s);
    return -1;
  }

  fprintf_log(0, "Listen at port %d\n", port);

  return s;
}

SSL_CTX *create_context()
{
  const SSL_METHOD *method;
  SSL_CTX *ctx;

  method = TLS_server_method();

  ctx = SSL_CTX_new(method);
  if (!ctx) {
    fputs_log_err(0, "Unable to create SSL context ", "\n");
    return NULL;
  }

  {
    long ssl_ctx_option = 0;
    void *NO_SSLv3   = env_get("NO_SSLV3");
    void *NO_TLSv1   = env_get("NO_TLSV1");
    void *NO_TLSv1_1 = env_get("NO_TLSV1_1");
    void *NO_TLSv1_2 = env_get("NO_TLSV1_2");
    char* my_ciphers = env_get("OPENSSL_CIPHERS");

    if (NO_SSLv3)    ssl_ctx_option |= SSL_OP_NO_SSLv3;
    if (NO_TLSv1)    ssl_ctx_option |= SSL_OP_NO_TLSv1;
    if (NO_TLSv1_1)  ssl_ctx_option |= SSL_OP_NO_TLSv1_1;
    if (NO_TLSv1_2)  ssl_ctx_option |= SSL_OP_NO_TLSv1_2;
    if (ssl_ctx_option)
      SSL_CTX_set_options(ctx, ssl_ctx_option);

    if (my_ciphers && !SSL_CTX_set_cipher_list(ctx, my_ciphers)) {
      fputs_log_err(0, "Unable to set cipher list ", "\n");
      SSL_CTX_free(ctx);
      return NULL;
    }
  }

  return ctx;
}

/*
 * Get OpenSSL's BIO stream from a file descriptor
 */
int get_bio_from_fd(BIO** bio_p, int fd)
{
  if (bio_p == NULL || fd < 3)
    return -1;

#if HAVE_BIO_S_SECMEM
  *bio_p = BIO_new(BIO_s_secmem());
  if (!(*bio_p)) {
    fprintf_log(0, "cannot make new BIO by BIO_s_secmem()\n");
    return -2;
  }
#else
  *bio_p = BIO_new(BIO_s_mem());
  if (!(*bio_p)) {
    fprintf_log(0, "cannot make new BIO by BIO_s_mem()\n");
    return -2;
  }
#endif

  {
    /* read contents to buffer */
    FILE* fh = fdopen(fd, "r");
    if (!fh) {
      fprintf_log(0, "cannot fopen() for fd%d\n", fd);
    } else {
      fseek(fh, 0L, SEEK_SET);

      while (1) {
        char buff[SIZE_CHUNK_PEM];
        size_t rlen = fread(buff, 1, SIZE_CHUNK_PEM, fh);
        BIO_write(*bio_p, buff, rlen);
        if (rlen < SIZE_CHUNK_PEM)
          break;
      }
      fclose(fh);
    }
    close(fd);
  }

  if (0 > BIO_seek(*bio_p, 0))
    return -1;
  return 1;
}

/*
 * Set chained server certifications to SSL_CTX from BIO stream
 */
int ssl_ctx_use_certificate_chain_from_bio_pem(BIO* bio, SSL_CTX* ctx)
{
  /* work as use_certificate_chain_file() with NULL SSL and non-NULL ctx */
  int ret = 0;
  X509 *x509_aux = NULL;
  pem_password_cb *passwd_cb;
  void *passwd_cb_dt;

  /* check arguments */
  if (!ctx || !bio)
    return -1;

  if (0 > BIO_seek(bio, 0))
    return -1;
  ERR_clear_error();

  passwd_cb    = SSL_CTX_get_default_passwd_cb(ctx),
  passwd_cb_dt = SSL_CTX_get_default_passwd_cb_userdata(ctx);
  x509_aux = PEM_read_bio_X509_AUX(bio, NULL, passwd_cb, passwd_cb_dt);
  if (x509_aux == NULL)
    return -2;

  fprintf_log(0, "  X509_AUX=%p is loaded from BIO=%p\n", x509_aux, bio);
  ret = SSL_CTX_use_certificate(ctx, x509_aux);
  if (ERR_peek_error() != 0) {
    fprintf_log(0, "  unable to set X509_AUX=%p for SSL_CTX=%p\n", x509_aux, ctx);
    ret = 0;
  }

  if (ret) {
    X509 *x509_ca;
    unsigned long err;
    if (0 == SSL_CTX_clear_chain_certs(ctx)) {
      fprintf_log(0, "  unable to clear chain cert for SSL_CTX=%p\n", ctx);
      ret = 0;
      goto end;
    }

    while (NULL != (x509_ca = PEM_read_bio_X509(bio, NULL, passwd_cb, passwd_cb_dt))) {
      fprintf_log(0, "  X509=%p is loaded from BIO=%p\n", x509_ca, bio);
      if (SSL_CTX_add0_chain_cert(ctx, x509_ca))
        continue;

      fprintf_log(0, "  unable to set X509=%p to chain of SSL_CTX=%p\n", x509_ca, ctx);
      X509_free(x509_ca);
      ret = 0;
      goto end;
    }
    err = ERR_peek_last_error();
    if (ERR_GET_LIB(err) == ERR_LIB_PEM &&
        ERR_GET_REASON(err) == PEM_R_NO_START_LINE)
      ERR_clear_error();
    else
      ret = 0;
  }

end:
  X509_free(x509_aux);
  return (ret);
}

void configure_context(SSL_CTX *ctx,
                       const char* envkey_srv_cert,
                       const char* envkey_srv_pkey)
{
  int fd_srv_cert = propn_get_fd_by_var_name(envkey_srv_cert);
  int fd_srv_pkey = propn_get_fd_by_var_name(envkey_srv_pkey);
  BIO* bio_srv_cert = NULL;
  BIO* bio_srv_pkey = NULL;

  fprintf_log(0, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, envkey_srv_cert);

  if (fd_srv_cert < 3) {
    fprintf_log(0, "%s is connected to invalid FD#%d\n", envkey_srv_cert, fd_srv_cert);
  }
  else if (get_bio_from_fd(&bio_srv_cert, fd_srv_cert) < 1) {
    fprintf_log(0, "cannot open a BIO stream for %d\n", fd_srv_cert);
  }
  else if (ssl_ctx_use_certificate_chain_from_bio_pem(bio_srv_cert, ctx) < 1) {
    fprintf_log(0, "fail to set CERT files in BIO %p\n", bio_srv_cert);
  }
  BIO_free(bio_srv_cert);

  fprintf_log(0, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, envkey_srv_pkey);

  if (fd_srv_pkey < 3) {
    fprintf_log(0, "%s is connected to invalid FD#%d\n", envkey_srv_pkey, fd_srv_pkey);
  }
  else if (get_bio_from_fd(&bio_srv_pkey, fd_srv_pkey) < 1) {
    fprintf_log(0, "cannot open a BIO stream for %d\n", fd_srv_pkey);
  }
  else {
    EVP_PKEY* pkey = PEM_read_bio_PrivateKey(bio_srv_pkey, NULL,
                                             SSL_CTX_get_default_passwd_cb(ctx),
                                             SSL_CTX_get_default_passwd_cb_userdata(ctx));

    if (!pkey) {
      fprintf_log(0, "  failed to load a PrivateKey from BIO=%p for SSL_CTX=%p\n", bio_srv_pkey, ctx);
    }
    else {
      int ret_pkey_ok;
      fprintf_log(0, "  PrivateKey=%p is loaded for SSL_CTX=%p\n", pkey, ctx);
      ret_pkey_ok = SSL_CTX_use_PrivateKey(ctx, pkey);
      fprintf_log(0, "  SSL_CTX_use_PrivateKey(SSL_CTX=%p, pkey=%p) returns %d\n", ctx, pkey, ret_pkey_ok);
      if (1 > ret_pkey_ok) {
        fprintf_log(0, "  private key from fd#%d does not match\n", fd_srv_pkey);
      }
      else {
        fprintf_log(0, "  PrivateKey is successfully set to SSL_CTX=%p\n", ctx);
      }
      EVP_PKEY_free(pkey);
    }
  }

  BIO_free(bio_srv_pkey);
}

/*
 * retrun 1 if retryable
 */
int ssl_err_is_retryable(int ssl_err)
{
  switch (ssl_err) {
  case SSL_ERROR_NONE:         return 1;
  case SSL_ERROR_WANT_READ:    return 1;
  case SSL_ERROR_WANT_WRITE:   return 1;

  /* SSL session is closed normally */
  case SSL_ERROR_ZERO_RETURN:  return 0;

  /* SSL session is no longer valid */
  case SSL_ERROR_WANT_CONNECT:     return 0;
  case SSL_ERROR_WANT_ACCEPT:      return 0;
  case SSL_ERROR_WANT_X509_LOOKUP: return 0;
  case SSL_ERROR_WANT_ASYNC:       return 0;
  case SSL_ERROR_WANT_ASYNC_JOB:   return 0;
#ifdef SSL_ERROR_WANT_CLIENT_HELLO_CB
  case SSL_ERROR_WANT_CLIENT_HELLO_CB: return 0;
#endif

  /* out-of-OpenSSL errors */
  case SSL_ERROR_SYSCALL:      return 0;
  case SSL_ERROR_SSL:          return 0;
  default:                     return 1;
  }
}

/*
 *
 */
const char* ossl_err_array[] = {
  "SSL_ERROR_NONE",
  "SSL_ERROR_SSL",
  "SSL_ERROR_WANT_READ",
  "SSL_ERROR_WANT_WRITE",
  "SSL_ERROR_WANT_X509_LOOKUP",
  "SSL_ERROR_SYSCALL",
  "SSL_ERROR_ZERO_RETURN",
  "SSL_ERROR_WANT_CONNECT",
  "SSL_ERROR_WANT_ACCEPT",
  "SSL_ERROR_WANT_ASYNC",
  "SSL_ERROR_WANT_ASYNC_JOB",
  "SSL_ERROR_WANT_CLIENT_HELLO_CB",
  "SSL_ERROR_WANT_RETRY_VERIFY"
};

#define NUM_KNOWN_SSL_ERRORS (int)(sizeof(ossl_err_array))

SSL* setup_ssl(propn_prog_opt_st* prog_opt, int* client, int* sock, SSL_CTX **ctx)
{
  SSL *ssl = NULL;

  if (!prog_opt || !client || !sock || !ctx)
    return NULL; /* invalid argument */

  *client = -1;
  *sock   = -1;

  *ctx = create_context();
  if (!(*ctx))
    goto bail_out;

  configure_context(*ctx, prog_opt->envkey_srv_cert, prog_opt->envkey_srv_pkey);

  if (prog_opt->port > 0)
    *sock = create_socket(prog_opt->port);

  ssl = SSL_new(*ctx);
  if (!ssl)
    return NULL;

  if (prog_opt->port > 0) {
    struct sockaddr_in addr;
    unsigned int len = sizeof(addr);

#if defined(SOL_SOCKET) && defined(SO_RCVTIMEO)
    if (prog_opt->timeout_accept > 0) {
      struct timeval tv = {prog_opt->timeout_accept, 0};
      if (0 > setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)))
        fprintf_log(0, "Failure in setsockopt( SO_RCVTIMEO ), errno=%s\n", strerror(errno));
    }
#endif
    *client = accept(*sock, (struct sockaddr*)&addr, &len);
    if (*client < 0) {
      fprintf_log(0, "Failure in accept(), errno=%s\n", strerror(errno));
      return NULL;
    }
    if (child_finished) {
      fprintf_log(0, "child process is already finished\n");
      return NULL;
    }
    fprintf_log(0, "SSL_set_fd(SSL=%p, socket=%d)\n", ssl, *client);
    SSL_set_fd(ssl, *client);
  } else {
    fprintf_log(0, "SSL_set_rfd(SSL=%p, 0), SSL_set_wfd(SSL=%p, 1)\n", ssl, ssl);
    SSL_set_rfd(ssl, 0);
    SSL_set_wfd(ssl, 1);
  }

  return ssl;

bail_out:
  if (-1 < *client) {
    close(*client);
    *client = -1;
  }

  if (-1 < *sock) {
    close(*sock);
    *sock = -1;
  }

  if (ssl) {
    SSL_shutdown(ssl);
    SSL_free(ssl);
    ssl = NULL;
  }

  if (*ctx)
    SSL_CTX_free(*ctx);
  *ctx = NULL;

  return NULL;
}

void proc_parent(trfd_s* trfd, ssl_sess_t* sess)
{
  struct timeval  tv;

  tuple_err_t err = TUPLE_ERR_ZERO;
  fdpb_t  c2p, p2c, e2p;
  int     p_c2p = trfd->r, p_p2c = trfd->w, p_e2p = trfd->e;
  fd_set  fds_rd;
  int     nfds;
  int     ssl_rd_got_eof = 0;

  FD_ZERO(&fds_rd);
  FD_SET(p_c2p, &fds_rd);
  FD_SET(p_e2p, &fds_rd);
  FD_SET(sess->rfd, &fds_rd);

  nfds = MY_MAX( MY_MAX(p_c2p, p_e2p), sess->rfd) + 1;

  fdpb_init(&p2c, p_p2c, pipe_buff_size);
  fdpb_init(&c2p, p_c2p, pipe_buff_size);
  fdpb_init(&e2p, p_e2p, pipe_buff_size);

  set_timeval(&tv, sess->timeout->select_rd, 0);
  set_timeval(&(p2c.tv), 1, 1);
  set_timeval(&(c2p.tv), 1, 1);
  set_timeval(&(e2p.tv), 1, 1);

  // reading loop
  while (0 != select(nfds, &fds_rd, NULL, NULL, &tv)) {
    dump_selected_fds(p_p2c, p_c2p, p_e2p, sess->rfd, sess->wfd, &fds_rd, NULL);

    fdpb_set_ready(&c2p, &fds_rd);
    fdpb_set_ready(&e2p, &fds_rd);

    handle_e2p(&e2p);
    handle_p2c(&p2c);

    if (FD_ISSET(sess->rfd, &fds_rd)) {
      fprintf_log(0, "something to be read\n");
      if (0 <= ssl_to_p2c(sess, &p2c, &err)) {
        fprintf_log(0, "  0 <= ssl_to_p2c()\n");
        handle_e2p(&e2p);
        handle_p2c(&p2c);
        fprintf_log(0, "    e2p, p2c handled, goto prep_next\n");
        goto prep_next;
      }

      fprintf_log(0, " ssl_to_p2c() < 0, ssl_rd_got_eof\n");
      ssl_rd_got_eof = 1;
      break;
    } else
    if (fdpb_has_data(&c2p)) {
      fprintf_log(0, "c2p has some data\n");
      if (0 > c2p_to_ssl(sess, &c2p, &err)) {
        fprintf_log(0, "  failure in c2p_to_ssl(), ssl_rd_got_eof\n");
        ssl_rd_got_eof = 1;
        break;
      }

      if (err.ssl != SSL_ERROR_WANT_READ && 0 == sess->get_pending(sess->ssl)) {
        fprintf_log(0, "nothing to read after writing\n");
        goto prep_next;
      }

      if (fdpb_is_valid(&p2c)) {
        fprintf_log(0, "p2c is valid\n");
        if (0 > ssl_to_p2c(sess, &p2c, &err))
          ssl_rd_got_eof = 1;
      } else {
        fprintf_log(0, "p2c is invalid\n");
        if (0 > discard_ssl_pended_data(sess, &err))
          ssl_rd_got_eof = 1;
      }

      fprintf_log(0, "ssl_rd_got_eof = %d\n", ssl_rd_got_eof);
      if (ssl_rd_got_eof)
        break;

      goto prep_next;

    } else
    if (fdpb_is_valid(&c2p) && c2p.ready) {
      fprintf_log(0, "c2p is ready\n");
      // SSL is not confirmed to be ready to write, but read from c2p to its internal buffer
      fdpb_read_internal(&c2p);

      fprintf_log(0, "fdpb_read_internal(c2p) got %zu bytes, total %zu bytes\n", c2p.len_chunk, c2p.len);
      if (c2p.got_eof) {
        fprintf_log(0, " c2p got EOF\n");
      } else
      if (0 > c2p_to_ssl(sess, &c2p, &err)) {
        ssl_rd_got_eof = 1;
        break;
      }
    }

    close_dead_pipes(&p2c, &c2p, &e2p);

    if (child_finished && !fdpb_is_valid(&p2c) && !fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "child process is finished, p2c, c2p, e2p are closed, break select() loop\n");
      break;
    }

prep_next:
    if (!fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "c2p & e2p are closed, exit loop\n");
      break;
    }

    fprintf_log(20, "prep for next select():");
    FD_ZERO(&fds_rd);
    if (fdpb_is_valid(&c2p)) {
      fprintf_log(21, " c2p");
      FD_SET(c2p.fd, &fds_rd);
    }
    if (fdpb_is_valid(&e2p)) {
      fprintf_log(21, " e2p");
      FD_SET(e2p.fd, &fds_rd);
    }
    if (ssl_err_is_retryable(err.ssl) && !ssl_rd_got_eof) {
      fprintf_log(21, " ssl_rd");
      FD_SET(sess->rfd, &fds_rd);
    }
    fprintf_log(21, "\n");

    set_timeval(&tv, sess->timeout->select_rd, 0);
  }
}

int main(const int argc, char* const* argv)
{
  int r = 0;
  propn_prog_opt_st prog_opt = PROPN_PROG_OPT_ST_NULL;

  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port = DEFAULT_PORT;
  prog_opt.timeout_accept = 60; /* most DJB tools have 60 sec timeout */
  prog_opt.timeout_select = 60; /* most DJB tools have 60 sec timeout */
  propn_parse_args(argc, argv, &prog_opt);
  if (prog_opt.buffsize > 0)
    pipe_buff_size = prog_opt.buffsize;

  debug_level = prog_opt.debug_level;

  /* Ignore broken pipe signals */
  // signal(SIGPIPE, SIG_IGN);
  signal(SIGPIPE, update_child_finished);
  signal(SIGCHLD, update_child_finished);

  if (prog_opt.pathname_log) {
    unlink(prog_opt.pathname_log);
    fh_log = fopen(prog_opt.pathname_log, "w");
  } else
    fh_log = stderr;

  if (fh_log)
    fflush(fh_log);

  /* Handle connections */
  {
    pid_t pid;

    trfd_s trfd_prnt = { 0, 0, 0 }, trfd_chld = { 0, 0, 0 };

    if (trfd_open( TRIFIDES_AUTO, &trfd_prnt, &trfd_chld )) {
      fprintf_log(0, "failure in 3-file descriptors between parent and child STDIO/STDERR\n");
      r = -1;
      goto log_close;
    }

    pid = fork();
    switch (pid) {
      case -1:
        r = -2;
        break;

      case 0:
        /* child process */
        trfd_close_all(&trfd_prnt);

        propn_uidgid(&prog_opt, 1);

        trfd_xfer_012(&trfd_chld);

        pathexec(argv + (prog_opt.index_last_parsed_argv + 1));
        break;

      default:
        /* parent process, should keep SSL pipes until child process exit */
        trfd_close_all(&trfd_chld);

        {
          int  sock = -1, client = -1;
          SSL_CTX *ctx = NULL;
          SSL     *ssl = NULL;

          if (propn_chroot(&prog_opt)) {
            fprintf_log(0, "chroot failed\n");
          }

          propn_uidgid(&prog_opt, 0);

          ssl = setup_ssl(&prog_opt, &client, &sock, &ctx);
          if (!ssl) {
            fputs_log_err(0, "Failed to setup SSL session ", "\n");
            goto bail_out;
          }

          if (SSL_accept(ssl) <= 0) {
            fputs_log_err(0, "Error in SSL_accept() ", "\n");
            goto bail_out;
          } else {
            ssl_sess_t sess;
            ssl_timeout_t sess_to = {prog_opt.timeout_accept, prog_opt.timeout_select, prog_opt.timeout_select};

            /* stderr from client should be written somewhere, even if no debug mode */
            if (!fh_log)
              fh_log = stderr;

            ssl_sess_clear(&sess);
            ssl_sess_init_err_sym_ossl(&sess);
            sess.timeout = &sess_to;
            ssl_sess_fill_ossl(&sess, ssl);

            proc_parent(&trfd_prnt, &sess);
            fprintf_log(0, "parental loop is finished\n");
            goto bail_out;
          }

bail_out:
          if (!child_finished) {
            fprintf_log(0, "terminate child process pid=%d\n", pid);
            kill(pid, SIGTERM);
          }
          if (ssl) {
            fprintf_log(0, "shutdown SSL session %p\n", ssl);
            SSL_shutdown(ssl);
            SSL_free(ssl);
          }
          if (0 <= client) {
            fprintf_log(0, "close() client %d\n", client);
            close(client);
          }
          if (ctx) {
            fprintf_log(0, "free() SSL context %p\n", ctx);
            SSL_CTX_free(ctx);
          }
          if (0 <= sock) {
            fprintf_log(0, "close() socket %d\n", sock);
            close(sock);
          }
        }
    } /* switch */
  }

log_close:
  if (fh_log)
    fclose(fh_log);

  exit(r);
}
