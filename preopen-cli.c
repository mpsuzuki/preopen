/* fprintf() */
#include <stdio.h>

/* getenv() */
#include <stdlib.h>

/* INT_MAX */
#include <limits.h>

/* isdigit() */
#include <ctype.h>

/* open() */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* close() */
#include <unistd.h>

/* getopt(3), getopt_long(3) */
#if HAVE_GETOPT_H
# include <getopt.h>
#endif

/* errno */
#include <errno.h>

/* string */
#include <string.h>

/* va_list */
#include <stdarg.h>

#include "preopen-cli.h"

/*
 * use extern fh_log and debug_level
 *
 * if (log_level < debug_level)
 *   fprintf(fh_log, ...);
 */
void fprintf_log(int log_level, char* fmt, ...)
{
  va_list args_ptr;

  if (!fh_log)
    return;

  if (debug_level <= log_level)
    return;

  va_start(args_ptr, fmt);
  vfprintf(fh_log, fmt, args_ptr);

  fflush(fh_log);
}

/*
 * Get a fd-like integer from specified environmental variable.
 * to avoid the confusion with STDIN, STDOUT, STDERR,
 * the integers 0, 1, and 2 are dealt as wrong numbers.
 */
int propn_get_int_from_env(const char* k)
{
  const char*  v = getenv(k);
  long         l = strtol(v, NULL, 0);
  if (l < 3 || INT_MAX < l)
    return -1;

  return l;
}

/*
 * Get a fd-like integer of file descriptor from string.
 * The string is tried as an environmental variable
 * whose value is a digit number, but if it is failed,
 * fallback as a pathname to the file.
 */
int propn_get_fd_by_var_name(const char* s)
{
  const char* es;
  int i, fd;

  if (!s)
    return -1;

  es = getenv(s);
  if (!es) {
    fprintf_log(0, "   environmental variable named %s"
                   " is not found, try as pathname\n", s);
    goto open_as_pathname;
  }

  for (i = 0 ; es[i] != '\0' ; i ++) {
    if ( !isdigit(es[i]) ) {
      fprintf_log(0, "    environmental variable %s=%s"
                     " has non-digit character, try as pathanme\n",
                     s, es);
      goto open_as_pathname;
    }
  }
  return propn_get_int_from_env(s);

open_as_pathname:
  fd = open(s, O_RDONLY);
  if (0 > fd)
    fprintf_log(0, " failed to open \"%s\", because %s\n", s, strerror(errno));
  else
    fprintf_log(0, " \"%s\" is successfully opened as a file\n", s);

  return fd;
}

/*
 * Allocate new buffer and read the content from
 * the file descriptor.
 * The second argument sets the maximum size of
 * the buffer, and returns the loaded size. If
 * it is NULL, or a pointer to "0", malloc() tries
 * to allocate the size obtained by fstat().
 *
 */
unsigned char* propn_read_fd(int fd, size_t* len)
{
  unsigned char* buff = NULL;
  struct stat fd_stat;
  FILE* fh = NULL;
  long rlen = 0;

  /* should not close STDIN, STDOUT, STDERR */
  if (fd < 3)
    return NULL;

  if (0 > fstat(fd, &fd_stat) || fd_stat.st_size < 0) {
    fprintf_log(0, "fstat() for fd#%d failed\n", fd);
    return NULL;
  }

  /* if file size is larger than maximum buffer size specified by *len */
  if (len && *len != 0 && *len < (size_t)fd_stat.st_size) {
    *len = 0;
    return NULL;
  }

  rlen = (long)fd_stat.st_size;

  fh = fdopen(fd, "r");  
  if (!fh)
    goto close_fd;

  buff = (unsigned char*)malloc(rlen);
  if (!buff)
    goto close_fh;

  if ((size_t)rlen > fread(buff, 1, rlen, fh)) {
    free(buff);
    buff = NULL;
  }

  if (len)
    *len = (size_t)rlen;

close_fh:
  fclose(fh);

close_fd:
  close(fd);

  return buff;
}

/* --------------------------------------------------------- */
/*                 B O I L E R   P L A T E                   */
/* --------------------------------------------------------- */

void propn_print_help(const char* prog_name)
{
  puts("");
  printf("%s [OPTION] [ENVNAME_FD_SERVER_CERT] ", prog_name);
  printf("[ENVNAME_FD_SERVER_PRIVKEY] ");
  printf("[ENVNAME_FD_CLIENT_CERT]\n");
  puts("");
  puts("  -s [PATH|ENVNAME], --server-cert=[PATH|ENVNAME]");
  puts("    set pathname of server cerfitication,");
  puts("      or environmental variable to resolve it,");
  puts("      or environmental variable to get its file descriptor number");
  puts("");
  puts("  -k [PATH|ENVNAME], --server-pkey=[PATH|ENVNAME]");
  puts("    set pathname of server privatekey,");
  puts("      or environmental variable to resolve it,");
  puts("      or environmental variable to get its file descriptor number");
  puts("");
  puts("  -a [PATH|ENVNAME], --ca-cert=[PATH|ENVNAME]");
  puts("    set pathname for bundled CA file,");
  puts("      or environmental variable to resolve it,");
  puts("      or environmental variable to get its file descriptor number");
  puts("");
  puts("  -c [PATH|ENVNAME], --client-cert=[PATH|ENVNAME]");
  puts("    set pathname for client cerfitication,");
  puts("      or environmental variable to resolve it,");
  puts("      or environmental variable to get its file descriptor number");
  puts("");
  puts("  -l PATH, --log=PATH");
  puts("    set pathname for logging");
  puts("");
  puts("  -M NUM, --max-pem-size=NUM");
  puts("    set max size of PEM files to read");
  puts("");
  puts("  -p NUM, --port=NUM");
  puts("    set port number");
  puts("");
  puts("  -q");
  puts("    discard all log message and sterr outputs from client");
  puts("");
  puts("  -R PATH, --chroot=PATH");
  puts("    set pathname to chroot");
  puts("");
  puts("  -S, --stdio");
  puts("    use STDIN and STDOUT");
  puts("    in this case, no debug messages to STDERR");
  puts("");
  puts("  -e, --envuidgid");
  puts("    set UID & GID by environment $UID & $GID");
  puts("    does not change child process uid & gid");
  puts("");
  puts("  -E, --envuidgid-child");
  puts("    set UID & GID by environment $UID & $GID");
  puts("    change both of parent & child process");
  puts("");
  puts("  -B, --buff-size");
  puts("    set buffer size for pipe to client");
  puts("    change both of parent & child process");
  puts("");
  puts("  -T, --timeout-accept");
  puts("    set timeout (in sec) to wait the initial connection");
  puts("");
  puts("  -t, --timeout-select");
  puts("    set timeout (in sec) to wait for next data in a connection");
  puts("");
  puts("  -h, -?, --help");
  puts("    show this message");
  puts("");
  puts("  --");
  puts("    end of the options\n");
  puts("    options after \"--\" is regarded as child command and its options\n");
  exit(0);
}

void propn_parse_args(const int argc,
                      char* const* argv,
                      propn_prog_opt_st* prog_opt)
{
  int c;

  prog_opt->index_last_parsed_argv = 0;
  prog_opt->reflect_envuidgid = 0;
  prog_opt->reflect_envuidgid_child = 0;

  while (1) {
    int option_index = 0;
#if HAVE_GETOPT_LONG
    static struct option long_options[] = {
      {"max-size-pem", required_argument, 0, 'M' },
      {"ca-cert",     required_argument, 0, 'a'},
      {"client-cert", required_argument, 0, 'c'},
      {"server-cert", required_argument, 0, 's'},
      {"server-pkey", required_argument, 0, 'k'},
      {"debug", required_argument, 0, 'd'},
      {"log",   required_argument, 0, 'l'},
      {"port",  required_argument, 0, 'p'},
      {"chroot",required_argument, 0, 'R'},
      {"buffer-size",required_argument, 0, 'B'},
      {"timeout-accept", required_argument, 0, 'T'},
      {"timeout-select", required_argument, 0, 't'},
      {"envuidgid", no_argument, 0, 'e'},
      {"envuidgid-child", no_argument, 0, 'E'},
      {"stdio", no_argument, 0, 'S'},
      {"help",  no_argument, 0, 'h'},
      {"quiet", no_argument, 0, 'q'},
      {NULL,    0,           0,  0 }
    };
    c = getopt_long(argc, argv, "?ha:c:k:l:M:R:B:d:p:s:T:t:SEeq", long_options, &option_index);
#else
    c = getopt(argc, argv, "?ha:c:k:l:M:R:B:d:p:s:T:t:SEeq");
#endif
    if (c < 0)
      break;
    switch (c) {
    case 'a':
      prog_opt->envkey_ca_cert = optarg;
      break;

    case 'c':
      prog_opt->envkey_cli_cert = optarg;
      break;

    case 's':
      prog_opt->envkey_srv_cert = optarg;
      break;

    case 'k':
      prog_opt->envkey_srv_pkey = optarg;
      break;

    case 'R':
      prog_opt->pathname_chroot = optarg;
      break;

    case 'l':
      prog_opt->pathname_log = optarg;
      break;

    case 'M':
      prog_opt->max_size_pem = (size_t)strtoul(optarg, NULL, 0);
      break;

    case 'd':
      prog_opt->debug_level = (int)strtol(optarg, NULL, 0);
      break;

    case 'p':
      prog_opt->port = (unsigned int)strtoul(optarg, NULL, 0);
      break;

    case 'q':
      prog_opt->debug_level = -1;
      break;

    case 'B':
      prog_opt->buffsize = (size_t)strtoul(optarg, NULL, 0);
      break;

    case 'S':
      prog_opt->port = 0;
      break;

    case 'T':
      prog_opt->timeout_accept = (int)strtoul(optarg, NULL, 0);
      break;

    case 't':
      prog_opt->timeout_select = (int)strtoul(optarg, NULL, 0);
      break;

    case 'E':
      prog_opt->reflect_envuidgid = 1;
      prog_opt->reflect_envuidgid_child = 1;
      break;

    case 'e':
      prog_opt->reflect_envuidgid = 1;
      prog_opt->reflect_envuidgid_child = 0;
      break;

    case 'h':
    case '?':
      propn_print_help(argv[0]);
    }
  }

  if (0 < optind && argv[optind - 1][0] == '-' && argv[optind - 1][1] == '-' && argv[optind - 1][2] == 0) {
    prog_opt->index_last_parsed_argv = optind - 1;
  } else {
    if (optind < argc) /* if there is non-option argument, it would be regarded as SSL file */
      prog_opt->envkey_srv_pkey = prog_opt->envkey_srv_cert = argv[optind++];
    if (optind < argc) /* if there is one more argument, it would be regarded as SSL private key file */
      prog_opt->envkey_srv_pkey = argv[optind++];
  }
}

int propn_chroot(const propn_prog_opt_st* prog_opt)
{
  int ret = -1;

  if (!prog_opt)
    return -1;

  if (!prog_opt->pathname_chroot)
    return 0;

  if (getuid())
    return -2;

  ret = chdir(prog_opt->pathname_chroot);
  fprintf_log(0, "chdir(\"%s\") returns %d\n", prog_opt->pathname_chroot, ret);
  if (ret)
    return -3;

  ret = chroot("."); 
  fprintf_log(0, "chroot(\".\") returns %d\n", ret);
  if (ret)
    return -4;

  return 0;
}

int propn_uidgid(const propn_prog_opt_st* prog_opt, int is_child)
{
  int r = 0;
  char* endptr;
  char* env_uid = getenv("UID");
  char* env_gid = getenv("GID");

  if (is_child && prog_opt->reflect_envuidgid_child == 0)
    return r;

  if (prog_opt->reflect_envuidgid == 0)
    return r;

  if (env_gid) {
    gid_t gid = (gid_t)strtoul(env_gid, &endptr, 0);
    if (env_gid < endptr) {
      if (0 > setgid(gid))
        fprintf_log(0, "setgid() returns %s\n", strerror(errno));
      r ++;
    }
  }

  if (env_uid) {
    uid_t uid = (uid_t)strtoul(env_uid, &endptr, 0);
    if (env_uid < endptr) {
      if (0 > setuid(uid))
        fprintf_log(0, "setuid() returns %s\n", strerror(errno));
      r ++;
    }
  }

  return r;
}
