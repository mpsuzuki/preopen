#include <sys/select.h>
#include <errno.h>

typedef struct {
  int     fd;

  char*   buff;
  size_t  limit; /* the limit of buffer */
  size_t  len;   /* the length of the valid data in the buffer */

  int     ready; /* boolean */
  int     got_eof;   /* 0: not EOF yet, 1: reached EOF */
  ssize_t len_chunk; /* the chunk size of the last read/write operation, useful to check we got EOF */

  struct timeval tv; /* for writeout */
} fdpb_t;

/* initialize pre-allocated fdpb object, with a file descriptor, and a size for buffer */
fdpb_t* fdpb_init           (fdpb_t*, int, size_t);

/* finish existing fdpb object, free() its buffer, and close() its file descriptor */
void    fdpb_done           (fdpb_t*);


/* return the size of room in the buffer */
size_t  fdpb_len_room       (fdpb_t*);

/* extend the internal buffer */
ssize_t fdpb_extend         (fdpb_t*, size_t);

/* discard heading bytes (of specified size) and shift the buffered data */
fdpb_t* fdpb_shift          (fdpb_t*, size_t);

/* read external file descriptor and fill the buffer of fdpb object, as far as room is available */
ssize_t fdpb_read_external  (fdpb_t*, int);

/* read internal file descriptor and fill the buffer of fdpb object, as far as room is available */
ssize_t fdpb_read_internal  (fdpb_t*);

/* write the buffer to external file descriptor */
ssize_t fdpb_write_external (fdpb_t*, int);

/* write the buffer to internal file descriptor */
ssize_t fdpb_write_internal (fdpb_t*);

/* set fdpb.ready by testing FD_ISSET() with internal file descriptor */
int     fdpb_set_ready      (fdpb_t*, fd_set*);

/* check fdpb.fd is fifo */
int     fdpb_is_fifo        (fdpb_t*);

/* check fdpb.fd is socket */
int     fdpb_is_socket      (fdpb_t*);

/* check fdpb.fd and fdpb.buff for validity */
int     fdpb_is_valid       (fdpb_t*);

/* check fdpb.fd is valid fd. no check for buffer */
int     fdpb_has_fd         (fdpb_t*);

/* check fdpb.buff has some data. no check for fd */
int     fdpb_has_data       (fdpb_t*);

/* call select() for the external descriptor becomes ready to be 'r' or 'w' */
int fdpb_wait_select_fd_external(fdpb_t*, int, char);

/* call select() for the internal descriptor becomes ready to be 'r' or 'w' */
int fdpb_wait_select_fd_internal(fdpb_t*, char);
