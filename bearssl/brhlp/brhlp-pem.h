#ifndef MY_BRSSL_PEM_H
#define MY_BRSSL_PEM_H
/* Based on BearSSL samples/server_basic.c */
/*
 * Copyright (c) 2016 Thomas Pornin <pornin@bolet.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <bearssl.h>

/* taken from bearssl/tools/brssl.h */
typedef struct {
  char*           name;
  unsigned char*  data;
  size_t          data_len;
} pem_object;

/* vector of pem_onj */
typedef struct {
  size_t       num;
  pem_object*  pem_obj;
} vec_pem_obj_t;

/* taken from bearssl/tools/files.c */
pem_object* decode_pem(const void* src, size_t len, size_t *num);
void free_pem_obj_contents(pem_object* pem_obj);
void vec_pem_obj_append(void* dest, pem_object*  pem_obj);

#endif /* MY_BRSSL_PEM_H */
