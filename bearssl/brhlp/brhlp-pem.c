/* Based on BearSSL samples/server_basic.c */
/*
 * Copyright (c) 2016 Thomas Pornin <pornin@bolet.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#include "brhlp-blob.h"
#include "brhlp-pem.h"

extern FILE* fh_log;

/* taken from bearssl/tools/files.c */
void
free_pem_obj_contents(pem_object* pem_obj)
{
  if (!pem_obj)
    return;

  if (pem_obj->name)
    free(pem_obj->name);

  if (pem_obj->data)
    free(pem_obj->data);
}

void
vec_pem_obj_append(void*        dest,
                   pem_object*  pem_obj)
{
  vec_pem_obj_t*  vec_pem_obj = dest;
  size_t          size_cur = (vec_pem_obj->num) * sizeof(pem_object);
  size_t          size_new = size_cur + sizeof(pem_object);

  pem_object*     pem_objs_new = malloc(size_new);
  memcpy(pem_objs_new, vec_pem_obj->pem_obj, size_cur);
  memset(vec_pem_obj->pem_obj, 0, size_cur);
  free(vec_pem_obj->pem_obj); 

  pem_objs_new[vec_pem_obj->num] = *pem_obj;
  vec_pem_obj->num     = vec_pem_obj->num + 1;
  vec_pem_obj->pem_obj = pem_objs_new;
}

/* taken from bearssl/tools/files.c */
pem_object *
decode_pem(const void* src, size_t len, size_t *num)
{
  vec_pem_obj_t           vec_pem_obj = {0, NULL};
  vec_blob_t              vec_blob = {0, NULL};
  br_pem_decoder_context  pem_ctx;
  pem_object              pem_obj;
  pem_object*             pem_obj_p;
  const unsigned char*    buf;
  int                     in_obj, extra_nl;

  br_pem_decoder_init(&pem_ctx);
  *num   = 0;
  buf    = src;
  in_obj = 0;
  pem_obj.name = NULL;
  pem_obj.data = NULL;
  pem_obj.data_len = 0;
  extra_nl = 1;
  while (len > 0) {
    size_t tlen = br_pem_decoder_push(&pem_ctx, buf, len);
    buf += tlen;
    len -= tlen;

    switch (br_pem_decoder_event(&pem_ctx)) {
    case BR_PEM_BEGIN_OBJ:
      pem_obj.name = strdup(br_pem_decoder_name(&pem_ctx));
      br_pem_decoder_setdest(&pem_ctx, vec_blob_append, &vec_blob);
      in_obj = 1;
      break;

    case BR_PEM_END_OBJ:
      if (in_obj) {
        pem_obj.data     = vec_blob.blob;
        pem_obj.data_len = vec_blob.size;
        vec_pem_obj_append(&vec_pem_obj, &pem_obj);

        /* vec_blob.blob is copied to the last item of vec_pem_obj.pem_obj[],
         * thus it should not be freed.
         */
        vec_blob.size = 0;
        vec_blob.blob = NULL;

        pem_obj.name = NULL;
        pem_obj.data = NULL;
        pem_obj.data_len = 0;
        in_obj = 0;
      }
      break;

    case BR_PEM_ERROR:
      free(pem_obj.name);
      if (vec_blob.blob)
        free(vec_blob.blob);
      vec_blob.size = 0;
      vec_blob.blob = NULL;

      if (fh_log)
        fprintf(fh_log, "ERROR: invalid PEM encoding\n");

      {
        size_t i;
        for (i = 0; i < vec_pem_obj.num; i++)
          free_pem_obj_contents(&vec_pem_obj.pem_obj[i]);
        free(vec_pem_obj.pem_obj);
      }

      return NULL;
    }

    /* to ensure the newline at the end, append extra newline  */
    if (len == 0 && extra_nl) {
      extra_nl = 0;
      buf = (const unsigned char*)"\n";
      len = 1;
    }
  }
  if (in_obj) {
    if (fh_log)
      fprintf(fh_log, "ERROR: unfinished PEM object\n");

    free(pem_obj.name);
    if (vec_blob.blob)
      free(vec_blob.blob);
    vec_blob.size = 0;
    vec_blob.blob = NULL;

    {
      size_t i;
      for (i = 0; i < vec_pem_obj.num; i++)
        free_pem_obj_contents(&vec_pem_obj.pem_obj[i]);
      free(vec_pem_obj.pem_obj);
    }

    return NULL;
  }

  *num = vec_pem_obj.num;
  vec_pem_obj_append(&vec_pem_obj, &pem_obj);
  pem_obj_p = vec_pem_obj.pem_obj;

  /* vec_pem_obj should be freed? */

  return pem_obj_p;
}
