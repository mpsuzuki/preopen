/* Based on BearSSL samples/server_basic.c */
/*
 * Copyright (c) 2016 Thomas Pornin <pornin@bolet.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#define DEFAULT_PORT 10443
#define MAX_SIZE_PEM 0x100000 /* 1MiB by default */
#define MAX_BUF 1024

extern FILE* fh_log;

#include "brhlp-pem.h"
#include "brhlp-certs.h"

struct vec_x509_cert {
  size_t                num;
  br_x509_certificate*  x509_cert;
};

void
vec_x509_cert_append(vec_x509_cert_ptr vec_cert, br_x509_certificate cert)
{
  size_t size_cur = sizeof(br_x509_certificate)*(vec_cert->num);
  size_t size_new = size_cur + sizeof(br_x509_certificate);
  br_x509_certificate* certs_cur = vec_cert->x509_cert;
  br_x509_certificate* certs_new = (br_x509_certificate*)malloc(size_new);
  memcpy(certs_new, certs_cur, size_cur);
  memset(certs_cur, 0, size_cur);
  free(certs_cur);
  certs_new[vec_cert->num].data_len = cert.data_len;
  certs_new[vec_cert->num].data     = NULL;
  if (cert.data) {
    certs_new[vec_cert->num].data   = (unsigned char*)malloc(cert.data_len);
    memcpy(certs_new[vec_cert->num].data, cert.data, cert.data_len);
  }
  vec_cert->num       = vec_cert->num + 1;
  vec_cert->x509_cert = certs_new;
}

/* taken from bearssl/tools/files.c */
vec_x509_cert_ptr
read_certificates(unsigned char* buff, size_t size_buff, vec_x509_cert_ptr vec_cert)
{
  pem_object*    pem_objs  = NULL;
  size_t         i, num_pem_objs = 0;

  if (!buff || !size_buff || !vec_cert)
    return vec_cert;

  /* do not try DER file, assume PEM */
  pem_objs = decode_pem(buff, size_buff, &num_pem_objs);
  if (num_pem_objs > 0)
    memset(buff, 0, size_buff);

  for (i = 0; i < num_pem_objs; i++) {
    if ( (0 == strcmp(pem_objs[i].name, "X509 CERTIFICATE")) ||
         (0 == strcmp(pem_objs[i].name, "CERTIFICATE"))      )
    {
      br_x509_certificate x509_cert;
      x509_cert.data     = pem_objs[i].data;
      x509_cert.data_len = pem_objs[i].data_len;
      vec_x509_cert_append(vec_cert, x509_cert); /* deep copy */
    }
  }

  for (i = 0; i < num_pem_objs; i++)
    free_pem_obj_contents(&pem_objs[i]);
  if (pem_objs)
    free(pem_objs);

  if (0 == vec_cert->num) {
    if (fh_log)
      fprintf(fh_log, "ERROR: no certificate is found\n");
    return NULL;
  }
  {
    br_x509_certificate  x509_end = { NULL, 0 };
    vec_x509_cert_append(vec_cert, x509_end);
  }
  return vec_cert;
}

void free_vec_x509_cert_contents(vec_x509_cert_ptr vec_cert)
{
  size_t i;

  if (!vec_cert)
    return;

  /* free from tail to head */
  for (i = vec_cert->num; 0 < i;) {
    i = i - 1;
    if (!vec_cert->x509_cert[i].data_len)
      continue;
    if (!vec_cert->x509_cert[i].data)
      continue;
    memset(vec_cert->x509_cert[i].data, 0, vec_cert->x509_cert[i].data_len);
    free(vec_cert->x509_cert[i].data);
  }
  free(vec_cert->x509_cert);
  vec_cert->num = 0;
}

size_t get_num_vec_cert(vec_x509_cert_ptr vec_cert)
{
  return vec_cert->num;
}

br_x509_certificate* get_arr_from_vec_cert(vec_x509_cert_ptr vec_cert)
{
  return vec_cert->x509_cert;
}

void clear_vec_cert(vec_x509_cert_ptr vec_cert)
{
  if (!vec_cert)
    return;

  vec_cert->num = 0;
  vec_cert->x509_cert = NULL;
}

vec_x509_cert_ptr new_vec_cert(void)
{
  struct vec_x509_cert* vec_cert = (vec_x509_cert_ptr)malloc(sizeof(struct vec_x509_cert));
  clear_vec_cert(vec_cert);
  return vec_cert;
}
