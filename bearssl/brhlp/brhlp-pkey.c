/* Based on BearSSL samples/server_basic.c */
/*
 * Copyright (c) 2016 Thomas Pornin <pornin@bolet.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#define DEFAULT_PORT 10443
#define MAX_SIZE_PEM 0x100000 /* 1MiB by default */
#define MAX_BUF 1024

#include "brhlp-pem.h"
#include "brhlp-pkey.h"

struct private_key {
  int key_type;  /* BR_KEYTYPE_RSA or BR_KEYTYPE_EC */
  union {
    br_rsa_private_key rsa;
    br_ec_private_key  ec;
  } key;
};

extern FILE* fh_log;

/* taken from bearssl/tools/keys.c */
private_key_ptr
decode_key(const unsigned char* buff, size_t size_buff)
{
  br_skey_decoder_context  dec_ctx;
  struct private_key*      pkey = NULL;
  int err;

  br_skey_decoder_init(&dec_ctx);
  br_skey_decoder_push(&dec_ctx, buff, size_buff);
  err = br_skey_decoder_last_error(&dec_ctx);
  if (err) {
    if (fh_log)
      fprintf(fh_log, "ERROR (decoding): err=%d\n", err);
  }
  switch (br_skey_decoder_key_type(&dec_ctx)) {
    const br_rsa_private_key* pkey_rsa;
    const br_ec_private_key*  pkey_ec;

  case BR_KEYTYPE_RSA:
    pkey_rsa = br_skey_decoder_get_rsa(&dec_ctx);
    pkey = malloc(sizeof(*pkey));
    pkey->key_type = BR_KEYTYPE_RSA;
    pkey->key.rsa.n_bitlen = pkey_rsa->n_bitlen;
    pkey->key.rsa.plen     = pkey_rsa->plen;
    pkey->key.rsa.qlen     = pkey_rsa->qlen;
    pkey->key.rsa.dplen    = pkey_rsa->dplen;
    pkey->key.rsa.dqlen    = pkey_rsa->dqlen;
    pkey->key.rsa.iqlen    = pkey_rsa->iqlen;
    pkey->key.rsa.p        = (unsigned char*)malloc(pkey_rsa->plen);
    pkey->key.rsa.q        = (unsigned char*)malloc(pkey_rsa->qlen);
    pkey->key.rsa.dp       = (unsigned char*)malloc(pkey_rsa->dplen);
    pkey->key.rsa.dq       = (unsigned char*)malloc(pkey_rsa->dqlen);
    pkey->key.rsa.iq       = (unsigned char*)malloc(pkey_rsa->iqlen);
    memcpy(pkey->key.rsa.p,  pkey_rsa->p,  pkey->key.rsa.plen);
    memcpy(pkey->key.rsa.q,  pkey_rsa->q,  pkey->key.rsa.qlen);
    memcpy(pkey->key.rsa.dp, pkey_rsa->dp, pkey->key.rsa.dplen);
    memcpy(pkey->key.rsa.dq, pkey_rsa->dq, pkey->key.rsa.dqlen);
    memcpy(pkey->key.rsa.iq, pkey_rsa->iq, pkey->key.rsa.iqlen);
    break;

  case BR_KEYTYPE_EC:
    pkey_ec = br_skey_decoder_get_ec(&dec_ctx);
    pkey = malloc(sizeof(*pkey));
    pkey->key_type     = BR_KEYTYPE_EC;
    pkey->key.ec.curve = pkey_ec->curve;
    pkey->key.ec.xlen  = pkey_ec->xlen;
    pkey->key.ec.x     = (unsigned char*)malloc(pkey_ec->xlen);
    memcpy(pkey->key.ec.x, pkey_ec->x, pkey_ec->xlen);
    break;

  default:
    if (fh_log)
      fprintf(fh_log, "Unknown key type: %d\n", br_skey_decoder_key_type(&dec_ctx));
  }
  return pkey;
}


/* taken from bearssl/tools/files.c */
private_key_ptr
read_private_key(unsigned char* buff, size_t size_buff)
{
  struct private_key*  pkey = NULL;
  pem_object*          pem_objs = NULL;
  size_t               i, num_pem_objs = 0;

  if (!buff || !size_buff)
    return NULL;

  /* do not try DER file, assume PEM */
  pem_objs = decode_pem(buff, size_buff, &num_pem_objs);
  if (0 < num_pem_objs)
    memset(buff, 0, size_buff);

  for (i = 0; i < num_pem_objs; i++) {
    if ( (0 == strcmp(pem_objs[i].name, "RSA PRIVATE KEY")) ||
         (0 == strcmp(pem_objs[i].name, "EC PRIVATE KEY"))  ||
         (0 == strcmp(pem_objs[i].name, "PRIVATE KEY"))     )
    {
      pkey = decode_key(pem_objs[i].data, pem_objs[i].data_len);
    }
  }

  for (i = 0; i < num_pem_objs; i++)
    free_pem_obj_contents(&pem_objs[i]);
  if (pem_objs)
    free(pem_objs); 

  if (!pkey) {
    if (fh_log)
      fprintf(fh_log, "ERROR: no PrivateKey is found\n");
    return NULL;
  }
  return pkey;
}

void
free_pkey_rsa_contents(br_rsa_private_key* pkey_rsa)
{
  if (!pkey_rsa)
    return;

  if (pkey_rsa->p) {
    memset(pkey_rsa->p,  0, pkey_rsa->plen);
    free(pkey_rsa->p);
  }

  if (pkey_rsa->p) {
    memset(pkey_rsa->q,  0, pkey_rsa->qlen);
    free(pkey_rsa->q);
  }

  if (pkey_rsa->dp) {
    memset(pkey_rsa->dp, 0, pkey_rsa->dplen);
    free(pkey_rsa->dp);
  }

  if (pkey_rsa->dq) {
    memset(pkey_rsa->dq, 0, pkey_rsa->dqlen);
    free(pkey_rsa->dq);
  }

  if (pkey_rsa->iq) {
    memset(pkey_rsa->iq, 0, pkey_rsa->iqlen);
    free(pkey_rsa->iq);
  }
}

void
free_pkey_ec_contents(br_ec_private_key* pkey_ec)
{
  if (!pkey_ec)
    return;

  if (pkey_ec->x) {
    memset(pkey_ec->x, 0, pkey_ec->xlen);
    free(pkey_ec->x);
  }
}

void
free_pkey_contents(private_key_ptr pkey)
{
  if (!pkey)
    return;

  switch (pkey->key_type) {
  case BR_KEYTYPE_RSA:
    free_pkey_rsa_contents(&(pkey->key.rsa));
    break;

  case BR_KEYTYPE_EC:
    free_pkey_ec_contents(&(pkey->key.ec));
    break;

  default: /* unknown pkey */
    break;
  }
}

int
get_type_pkey(private_key_ptr pkey)
{
  return pkey->key_type;
}

br_rsa_private_key*
get_ptr_rsa_from_pkey(private_key_ptr pkey)
{
  return &(pkey->key.rsa);
}

br_ec_private_key*
get_ptr_ec_from_pkey(private_key_ptr pkey)
{
  return &(pkey->key.ec);
}
