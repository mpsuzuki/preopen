#ifndef MY_BRSSL_PKEY
#define MY_BRSSL_PKEY
/* Based on BearSSL samples/server_basic.c */
/*
 * Copyright (c) 2016 Thomas Pornin <pornin@bolet.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <bearssl.h>

/* taken from bearssl/tools/brssl.h */
/*
 * Aggregate type for a private key.
 */
typedef struct private_key *private_key_ptr;

private_key_ptr decode_key(const unsigned char* buff, size_t size_buff);
private_key_ptr read_private_key(unsigned char* buff, size_t size_buff);
void free_pkey_contents(private_key_ptr pkey);
int get_type_pkey(private_key_ptr pkey);
br_rsa_private_key* get_ptr_rsa_from_pkey(private_key_ptr pkey);
br_ec_private_key* get_ptr_ec_from_pkey(private_key_ptr pkey);
#endif /* MY_BRSSL_PKEY */
