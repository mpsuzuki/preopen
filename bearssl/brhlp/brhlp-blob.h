#ifndef MY_BRSSL_BLOB_H
#define MY_BRSSL_BLOB_H
/*
 * here, "blob" is an array of unsigned char,
 * which may lack a NULL terminator.
 */
typedef struct {
  size_t          size;
  unsigned char*  blob;
} vec_blob_t;

void vec_blob_append(void* dest, const void* chunk, size_t chunk_size);
#endif /* MY_BRSSL_BLOB_H */
