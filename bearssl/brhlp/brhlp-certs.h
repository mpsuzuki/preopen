#ifndef MY_BRSSL_CERTS_H
#define MY_BRSSL_CERTS_H
/* Based on BearSSL samples/server_basic.c */
/*
 * Copyright (c) 2016 Thomas Pornin <pornin@bolet.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <bearssl.h>

typedef struct vec_x509_cert *vec_x509_cert_ptr;

void vec_x509_cert_append(vec_x509_cert_ptr vec_cert, br_x509_certificate cert);
vec_x509_cert_ptr read_certificates(unsigned char* buff, size_t size_buff, vec_x509_cert_ptr vec_cert);
void free_vec_x509_cert_contents(vec_x509_cert_ptr vec_cert);
size_t get_num_vec_cert(vec_x509_cert_ptr vec_cert);
br_x509_certificate* get_arr_from_vec_cert(vec_x509_cert_ptr vec_cert);
void clear_vec_cert(vec_x509_cert_ptr vec_cert);
vec_x509_cert_ptr new_vec_cert(void);
 
#endif /* MY_BRSSL_CERTS_H */
