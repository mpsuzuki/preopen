/*
 * cipher's name string to code.
 */

typedef struct {
  const char* name;
  uint16_t code;
} brhlp_name_code16_t;

uint16_t  brhlp_suite_get_code16_from_str(const char*);
uint16_t* brhlp_parse_suites_string(const char*, size_t*);
const char* brhlp_suite_code16_to_name(uint16_t);
const char* brhlp_protocol_code16_to_name(uint16_t);
