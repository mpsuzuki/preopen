#include <stdlib.h>
#include <string.h>
#include "brhlp-blob.h"

void
vec_blob_append(void*        dest,
                const void*  chunk,
                size_t       chunk_size)
{
  vec_blob_t* vec_blob_p = dest;
  size_t size_new = vec_blob_p->size + chunk_size;
  unsigned char* blob_new  = (unsigned char*)malloc(size_new);

  memcpy(blob_new, vec_blob_p->blob, vec_blob_p->size);
  memset(vec_blob_p->blob, 0, vec_blob_p->size);
  free(vec_blob_p->blob);

  memcpy(blob_new + vec_blob_p->size, chunk, chunk_size);
  vec_blob_p->size = size_new;
  vec_blob_p->blob = blob_new;
}
