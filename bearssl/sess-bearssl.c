#include <unistd.h>
#include <bearssl.h>

#include "fdpb.h"

#include "sslexec-help-sess.h"
#include "sess-bearssl.h"

br_ssl_server_context* get_br_ssl_server_from_sess(ssl_sess_t* sess)
{
  if (sess && sess->ssl)
    return ((brssl_ctxs_t*)(sess->ssl))->srv;
  else
    return NULL;
}

br_ssl_engine_context* get_br_ssl_engine_from_sess(ssl_sess_t* sess)
{
  if (sess && sess->ssl)
    return ((brssl_ctxs_t*)(sess->ssl))->eng;
  else
    return NULL;
}

br_sslio_context* get_br_sslio_from_sess(ssl_sess_t* sess)
{
  if (sess && sess->ssl)
    return ((brssl_ctxs_t*)(sess->ssl))->io;
  else
    return NULL;
}

ssize_t brssl_read(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  brssl_ctxs_t* ctxs = (brssl_ctxs_t*)ssl;
  MY_UNUSED(d);
  return br_sslio_read(ctxs->io, buff, (size_t)len);
}

ssize_t brssl_write(void* ssl, void* buff, ssize_t len, sess_data_t* d)
{
  brssl_ctxs_t* ctxs = (brssl_ctxs_t*)ssl;
  MY_UNUSED(d);
  return br_sslio_write(ctxs->io, buff, (size_t)len);
}

ssize_t brssl_get_pending(void* ssl)
{
  size_t rlen;
  brssl_ctxs_t* ctxs = (brssl_ctxs_t*)ssl;
  if (NULL != br_ssl_engine_recvapp_buf(ctxs->eng, &rlen))
    return (ssize_t)rlen;

  return 0;
}

int brssl_get_ssl_err(void* ssl, int ssl_ret)
{
  brssl_ctxs_t* ctxs = (brssl_ctxs_t*)ssl;
  MY_UNUSED(ssl_ret);

  return br_ssl_engine_last_error(ctxs->eng);
}

typedef struct {
  int          code;
  const char*  sym;
} tuple_err_code_sym;

static tuple_err_code_sym brssl_err_array[] = {
  { BR_ERR_OK,                "BR_ERR_OK" },
  { BR_ERR_BAD_PARAM,         "BR_ERR_BAD_PARAM" },
  { BR_ERR_BAD_STATE,         "BR_ERR_BAD_STATE" },
  { BR_ERR_UNSUPPORTED_VERSION, "BR_ERR_UNSUPPORTED_VERSION" },
  { BR_ERR_BAD_VERSION,       "BR_ERR_BAD_VERSION" },
  { BR_ERR_BAD_LENGTH,        "BR_ERR_BAD_LENGTH" },
  { BR_ERR_TOO_LARGE,         "BR_ERR_TOO_LARGE" },
  { BR_ERR_BAD_MAC,           "BR_ERR_BAD_MAC" },
  { BR_ERR_NO_RANDOM,         "BR_ERR_NO_RANDOM" },
  { BR_ERR_UNKNOWN_TYPE,      "BR_ERR_UNKNOWN_TYPE" },
  { BR_ERR_UNEXPECTED,        "BR_ERR_UNEXPECTED" },
  { BR_ERR_BAD_CCS,           "BR_ERR_BAD_CCS" },
  { BR_ERR_BAD_ALERT,         "BR_ERR_BAD_ALERT" },
  { BR_ERR_BAD_HANDSHAKE,     "BR_ERR_BAD_HANDSHAKE" },
  { BR_ERR_OVERSIZED_ID,      "BR_ERR_OVERSIZED_ID" },
  { BR_ERR_BAD_CIPHER_SUITE,  "BR_ERR_BAD_CIPHER_SUITE" },
  { BR_ERR_BAD_COMPRESSION,   "BR_ERR_BAD_COMPRESSION" },
  { BR_ERR_BAD_FRAGLEN,       "BR_ERR_BAD_FRAGLEN" },
  { BR_ERR_BAD_SECRENEG,      "BR_ERR_BAD_SECRENEG" },
  { BR_ERR_EXTRA_EXTENSION,   "BR_ERR_EXTRA_EXTENSION" },
  { BR_ERR_BAD_SNI,           "BR_ERR_BAD_SNI" },
  { BR_ERR_BAD_HELLO_DONE,    "BR_ERR_BAD_HELLO_DONE" },
  { BR_ERR_LIMIT_EXCEEDED,    "BR_ERR_LIMIT_EXCEEDED" },
  { BR_ERR_BAD_FINISHED,      "BR_ERR_BAD_FINISHED" },
  { BR_ERR_RESUME_MISMATCH,   "BR_ERR_RESUME_MISMATCH" },
  { BR_ERR_INVALID_ALGORITHM, "BR_ERR_INVALID_ALGORITHM" },
  { BR_ERR_BAD_SIGNATURE,     "BR_ERR_BAD_SIGNATURE" },
  { BR_ERR_WRONG_KEY_USAGE,   "BR_ERR_WRONG_KEY_USAGE" },
  { BR_ERR_NO_CLIENT_AUTH,    "BR_ERR_NO_CLIENT_AUTH" },
  { BR_ERR_IO,                "BR_ERR_IO" },
  { BR_ERR_RECV_FATAL_ALERT,  "BR_ERR_RECV_FATAL_ALERT" },
  { BR_ERR_SEND_FATAL_ALERT,  "BR_ERR_SEND_FATAL_ALERT" },
  { 0, NULL }
};

const char* brssl_get_err_sym(int err)
{
  int i;
  for (i = 0; brssl_err_array[i].sym != NULL; i++)
    if (brssl_err_array[i].code == err)
      return brssl_err_array[i].sym;

  return NULL;
}

/*
 * BearSSL has no API to retrieve the file descriptors
 * which were written in br_sslio_init(). We have to set
 * ssl_sess_t.rfd & .wfd externally and manually.
 */
ssl_sess_t* ssl_sess_fill_brssl(ssl_sess_t* sess, brssl_ctxs_t* ctxs)
{
  if (!sess || !ctxs)
    return NULL;

  sess->ssl = ctxs;
  sess->rfd = -1;
  sess->wfd = -1;
  sess->read  = brssl_read;
  sess->write = brssl_write;
  sess->get_pending = brssl_get_pending;
  sess->get_ssl_err = brssl_get_ssl_err;
  sess->get_err_sym = brssl_get_err_sym;

  return sess;
}

ssl_sess_t* ssl_sess_init_err_sym_brssl(ssl_sess_t* sess)
{
  if (!sess)
    return NULL;

  sess->get_ssl_err = brssl_get_ssl_err;
  sess->get_err_sym = brssl_get_err_sym;

  return sess;
}
