/* Based on BearSSL samples/server_basic.c */
/*
 * Copyright (c) 2016 Thomas Pornin <pornin@bolet.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#include "preopen-cli.h"
#define DEFAULT_PORT 10443
#define MAX_SIZE_PEM 0x100000 /* 1MiB by default */
#define MAX_BUF 1024

FILE* fh_log = NULL;
int   debug_level = 0;

#include <bearssl.h>
#include "brhlp/brhlp-certs.h"
#include "brhlp/brhlp-pkey.h"

int
set_ctx_cert_pkey_fd2(br_ssl_server_context* srv_ctx,
                      int fd_srv_cert,
                      int fd_srv_pkey,
                      size_t max_size_pem,
                      vec_x509_cert_ptr vec_cert,
                      private_key_ptr* pkey_p)
{
  int              ret = -1;
  private_key_ptr  pkey = NULL;
  unsigned char*   buff = NULL;
  size_t           size_buff = max_size_pem;

  if (!vec_cert || !pkey_p)
    return ret;

  if (NULL == (buff = propn_read_fd(fd_srv_cert, &size_buff)))
    return ret;

  vec_cert = read_certificates(buff, size_buff, vec_cert);
  /* erase sensitive data in buff */
  memset(buff, 0, size_buff);
  free(buff);

  if (!vec_cert)
    return ret;

  size_buff = max_size_pem;
  if (NULL == (buff = propn_read_fd(fd_srv_pkey, &size_buff)))
    return ret;
  pkey = read_private_key(buff, size_buff);
  memset(buff, 0, size_buff);
  free(buff);

  if (!pkey)
    goto free_certs;
  *pkey_p = pkey;

  {
    /* exclude final {0, NULL} entry in certs->x509_cert */
    br_x509_certificate* x509_certs = get_arr_from_vec_cert(vec_cert);
    size_t               num_certs  = get_num_vec_cert(vec_cert);

    switch (get_type_pkey(pkey)) {
    case BR_KEYTYPE_RSA:
      br_ssl_server_init_full_rsa(srv_ctx, x509_certs, num_certs - 1,
                                  get_ptr_rsa_from_pkey(pkey));
      return 0;

    case BR_KEYTYPE_EC:
      br_ssl_server_init_full_ec (srv_ctx, x509_certs, num_certs - 1,
                                  BR_KEYTYPE_EC,
                                  get_ptr_ec_from_pkey(pkey));
      return 0;

    default:
      fprintf_log(0, "No known encryption of private key\n");
    }
  }

free_certs:
  free_vec_x509_cert_contents(vec_cert);

  return ret;
}

int
fd_read(void* fd_ctx, unsigned char* buff, size_t size_buff)
{
  int fd = *(int *)fd_ctx;
  for (;;) {
    ssize_t rlen = read(fd, buff, size_buff);
    if (rlen < 0 && EINTR == errno)
      continue;
    if (rlen <= 0)
      return -1;
    return (int)rlen;
  }
}

int
fd_write(void* fd_ctx, const unsigned char* buff, size_t size_buff)
{
  int fd = *(int *)fd_ctx;
  for (;;) {
    ssize_t wlen = write(fd, buff, size_buff);
    if (wlen < 0 && EINTR == errno)
      continue;
    if (wlen <= 0)
      return -1;
    return (int)wlen;
  }
}

int
main(const int argc, char* const* argv)
{
  int                    ret = -1;
  vec_x509_cert_ptr      vec_cert = new_vec_cert();
  private_key_ptr        pkey = NULL;
  unsigned char          iobuf[MAX_SIZE_PEM];
  int                    fd_stdin = 0, fd_stdout = 1;
  int                    fd_srv_cert = -1, fd_srv_pkey = -1, listen_sd = -1, sd = -1;
  size_t                 max_size_pem;
  br_ssl_server_context  srv_ctx  = { 0 };
  br_sslio_context       io_ctx   = { 0 };
  propn_prog_opt_st      prog_opt = PROPN_PROG_OPT_ST_NULL;

  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port            = DEFAULT_PORT;
  prog_opt.max_size_pem    = MAX_SIZE_PEM;
  

  propn_parse_args(argc, argv, &prog_opt);

  if (prog_opt.pathname_log)
    fh_log = fopen(prog_opt.pathname_log, "w");
  if (!fh_log && prog_opt.port > 0)
    fh_log = stderr;
  propn_chroot(&prog_opt);

  fd_srv_cert = propn_get_fd_by_var_name(prog_opt.envkey_srv_cert);
  fprintf_log(0, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, prog_opt.envkey_srv_cert);

  fd_srv_pkey = propn_get_fd_by_var_name(prog_opt.envkey_srv_pkey);
  fprintf_log(0, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, prog_opt.envkey_srv_pkey);

  max_size_pem = prog_opt.max_size_pem;

  fprintf_log(0, "before certification install: %d\n", br_ssl_engine_last_error(&srv_ctx.eng));

  ret = set_ctx_cert_pkey_fd2(&srv_ctx, fd_srv_cert, fd_srv_pkey, max_size_pem, vec_cert, &pkey);
  fprintf_log(0, "after certification install: %d\n", br_ssl_server_reset(&srv_ctx));

  br_ssl_engine_set_buffer(&srv_ctx.eng, iobuf, sizeof(iobuf), 1);
  fprintf_log(0, "after engine buffer setting: %d\n", br_ssl_engine_last_error(&srv_ctx.eng));

  br_ssl_server_reset(&srv_ctx);
  fprintf_log(0, "after engine reset: %d\n", br_ssl_engine_last_error(&srv_ctx.eng));

  if (prog_opt.port > 0) {
    char topbuf[512];
    struct sockaddr_in sa_serv;
    struct sockaddr_in sa_cli;
    socklen_t client_len = sizeof(sa_cli);
    int optval = 1;
    listen_sd = socket(AF_INET, SOCK_STREAM, 0);

    memset(&sa_serv, '\0', sizeof(sa_serv));
    sa_serv.sin_family      = AF_INET;
    sa_serv.sin_addr.s_addr = INADDR_ANY;
    sa_serv.sin_port        = htons(prog_opt.port); /* Server Port number */

    setsockopt(listen_sd, SOL_SOCKET, SO_REUSEADDR, (void *)&optval, sizeof(int));

    bind(listen_sd, (struct sockaddr *)&sa_serv, sizeof(sa_serv));
    listen(listen_sd, 1024);
    fprintf_log(0, "Server ready. Listening to port '%d'.\n\n", prog_opt.port);

    sd = accept(listen_sd, (struct sockaddr *)&sa_cli, &client_len);
    fprintf_log(0, "- connection from %s, port %d\n",
                   inet_ntop(AF_INET, &sa_cli.sin_addr, topbuf, sizeof(topbuf)),
                   ntohs(sa_cli.sin_port));

    br_sslio_init(&io_ctx, &srv_ctx.eng, fd_read, &sd, fd_write, &sd);
  } else {
    br_sslio_init(&io_ctx, &srv_ctx.eng, fd_read, &fd_stdin, fd_write, &fd_stdout);
  }
  fprintf(fh_log, "SSL error: %d\n", br_ssl_engine_last_error(&srv_ctx.eng));

  for (;;) {
    unsigned char x;
    if (0 > br_sslio_read(&io_ctx, &x, 1))
      goto cli_drop;
    br_sslio_write_all(&io_ctx, "test\n", strlen("test\n"));
    br_sslio_close(&io_ctx);
  }

cli_drop:
  fprintf_log(0, "SSL error: %d\n", br_ssl_engine_last_error(&srv_ctx.eng));
  if (2 < sd)
    close(sd);

  if (pkey) {
    free_pkey_contents(pkey);
    free(pkey);
  }

  free_vec_x509_cert_contents(vec_cert);
  free(vec_cert);

  if (fh_log)
    fclose(fh_log);
  exit(ret);
}
