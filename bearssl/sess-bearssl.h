#ifndef SESS_BSSL_H
#define SESS_BSSL_H

#include <bearssl.h>

#include "sslexec-help-sess.h"

typedef struct {
  br_ssl_server_context* srv;
  br_ssl_engine_context* eng;
  br_sslio_context*      io;
} brssl_ctxs_t;

ssl_sess_t* ssl_sess_fill_brssl(ssl_sess_t*, brssl_ctxs_t*);
ssl_sess_t* ssl_sess_init_err_sym_brssl(ssl_sess_t* sess);
br_ssl_server_context* get_br_ssl_server_from_sess(ssl_sess_t*);
br_ssl_engine_context* get_br_ssl_engine_from_sess(ssl_sess_t*);
br_sslio_context* get_br_sslio_from_sess(ssl_sess_t*);

#endif
