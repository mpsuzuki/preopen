#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <sys/select.h>

/* sockaddr_in */
#include <netinet/in.h>

#include <bearssl.h>
#include "brhlp/brhlp-certs.h"
#include "brhlp/brhlp-names.h"
#include "brhlp/brhlp-pkey.h"

#include "env.h"
#include "pathexec.h"

#include "config.h"
#include "preopen-cli.h"
#include "fdpb.h"
#include "trifides.h"

#include "sslexec-help.h"
#include "sslexec-help-sess.h"

#include "sess-bearssl.h"


/* max size of PEM file: 1MiB by default */
#define MAX_SIZE_PEM 0x100000

/* chunk size to load PEM file */
#define SIZE_CHUNK_PEM 1024

#ifndef BEARSSL_BUFFSIZE_DEFAULT
#define BEARSSL_BUFFSIZE_DEFAULT 10000
#endif

#define DEFAULT_PORT 10443

FILE* fh_log = NULL;
int debug_level = 0;
size_t pipe_buff_size = 16;
int child_finished = 0;

#define MAX_REALLOC_SIZE (2 << 16)

int
set_ctx_cert_pkey_fd2(br_ssl_server_context* srv_ctx,
                      int fd_srv_cert,
                      int fd_srv_pkey,
                      size_t max_size_pem,
                      vec_x509_cert_ptr vec_cert,
                      private_key_ptr* pkey_p)
{
  int              ret = -1;
  private_key_ptr  pkey = NULL;
  unsigned char*   buff = NULL;
  size_t           size_buff = max_size_pem;

  if (!vec_cert || !pkey_p)
    return ret;

  if (NULL == (buff = propn_read_fd(fd_srv_cert, &size_buff)))
    return ret;

  vec_cert = read_certificates(buff, size_buff, vec_cert);
  /* erase sensitive data in buff */
  memset(buff, 0, size_buff);
  free(buff);

  if (!vec_cert)
    return ret;

  size_buff = max_size_pem;
  if (NULL == (buff = propn_read_fd(fd_srv_pkey, &size_buff)))
    return ret;
  pkey = read_private_key(buff, size_buff);
  memset(buff, 0, size_buff);
  free(buff);

  if (!pkey)
    goto free_certs;
  *pkey_p = pkey;

  {
    /* exclude final {0, NULL} entry in certs->x509_cert */
    br_x509_certificate* x509_certs = get_arr_from_vec_cert(vec_cert);
    size_t               num_certs  = get_num_vec_cert(vec_cert);

    switch (get_type_pkey(pkey)) {
    case BR_KEYTYPE_RSA:
      br_ssl_server_init_full_rsa(srv_ctx, x509_certs, num_certs - 1,
                                  get_ptr_rsa_from_pkey(pkey));
      return 0;

    case BR_KEYTYPE_EC:
      br_ssl_server_init_full_ec (srv_ctx, x509_certs, num_certs - 1,
                                  BR_KEYTYPE_EC,
                                  get_ptr_ec_from_pkey(pkey));
      return 0;

    default:
      if (fh_log)
        fprintf(fh_log, "No known encryption of private key\n");
    }
  }

free_certs:
  free_vec_x509_cert_contents(vec_cert);

  return ret;
}

int
fd_read(void* fd_ctx, unsigned char* buff, size_t size_buff)
{
  int fd = *(int *)fd_ctx;
  for (;;) {
    ssize_t rlen = read(fd, buff, size_buff);
    if (rlen < 0 && EINTR == errno)
      continue;
    if (rlen <= 0)
      return -1;
    return (int)rlen;
  }
}

int
fd_write(void* fd_ctx, const unsigned char* buff, size_t size_buff)
{
  int fd = *(int *)fd_ctx;
  for (;;) {
    ssize_t wlen = write(fd, buff, size_buff);
    if (wlen < 0 && EINTR == errno)
      continue;
    if (wlen <= 0)
      return -1;
    return (int)wlen;
  }
}

int
create_socket(propn_prog_opt_st* prog_opt, int* client, int* sock)
{
  char topbuf[512];
  struct sockaddr_in sa_serv;
  struct sockaddr_in sa_cli;
  socklen_t client_len = sizeof(sa_cli);
  int optval = 1;
  struct timeval tv = {prog_opt->timeout_accept, 0};

  *sock = socket(AF_INET, SOCK_STREAM, 0);

  memset(&sa_serv, '\0', sizeof(sa_serv));
  sa_serv.sin_family      = AF_INET;
  sa_serv.sin_addr.s_addr = INADDR_ANY;
  sa_serv.sin_port        = htons(prog_opt->port); /* Server Port number */

#if defined(SOL_SOCKET) && defined(SO_REUSEADDR)
  setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, (void *)&optval, sizeof(int));
#endif

  bind(*sock, (struct sockaddr *)&sa_serv, sizeof(sa_serv));
  listen(*sock, 1024);
  fprintf_log(0, "Server ready. Listening to port '%d'.\n\n", prog_opt->port);

#if defined(SOL_SOCKET) && defined(SO_RCVTIMEO)
  setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
#endif
  *client = accept(*sock, (struct sockaddr *)&sa_cli, &client_len);
  if (*client < 0) {
    fprintf_log(0, "failure in accept(): errno=%d: %s\n", errno, strerror(errno));
  } else
    fprintf_log(0, "- connection from %s, port %d\n",
                   inet_ntop(AF_INET, &sa_cli.sin_addr, topbuf, sizeof(topbuf)),
                   ntohs(sa_cli.sin_port) );
  return *client;
}

void
set_br_eng_tls_protocol_range(br_ssl_engine_context* eng)
{
  unsigned proto_ver_min = BR_TLS10, proto_ver_max = BR_TLS12;
  const char*  no_tlsv1 = env_get("NO_TLSV1");
  const char*  no_tlsv1_1 = env_get("NO_TLSV1_1");
  const char*  no_tlsv1_2 = env_get("NO_TLSV1_2");

  if (no_tlsv1 && !no_tlsv1_1 && !no_tlsv1_2)
    proto_ver_min = BR_TLS11;
  else if (no_tlsv1_1 && !no_tlsv1_2)
    proto_ver_min = BR_TLS12;
  else if (!no_tlsv1 && no_tlsv1_1 && no_tlsv1_2)
    proto_ver_max = BR_TLS10, proto_ver_max = BR_TLS10;
  else if (no_tlsv1 && !no_tlsv1_1 && no_tlsv1_2)
    proto_ver_max = BR_TLS11, proto_ver_max = BR_TLS11;
  else if (no_tlsv1 || no_tlsv1_1 || no_tlsv1_2) {
    fprintf_log(0, "NO_TLSV1=%s NO_TLSV1_1=%s NO_TLSV1_2=%s are invalid set, ignored\n",
                    no_tlsv1, no_tlsv1_1, no_tlsv1_2);
  }
  fprintf_log(0, "TLS min-max is set to 0x%04x(%s) - 0x%04x(%s)\n",
                 proto_ver_min, brhlp_protocol_code16_to_name(proto_ver_min),
                 proto_ver_max, brhlp_protocol_code16_to_name(proto_ver_max));

  /* BearSSL cannot have a "hole" of supported protocol version */
  br_ssl_engine_set_versions(eng, proto_ver_min, proto_ver_max);
}

void
set_br_eng_cipher_suites(br_ssl_engine_context* eng)
{
  size_t     suites_num = 0, i;
  uint16_t*  suites = brhlp_parse_suites_string(env_get("BEARSSL_CIPHERS"), &suites_num);

  if (suites_num < 1) {
    fprintf_log(0, "no cipher suite is specified\n");
    return;
  }

  for (i = 0; i < suites_num; i++)
    fprintf_log(0, "set cipher suite[%d]: 0x%04x:%s\n", i, suites[i], brhlp_suite_code16_to_name(suites[i]));

  br_ssl_engine_set_suites(eng, suites, suites_num);
  free(suites);
}

int
setup_ssl(propn_prog_opt_st* prog_opt, int* client, int* sock, ssl_sess_t* sess)
{
  int ret;
  vec_x509_cert_ptr  vec_cert = new_vec_cert();
  private_key_ptr    pkey = NULL;
  int                fd_srv_cert = -1, fd_srv_pkey = -1;
  unsigned char*     ibuf = NULL;
  unsigned char*     obuf = NULL;
  brssl_ctxs_t*      ctxs = NULL;

  *sock   = -1;
  *client = -1;
  ctxs = (brssl_ctxs_t*)sess->ssl;

  fd_srv_cert = propn_get_fd_by_var_name(prog_opt->envkey_srv_cert);
  fprintf_log(0, "CERTFILE: fd#%d <- %s\n", fd_srv_cert, prog_opt->envkey_srv_cert);

  fd_srv_pkey = propn_get_fd_by_var_name(prog_opt->envkey_srv_pkey);
  fprintf_log(0, "KEYFILE: fd#%d <- %s\n", fd_srv_pkey, prog_opt->envkey_srv_pkey);

  fprintf_log(0, "before certification install: %d\n", br_ssl_engine_last_error(ctxs->eng));

  ret = set_ctx_cert_pkey_fd2(ctxs->srv, fd_srv_cert, fd_srv_pkey, MAX_SIZE_PEM, vec_cert, &pkey);
  fprintf_log(0, "after certification install: %d\n", br_ssl_server_reset(ctxs->srv));
  if (ret)
    return ret;

  ibuf = malloc(prog_opt->buffsize);
  if (!ibuf)
    return errno;

  obuf = malloc(prog_opt->buffsize);
  if (!obuf)
    return errno;

  /* br_ssl_engine_set_buffer(ctxs->eng, iobuf, prog_opt->buffsize, 1); */
  br_ssl_engine_set_buffers_bidi(ctxs->eng, ibuf, prog_opt->buffsize,
                                            obuf, prog_opt->buffsize);

  fprintf_log(0, "after engine buffer setting: %d\n", br_ssl_engine_last_error(ctxs->eng));

  /* BearSSL requests server reset after buffer setting */

  /* NOTE: br_ssl_server_reset() returns 1 on success, 0 on error. */
  ret = br_ssl_server_reset(ctxs->srv);
  fprintf_log(0, "after engine reset: %d\n", br_ssl_engine_last_error(ctxs->eng));
  if (ret == 0)
    return -1;

  set_br_eng_tls_protocol_range(ctxs->eng);
  set_br_eng_cipher_suites(ctxs->eng);

  if (prog_opt->port == 0) {
    /*
     * br_sslio_init() require the pointers holding the file descriptors to read/write
     * coded data (not plain data), so we use &(sess->rfd), &(sess->wfd) for this.
     */
    sess->rfd = 0;
    sess->wfd = 1;
    br_sslio_init(ctxs->io, ctxs->eng, fd_read, &(sess->rfd), fd_write, &(sess->wfd));
    return 0;
  }

  ret = create_socket(prog_opt, client, sock);
  if (ret < 0)
    goto bail_out;

  sess->rfd = *client;
  sess->wfd = *client;
  br_sslio_init(ctxs->io, ctxs->eng, fd_read, client, fd_write, client);
  return 0;

bail_out:
  /* error occured before sslio initialization, ctxs->io should not be looking like valid */
  ctxs->io = NULL;

  if (0 <= *client)
    close(*client);
  *client = -1;

  if (0 <= *sock)
    close(*sock);
  *sock = -1;

  return ret;
}

int ssl_err_is_retryable(int err)
{
  switch (err) {
  case BR_ERR_OK:
  case BR_ERR_BAD_STATE:
    return 1;
  default:
    return 0;
  }
}

void proc_parent(trfd_s* trfd, ssl_sess_t* sess)
{
  struct timeval  tv;

  tuple_err_t err = TUPLE_ERR_ZERO;
  fdpb_t  c2p, p2c, e2p;
  int     p_c2p = trfd->r, p_p2c = trfd->w, p_e2p = trfd->e;
  fd_set  fds_rd;
  /* fd_set  fds_wr; */
  int     nfds;
  int     ssl_rd_got_eof = 0;

  FD_ZERO(&fds_rd);
  FD_SET(p_c2p, &fds_rd);
  FD_SET(p_e2p, &fds_rd);
  FD_SET(sess->rfd, &fds_rd);

  nfds = MY_MAX( MY_MAX(p_c2p, p_e2p), sess->rfd ) + 1;

  fdpb_init(&p2c, p_p2c, pipe_buff_size);
  fdpb_init(&c2p, p_c2p, pipe_buff_size);
  fdpb_init(&e2p, p_e2p, pipe_buff_size);

  set_timeval(&tv, sess->timeout->select_rd, 0);
  set_timeval(&(p2c.tv), 1, 1);
  set_timeval(&(c2p.tv), 1, 1);
  set_timeval(&(e2p.tv), 1, 1);

  // reading loop
  while (0 != select(nfds, &fds_rd, NULL, NULL, &tv)) {
    dump_selected_fds(p_p2c, p_c2p, p_e2p, sess->rfd, sess->wfd, &fds_rd, NULL);

    fdpb_set_ready(&c2p, &fds_rd);
    fdpb_set_ready(&e2p, &fds_rd);

    handle_e2p(&e2p);
    handle_p2c(&p2c);

    if (FD_ISSET(sess->rfd, &fds_rd)) {
      if (0 <= ssl_to_p2c(sess, &p2c, &err)) {
        handle_e2p(&e2p);
        handle_p2c(&p2c);
        goto prep_next;
      }

      ssl_rd_got_eof = 1;
      break;
    } else
    if (fdpb_has_data(&c2p)) {
      if (0 > c2p_to_ssl(sess, &c2p, &err)                 ||
          0 > br_sslio_flush(get_br_sslio_from_sess(sess)) ) {
        ssl_rd_got_eof = 1;
        break;
      }

      if (err.ssl != BR_ERR_BAD_STATE && 0 == sess->get_pending(sess->ssl)) {
        fprintf_log(0, "nothing to read after writing\n");
        goto prep_next;
      }

      if (fdpb_is_valid(&p2c)) {
        if (0 > ssl_to_p2c(sess, &p2c, &err))
          ssl_rd_got_eof = 1;
      } else {
        if (0 > discard_ssl_pended_data(sess, &err))
          ssl_rd_got_eof = 1;
      }

      fprintf_log(0, "ssl_rd_got_eof = %d\n", ssl_rd_got_eof);
      if (ssl_rd_got_eof)
        break;

      goto prep_next;

    } else
    if (fdpb_is_valid(&c2p) && c2p.ready) {
      // SSL is not confirmed to be ready to write, but read from c2p to its internal buffer
      fdpb_read_internal(&c2p);

      fprintf_log(0, "fdpb_read_internal(c2p) got %zu bytes, total %zu bytes\n", c2p.len_chunk, c2p.len);
      if (c2p.got_eof) {
        fprintf_log(0, " c2p got EOF\n");
      } else if (0 > c2p_to_ssl(sess, &c2p, &err)                 ||
                 0 > br_sslio_flush(get_br_sslio_from_sess(sess)) ) {
        ssl_rd_got_eof = 1;
        break;
      }
    }

    close_dead_pipes(&p2c, &c2p, &e2p);

    if (child_finished && !fdpb_is_valid(&p2c) && !fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "child process is finished, p2c, c2p, e2p are closed, break select() loop\n");
      break;
    }

prep_next:
    if (!fdpb_is_valid(&c2p) && !fdpb_is_valid(&e2p)) {
      fprintf_log(0, "c2p & e2p are closed, exit loop\n");
      break;
    }

    fprintf_log(20, "prep for next select():");
    FD_ZERO(&fds_rd);
    if (fdpb_is_valid(&c2p)) {
      fprintf_log(21, " c2p");
      FD_SET(c2p.fd, &fds_rd);
    }
    if (fdpb_is_valid(&e2p)) {
      fprintf_log(21, " e2p");
      FD_SET(e2p.fd, &fds_rd);
    }
    if (ssl_err_is_retryable(err.ssl) && !ssl_rd_got_eof) {
      fprintf_log(21, " ssl_rd");
      FD_SET(sess->rfd, &fds_rd);
    }
    fprintf_log(21, "\n");

    set_timeval(&tv, sess->timeout->select_rd, 0);
  }
}

int main(const int argc, char* const* argv)
{
  int r = 0;
  propn_prog_opt_st prog_opt = PROPN_PROG_OPT_ST_NULL;

  prog_opt.envkey_srv_cert = "server-crt.pem";
  prog_opt.envkey_srv_pkey = "server-privatekey.pem";
  prog_opt.envkey_cli_cert = "client-crt.pem";
  prog_opt.port = DEFAULT_PORT;
  prog_opt.timeout_accept = 60;
  prog_opt.timeout_select = 60;
  prog_opt.buffsize = BEARSSL_BUFFSIZE_DEFAULT;
  propn_parse_args(argc, argv, &prog_opt);
  if (prog_opt.buffsize > 0)
    pipe_buff_size = prog_opt.buffsize;

  debug_level = prog_opt.debug_level;

  /* Ignore broken pipe signals */
  // signal(SIGPIPE, SIG_IGN);
  signal(SIGPIPE, update_child_finished);
  signal(SIGCHLD, update_child_finished);

  if (prog_opt.pathname_log) {
    unlink(prog_opt.pathname_log);
    fh_log = fopen(prog_opt.pathname_log, "w");
  } else
    fh_log = stderr;

  if (fh_log)
    fflush(fh_log);

  /* Handle connections */
  {
    pid_t pid;

    trfd_s trfd_prnt = { 0, 0, 0 }, trfd_chld = { 0, 0, 0 };

    if (trfd_open( TRIFIDES_AUTO, &trfd_prnt, &trfd_chld )) {
      fprintf_log(0, "failure in 3-file descriptors between parent and child STDIO/STDERR\n");
      r = -1;
      goto log_close;
    }

    pid = fork();
    switch (pid) {
      case -1:
        r = -2;
        break;

      case 0:
        /* child process */
        trfd_close_all(&trfd_prnt);
        propn_uidgid(&prog_opt, 1);
        trfd_xfer_012(&trfd_chld);

        pathexec(argv + (prog_opt.index_last_parsed_argv + 1));
        break;

      default:
        /* parent process, should keep SSL pipes until child process exit */
        trfd_close_all(&trfd_chld);
        {
          int ret = 0, sock = -1, client = -1;
          br_ssl_server_context  srv_ctx  = { 0 };
          br_sslio_context       io_ctx   = { 0 };

          ssl_sess_t     sess;
          ssl_timeout_t  sess_to = {prog_opt.timeout_accept, prog_opt.timeout_select, prog_opt.timeout_select};
          brssl_ctxs_t   ctxs = {&srv_ctx, &(srv_ctx.eng), &io_ctx};

          ssl_sess_clear(&sess);
          ssl_sess_init_err_sym_brssl(&sess);
          ssl_sess_fill_brssl(&sess, &ctxs);

          if (propn_chroot(&prog_opt)) {
            fprintf_log(0, "chroot failed\n");
          }

          propn_uidgid(&prog_opt, 0);

          sess.timeout = &sess_to;

          ret = setup_ssl(&prog_opt, &client, &sock, &sess);
          if (ret) {
            fprintf_log(0, "Failure in setup_ssl(): error code=%d, %s\n", ret, strerror(ret) ? strerror(ret) : "Unknown");
            goto bail_out;
          }

          if (!fh_log)
            fh_log = stderr;

          proc_parent(&trfd_prnt, &sess);
          fprintf_log(0, "parental loop is finished\n");
          goto bail_out;

bail_out:
          if (!child_finished) {
            fprintf_log(0, "terminate child process pid=%d\n", pid);
            kill(pid, SIGTERM);
          }
          if (ctxs.io) {
            br_sslio_close(ctxs.io);
          }
          if (0 <= client) {
            fprintf_log(0, "close() client %d\n", client);
            close(client);
          }
          if (0 <= sock) {
            fprintf_log(0, "close() sock %d\n", sock);
            close(sock);
          }
        }
    } /* switch */
  }

log_close:
  if (fh_log)
    fclose(fh_log);

  exit(r);
}
