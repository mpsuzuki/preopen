#ifndef SSLEXEC_HELP_H
#define SSLEXEC_HELP_H

/* fd_set */
#include <sys/select.h>

extern int child_finished;

void update_child_finished(int);

struct timeval* set_timeval(struct timeval*, unsigned int, unsigned int);
void log_escaped_str(int, char*, size_t);

typedef struct {
  int sys; /* POSIX errno */
  int ssl; /* SSL implementation dependent error code */
} tuple_err_t;

#define TUPLE_ERR_ZERO { 0, 0 }

void tuple_err_clear(tuple_err_t*);
int  tuple_err_ok(tuple_err_t*);

void dump_selected_fds(int, int, int,  int, int,  fd_set*, fd_set*);
void close_fdpb_if_eof(fdpb_t*, char*);
void close_dead_pipes(fdpb_t*, fdpb_t*, fdpb_t*);

void handle_e2p(fdpb_t*);
void handle_p2c(fdpb_t*);

#endif
