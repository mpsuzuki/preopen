#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <errno.h>
#include <sys/types.h>
#include <signal.h>

#include <sys/select.h>

#if HAVE_GETOPT_H
# include <getopt.h>
#endif

#if !HAVE_GETLINE
# include "getline.h"
#endif

#if !HAVE_STRNLEN
# include "strnlen.h"
#endif

extern char** environ;
char*  flog_path = NULL;
FILE*  flog = NULL;
int    timeout_start  = 10;
int    timeout_select = 10;
int    mode_quiet = 0;

struct timeval* set_timeval(struct timeval* tv, int sec, int usec)
{
  tv->tv_sec  = sec;
  tv->tv_usec = usec;
  return tv;
}

void sighandle_child_term(int signum)
{
  if (flog) {
    fprintf(flog, "received signal=%s\n", strsignal(signum));
    fclose(flog);
  }

  fclose(stdin);
  fclose(stdout);
  exit(0);
}

int buff_ends_newline(char* buff, size_t n)
{
  size_t len;
  if (0 < n)
    len = strnlen(buff, n);
  else
    len = strlen(buff);

  if (len == 0)
    return 0;
  else if (buff[len - 1] == '\n')
    return 1;
  else
    return 0;
}

void dump_env()
{
  int i;
  for (i = 0; environ[i] != NULL; i++) {
    fprintf(flog, "\t%s\n", environ[i]);
  }
}

void parse_args(int argc, char** argv)
{
  while (1) {
    int option_index = 0;
#if HAVE_GETOPT_LONG
    static struct option long_options[] = {
      {"timeout-start",  required_argument, 0, 'T'},
      {"timeout-select", required_argument, 0, 't'},
      {"quiet",          no_argument,       0, 'q'},
      {NULL, 0, 0, 0}
    };
    int c = getopt_long(argc, argv, "T:t:q", long_options, &option_index);
#else
    int c = getopt(argc, argv, "T:t:q");
#endif
    if (c < 0)
      break;
    switch (c) {
    case 'T':
       timeout_start = (int)strtoul(optarg, NULL, 0);
       break;
    case 't':
       timeout_select = (int)strtoul(optarg, NULL, 0);
       break;
    case 'q':
       mode_quiet = 1;
       break;
    }
  }
  if (optind < argc) {
    flog_path = argv[optind];
  }
}

int main(int argc, char** argv)
{
  char*   buff = NULL;
  ssize_t data_len;
  size_t  data_limit;
  fd_set  fds_rd;
  struct timeval tv;

  parse_args(argc, argv);
  if (flog_path)
    flog = fopen(flog_path, "w");
  if (!flog && !mode_quiet)
    flog = stderr;

  if (flog) {
    fprintf(flog, "environment\n");
    if (!mode_quiet)
      dump_env();
    fprintf(flog, "\n");
  }

  FD_ZERO(&fds_rd);
  FD_SET(0, &fds_rd);

  set_timeval(&tv, timeout_start, 0);

  while (0 != select(1, &fds_rd, NULL, NULL, tv.tv_sec == 0 ? NULL : &tv)) {
    data_len = getline(&buff, &data_limit, stdin);

    switch(data_len) {
    case EINVAL:
    case ENOMEM:
      if (flog)
        fprintf(flog, "client cannot read anymore\n");
      goto bailout;

    case EOF:
      if (flog)
        fprintf(flog, "client received EOF, exit\n");
      goto bailout;

    case 0:
      if (flog)
        fprintf(flog, "client received zero-length data, exit\n");
      goto bailout;

    default:
      printf("%s", buff);
      if (flog)
        fprintf(flog, "received %zu bytes: %s%s",
                data_len, buff,
                buff_ends_newline(buff, 0) ? "" : "\n");
      if (buff) {
        free(buff);
        buff = NULL;
      }
    }
    fflush(stdout);
    fflush(flog);
    set_timeval(&tv, timeout_select, 0);
  }

bailout:
  if (flog)
    fclose(flog);
  fclose(stdin);
  fclose(stdout);

  exit(0);
}
