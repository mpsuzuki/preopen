/* for size_t */
#include <sys/types.h>

size_t strnlen(const char* s, size_t maxlen)
{
  size_t i;
  for (i = 0; s[i] && i < maxlen; i++)
    ;
  return i;
}
