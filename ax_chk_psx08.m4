dnl -------------------------------------------------------
dnl AX_CHECK_POSIX2008_FUNC
dnl
dnl On some platforms, the functions requested by POSIX2008 
dnl are implemented in libc, but the declarations are not
dnl visible by default. On these systems, _BSD_SOURCE, or
dnl _GNU_SOURCE, or _POSIX_C_SOURCE=200809 are required to
dnl make new functions declared.
dnl
dnl $1 function name
dnl $2 variable name to get a macro to declare the function
dnl $3 action for the case some macro enables declaration
dnl $4 action for the case no macro enables declaration
dnl -------------------------------------------------------
dnl

AC_DEFUN([AX_CHECK_POSIX2008_FUNC],[
  $2=
  ax_chk_orig_CFLAGS="${CFLAGS}"
  ax_chk_psx08_upcase_func=`echo $1 | tr 'a-z' 'A-Z'`
  for ax_chk_psx08_def in \
    -D_POSIX_C_SOURCE=200809 \
    -D_BSD_SOURCE \
    -D_GNU_SOURCE \
    -D_WITH_${ax_chk_psx08_upcase_func}
  do
    AC_MSG_CHECKING([whether ${ax_chk_psx08_def} makes $1 declared])
    CFLAGS="${ax_chk_orig_CFLAGS} [$]{ax_chk_psx08_def}"
    AC_COMPILE_IFELSE([
      AC_LANG_PROGRAM[[
        /* for getline(), getdelim() */
        #include <stdio.h>

        /* for strnlen() */
        #include <string.h>
    ]],[[
        #ifndef $1
        (void) $1;
        #endif
    ]])],[
      AC_MSG_RESULT([yes])
      ax_cv_def_to_declare_$1=[$]{ax_chk_psx08_def}
      $2=[$]{ax_chk_psx08_def}
      break
    ],[
      AC_MSG_RESULT([no])
    ])
  done
  CFLAGS="${ax_chk_orig_CFLAGS}"
  if test x"${$2}" != x
  then
    $3
  else
    $4
  fi
])
