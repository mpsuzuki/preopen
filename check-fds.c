#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>

#if !HAVE_GETLINE
# include "getline.h"
#endif

void dump_environ(const char** environ)
{
  char** e;
  for (e = (char**)environ; *e != NULL && **e != '\0'; e++)
    puts(*e);
}

int main(int argc, const char** argv, const char** environ)
{
  int i;
  dump_environ(environ);
  printf("UID=%d\n", getuid());
  for (i = 1; i < argc; i++) {
    char* env_value = getenv(argv[i]);
    char* endptr;
    unsigned long l_fd = strtoul(env_value, &endptr, 0);
    if (*endptr != '\0') {
      printf("# %s does not give an integer \"%s\"\n", argv[i], env_value);
    } else if (INT_MAX <= l_fd) {
      printf("# %s is too big number (%ld)\n", argv[i], l_fd);
    } else {
      int i_fd = (int)l_fd;
      FILE* f = fdopen(i_fd, "r");
      char* l = NULL;
      size_t s;

      printf("# %s -> fd:%d\n", argv[i], i_fd);
      if (!f)
        continue;
      while (0 <= getline(&l, &s, f)) {
        printf("%s\t%s", argv[i], l);
      }
      fclose(f);
    }
  }
}

