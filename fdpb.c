#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>

/* struct stat */
#include <sys/types.h>

/* fstat() */
#include <sys/stat.h>

#include "fdpb.h"

fdpb_t* fdpb_init(fdpb_t* f, int fd, size_t s)
{
  char* b = NULL;

  if (f) {
    f->fd        = fd;
    f->buff      = NULL;
    f->limit     = 0;
    f->len       = 0;
    f->ready     = 0;
    f->got_eof   = 0;
    f->len_chunk = 0;
    f->tv.tv_sec = 0;
    f->tv.tv_usec = 0;

    b = (char*)malloc(s + 1);
    if (!b)
      return NULL;

    memset(b, 0, s + 1);
    f->buff  = b;
    f->limit = s;
  }

  return f;
}

size_t fdpb_len_room(fdpb_t* f)
{
  if (!f)
    return -1;

  return (f->limit - f->len);
}

ssize_t fdpb_extend(fdpb_t* f, size_t delta)
{
  char* buff_new = realloc(f->buff, f->limit + delta + 1);
  if (!buff_new)
    return (-errno);

  memset(buff_new + f->len, 0, f->limit + delta + 1 - f->len);

  f->limit += delta;
  f->buff = buff_new;
  return delta;
}

fdpb_t* fdpb_shift(fdpb_t* f, size_t delta)
{
  if (!f)
    return NULL;

  memmove(f->buff, f->buff + delta, f->limit - delta);
  memset(f->buff + f->limit - delta, 0, delta);
  f->len -= delta;
  return f;
}

ssize_t fdpb_read_external(fdpb_t* f, int fd)
{
  if (!f || fd < 0)
    return -1;

  f->len_chunk = read(fd, f->buff + f->len, f->limit - f->len);
  if (0 < f->len_chunk)
    f->len += f->len_chunk;
  else if (0 == f->len_chunk)
    f->got_eof = 1;
  return f->len_chunk;
}

ssize_t fdpb_read_internal(fdpb_t* f)
{
  if (!f)
    return -1;

  return fdpb_read_external(f, f->fd);
}

ssize_t fdpb_write_external(fdpb_t* f, int fd)
{
  if (!f || fd < 0)
    return -1;

  if (f->len == 0)
    return 0;

  if (f->tv.tv_sec != 0 || f->tv.tv_usec != 0) {
    switch (fdpb_wait_select_fd_external(f, fd, 'w')) {
    case 0: // timeout
      return 0;
    case -1: // something goes wrong
      return -1;
    default:
      break;
    }
  }

  f->len_chunk = write(fd, f->buff, f->len);
  if (0 < f->len_chunk)
    fdpb_shift(f, f->len_chunk);

  return f->len_chunk;
}

ssize_t fdpb_write_internal(fdpb_t* f)
{
  if (!f)
    return -1;
  return fdpb_write_external(f, f->fd);
}


int fdpb_set_ready(fdpb_t* f, fd_set* fds)
{
  if (!fdpb_is_valid(f))
    return 0;

  if (FD_ISSET(f->fd, fds) && !f->got_eof)
    f->ready = 1;
  else
    f->ready = 0;

  return f->ready;
}

int fdpb_wait_select_fd_external(fdpb_t* f, int fd, char m)
{
  int            r;
  fd_set         fds;
  struct timeval tv;

  if (!f)
    return -2;

  if (fd < 0)
    return -3;

  switch (m) {
  case 'R': case 'r': m = 'r'; break;
  case 'W': case 'w': m = 'w'; break;
  default: return -4;
  }

  tv = f->tv;

  FD_ZERO(&fds);
  FD_SET(fd, &fds);
  r = select(fd + 1,
             (m == 'r' ? &fds : NULL),
             (m == 'w' ? &fds : NULL),
              NULL,
              &tv);

  if (r == 1 && !FD_ISSET(fd, &fds)) {
    // this should not happen.
    r = 0;
  }

  return r;
}

int fdpb_wait_select_fd_internal(fdpb_t* f, char m)
{
  if (!fdpb_is_valid(f))
    return -1;

  return fdpb_wait_select_fd_external(f, f->fd, m);
}

void fdpb_done(fdpb_t* f)
{
  if (!f)
    return;

  if (f->buff)
    free(f->buff);

  f->buff = NULL;

  if (f->fd < 0)
    return;

  close(f->fd);
  f->fd = -1;

  f->len   = 0;
  f->limit = 0;
  f->ready = 0;
  f->got_eof = 1;
  f->len_chunk = 0;
}

int fdpb_has_data(fdpb_t* f)
{
  if (!f)
    return 0;

  if (!f->buff)
    return 0;

  if (!f->len)
    return 0;

  return 1;
}

int fdpb_has_fd(fdpb_t* f)
{
  if (!f)
    return 0;

  if (f->fd < 0)
    return 0;

  /* XXX we do not check the detailed nature of fd */
  /* p2c, c2p, e2p should be (sb.st_mode & S_IFMT) == S_IFIFO  */
  /* TCP/IP socket should be (sb.st_mode & S_IFMT) == S_IFSOCK */

  if (f->got_eof)
    return 0;

  return 1;
}

int fdpb_get_ifmt(fdpb_t* f)
{
  struct stat sb;

  if (!f)
    return -1;

  if (0 > fstat(f->fd, &sb))
    return -1;

  return (sb.st_mode & S_IFMT);
}

int fdpb_is_socket(fdpb_t* f)
{
  int s_ifmt = fdpb_get_ifmt(f);

  return (s_ifmt == S_IFSOCK ? 1 : 0);
}

int fdpb_is_fifo(fdpb_t* f)
{
  int s_ifmt = fdpb_get_ifmt(f);

  return (s_ifmt == S_IFIFO ? 1 : 0);
}

int fdpb_is_valid(fdpb_t* f)
{
  if (!f)
    return 0;

  if (!fdpb_has_fd(f))
    return 0;

  if (!f->buff)
    return 0;

  return 1;
}
