QLIBS-MINI
==========

This directory contains the minimum components from fehQlibs
( www.fehcom.de/ipnet/qlibs.html ) which are needed to build
preopen. Refer LICENSE file in this directory.

The files in this directory may be older than the latest
fehQlibs. Current files are taken from fehQlibs-23.
